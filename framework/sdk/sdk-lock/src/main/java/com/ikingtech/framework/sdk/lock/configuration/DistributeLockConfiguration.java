package com.ikingtech.framework.sdk.lock.configuration;

import com.ikingtech.framework.sdk.lock.lock.DistributeLock;
import com.ikingtech.framework.sdk.lock.lock.RedisDistributeLock;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.integration.redis.util.RedisLockRegistry;

/**
 * @author tie yan
 */
public class DistributeLockConfiguration {

    @Bean(destroyMethod = "destroy")
    public RedisLockRegistry redisLockRegistry(LettuceConnectionFactory lettuceConnectionFactory) {
        return new RedisLockRegistry(lettuceConnectionFactory, "dist_lock");
    }

    @Bean
    public DistributeLock distributeLock(RedisLockRegistry redisLockRegistry) {
        return new RedisDistributeLock(redisLockRegistry);
    }
}
