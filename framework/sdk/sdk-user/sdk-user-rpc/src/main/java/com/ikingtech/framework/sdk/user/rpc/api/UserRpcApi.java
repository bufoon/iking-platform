package com.ikingtech.framework.sdk.user.rpc.api;

import com.ikingtech.framework.sdk.user.api.UserApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "UserRpcApi", path = "/system/user")
public interface UserRpcApi extends UserApi {
}
