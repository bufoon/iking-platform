package com.ikingtech.framework.sdk.user.extension.configuration;

import com.ikingtech.framework.sdk.user.extension.UserEventListenerCollector;
import com.ikingtech.framework.sdk.user.extension.UserEventResolver;
import com.ikingtech.framework.sdk.user.extension.UserInfoEventReminder;
import com.ikingtech.framework.sdk.user.extension.properties.UserExtensionProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@EnableConfigurationProperties({UserExtensionProperties.class})
public class UserExtensionConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".user", name = "extension-enable", havingValue = "true")
    public UserEventListenerCollector userEventListenerCollector(StringRedisTemplate redisTemplate, List<UserInfoEventReminder> reminders) {
        return new UserEventListenerCollector(redisTemplate, reminders);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".auth", name = "extension-enable", havingValue = "true")
    public UserEventResolver userEventResolver(List<UserInfoEventReminder> reminders) {
        return new UserEventResolver(reminders);
    }
}
