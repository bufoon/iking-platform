package com.ikingtech.framework.sdk.user.extension;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.cache.constants.CacheConstants.USER_EVENT_REMINDER;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class UserEventListenerCollector implements ApplicationRunner {

    private final StringRedisTemplate redisTemplate;

    private final List<UserInfoEventReminder> reminders;

    @Value("${spring.application.name}")
    private String serverName;

    @Override
    public void run(ApplicationArguments args) {
        if (Tools.Coll.isBlank(this.reminders)) {
            this.redisTemplate.opsForSet().remove(USER_EVENT_REMINDER, this.serverName);
        }
        this.redisTemplate.opsForSet().add(USER_EVENT_REMINDER, this.serverName);
    }
}
