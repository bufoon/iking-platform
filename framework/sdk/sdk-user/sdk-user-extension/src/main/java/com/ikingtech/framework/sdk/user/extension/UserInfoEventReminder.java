package com.ikingtech.framework.sdk.user.extension;

import com.ikingtech.framework.sdk.user.extension.model.UserInfoEvent;

/**
 * @author tie yan
 */
public interface UserInfoEventReminder {

    void add(UserInfoEvent user);

    void delete(String id);

    void update(UserInfoEvent user);
}
