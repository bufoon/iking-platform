package com.ikingtech.framework.sdk.user.api;

import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.user.model.*;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface UserApi {

    /**
     * 添加用户信息
     *
     * @param user 用户信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加用户信息", description = "添加用户信息")
    R<String> add(@Parameter(name = "user", description = "用户信息")
                  @RequestBody UserDTO user);

    /**
     * 删除用户
     *
     * @param id 用户编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除用户", description = "删除用户")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新用户信息
     *
     * @param user 用户信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新用户信息", description = "更新用户信息")
    R<Object> update(@Parameter(name = "user", description = "用户信息")
                     @RequestBody UserDTO user);

    /**
     * 更新用户基本信息
     *
     * @param user 用户基本信息
     * @return 返回更新结果
     */
    @PostRequest(order = 4, value = "/info/update", summary = "更新用户基本信息", description = "更新用户基本信息")
    R<Object> updateUserInfo(@Parameter(name = "user", description = "用户基本信息")
                             @RequestBody UserBasicDTO user);

    /**
     * 获取所有用户基本信息
     *
     * @return 返回所有用户基本信息列表
     */
    @PostRequest(order = 5, value = "/info/list/all", summary = "获取所有用户基本信息", description = "获取所有用户基本信息")
    R<List<UserBasicDTO>> allInfo();

    /**
     * 获取分页用户信息
     *
     * @param queryParam 查询条件
     * @return 返回分页用户信息列表
     */
    @PostRequest(order = 6, value = "/list/page", summary = "获取分页用户信息", description = "获取分页用户信息")
    R<List<UserDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                          @RequestBody UserQueryParamDTO queryParam);

    /**
     * 获取用户详细信息
     *
     * @param id 用户编号
     * @return 返回用户详细信息
     */
    @PostRequest(order = 7, value = "/detail/id", summary = "获取用户详细信息", description = "获取用户详细信息")
    R<UserDTO> detail(@Parameter(name = "id", description = "编号")
                      @RequestBody String id);

    /**
     * 绑定用户社交信息
     *
     * @param userBind 用户绑定信息
     * @return 返回绑定结果
     */
    @PostRequest(order = 8, value = "/bind/social", summary = "绑定用户社交信息", description = "绑定用户社交信息")
    R<Object> bindSocial(@Parameter(name = "userBind", description = "用户绑定信息")
                         @RequestBody UserSocialBindDTO userBind);

    /**
     * 重置用户密码
     *
     * @param id 用户编号
     * @return 返回重置结果
     */
    @PostRequest(order = 9, value = "/password/reset", summary = "重置用户密码", description = "重置用户密码")
    R<Object> resetPassword(@Parameter(name = "id", description = "用户编号。")
                            @RequestBody String id);

    /**
     * 重置租户管理员用户密码
     *
     * @param tenantCode 租户标识
     * @return 返回重置结果
     */
    @PostRequest(order = 10, value = "/tenant/admin-user/password/reset", summary = "重置租户管理员用户密码", description = "重置租户管理员用户密码")
    R<Object> resetTenantAdminUserPassword(@Parameter(name = "tenantCode", description = "租户标识。")
                                           @RequestBody String tenantCode);

    /**
     * 修改用户密码
     *
     * @param modifyParam 用户修改密码信息
     * @return 返回修改结果
     */
    @PostRequest(order = 11, value = "/password/modify", summary = "修改用户密码", description = "修改用户密码")
    R<Object> modifyPassword(@Parameter(name = "modifyParam", description = "用户修改密码信息")
                             @RequestBody UserPasswordModifyParamDTO modifyParam);

    /**
     * 锁定用户
     *
     * @param username 用户名
     * @return 返回锁定结果
     */
    @PostRequest(order = 12, value = "/lock", summary = "锁定用户", description = "锁定用户")
    R<Object> lock(@Parameter(name = "username", description = "用户名")
                   @RequestBody String username);

    /**
     * 解锁用户
     *
     * @param username 用户名
     * @return 返回解锁结果
     */
    @PostRequest(order = 13, value = "/unlock", summary = "解锁用户", description = "解锁用户")
    R<Object> unlock(@Parameter(name = "username", description = "用户名")
                     @RequestBody String username);

    /**
     * 根据角色编号集合查询用户基本信息
     *
     * @param roleIds 角色编号集合
     * @return 用户信息列表
     */
    @PostRequest(order = 14, value = "/info/list/role-ids", summary = "根据角色编号集合查询用户基本信息", description = "根据角色编号集合查询用户基本信息")
    R<List<UserBasicDTO>> listInfoByRoleIds(@Parameter(name = "roleIds", description = "角色编号集合")
                                            @RequestBody BatchParam<String> roleIds);

    /**
     * 根据角色编号集合查询用户信息
     *
     * @param roleIds 角色编号集合
     * @return 用户信息列表
     */
    @PostRequest(order = 15, value = "/list/role-ids", summary = "根据角色编号集合查询用户信息", description = "根据角色编号集合查询用户信息")
    R<List<UserDTO>> listByRoleIds(@Parameter(name = "roleIds", description = "角色编号集合")
                                   @RequestBody BatchParam<String> roleIds);

    /**
     * 根据角色编号查询用户ID列表
     *
     * @param roleId 角色编号
     * @return 用户ID列表
     */
    @PostRequest(order = 16, value = "/id/list/role-id", summary = "根据角色编号查询用户ID列表", description = "根据角色编号查询用户ID列表")
    R<List<String>> listIdByRoleId(@Parameter(name = "roleId", description = "角色编号")
                                   @RequestBody String roleId);

    /**
     * 根据部门编号查询用户ID列表
     *
     * @param deptId 部门编号
     * @return 用户ID列表
     */
    @PostRequest(order = 17, value = "/id/list/dept-id", summary = "根据部门编号查询用户ID列表", description = "根据部门编号查询用户ID列表")
    R<List<String>> listIdByDeptId(@Parameter(name = "deptId", description = "部门编号")
                                   @RequestBody String deptId);

    /**
     * 根据岗位编号集合查询用户基本信息
     *
     * @param postIds 岗位编号集合
     * @return 用户信息列表
     */
    @PostRequest(order = 18, value = "/info/list/post-ids", summary = "根据岗位编号集合查询用户基本信息", description = "根据岗位编号集合查询用户基本信息")
    R<List<UserBasicDTO>> listInfoByPostIds(@Parameter(name = "postIds", description = "岗位编号集合")
                                            @RequestBody BatchParam<String> postIds);

    /**
     * 根据岗位编号查询用户ID列表
     *
     * @param postId 岗位编号
     * @return 用户ID列表
     */
    @PostRequest(order = 19, value = "/id/list/post-id", summary = "根据岗位编号查询用户ID列表", description = "根据岗位编号查询用户ID列表")
    R<List<String>> listIdByPostId(@Parameter(name = "postId", description = "岗位编号")
                                   @RequestBody String postId);

    /**
     * 根据菜单标识集合查询用户基本信息
     *
     * @param menuCodes 菜单标识集合
     * @return 用户信息列表
     */
    @PostRequest(order = 20, value = "/info/list/menu-codes", summary = "根据菜单标识集合查询用户基本信息", description = "根据菜单标识集合查询用户基本信息")
    R<List<UserBasicDTO>> listInfoByMenuCodes(@Parameter(name = "menuCodes", description = "菜单标识集合。")
                                              @RequestBody BatchParam<String> menuCodes);

    /**
     * 根据菜单标识查询用户基本信息
     *
     * @param menuCode 菜单编号
     * @return 用户信息
     */
    @PostRequest(order = 21, value = "/info/list/menu-code", summary = "根据菜单标识查询用户基本信息", description = "根据菜单标识查询用户基本信息")
    R<List<UserBasicDTO>> listInfoByMenuCode(@Parameter(name = "menuCode", description = "菜单标识。")
                                             @RequestBody String menuCode);

    /**
     * 根据用户编号集合查询用户基本信息
     *
     * @param ids 用户编号集合
     * @return 用户信息列表
     */
    @PostRequest(order = 22, value = "/info/list/ids", summary = "根据用户编号集合查询用户基本信息", description = "根据用户编号集合查询用户基本信息")
    R<List<UserBasicDTO>> listInfoByIds(@Parameter(name = "ids", description = "用户编号集合")
                                        @RequestBody BatchParam<String> ids);

    /**
     * 根据用户编号集合查询用户基本信息映射
     *
     * @param ids 用户编号集合
     * @return 用户信息映射
     */
    @PostRequest(order = 23, value = "/info/map/ids", summary = "根据用户编号集合查询用户基本信息映射", description = "根据用户编号集合查询用户基本信息映射")
    R<Map<String, UserBasicDTO>> mapInfoByIds(@Parameter(name = "ids", description = "用户编号集合")
                                              @RequestBody BatchParam<String> ids);

    /**
     * 根据用户编号集合查询用户信息
     *
     * @param ids 用户编号集合
     * @return 用户信息列表
     */
    @PostRequest(order = 24, value = "/list/ids", summary = "根据用户编号集合查询用户信息", description = "根据用户编号集合查询用户信息")
    R<List<UserDTO>> listByIds(@Parameter(name = "ids", description = "用户编号集合")
                               @RequestBody BatchParam<String> ids);

    /**
     * 根据用户编号查询用户基本信息
     *
     * @param id 用户编号
     * @return 用户信息
     */
    @PostRequest(order = 25, value = "/info/id", summary = "根据用户编号查询用户基本信息", description = "根据用户编号查询用户基本信息")
    R<UserBasicDTO> getInfoById(@Parameter(name = "id", description = "用户编号")
                                @RequestBody String id);

    /**
     * 根据用户身份凭证查询用户基本信息
     *
     * @param credentialName 用户身份凭证
     * @return 用户信息
     */
    @PostRequest(order = 26, value = "/credential", summary = "根据用户身份凭证查询用户基本信息", description = "根据用户身份凭证查询用户基本信息")
    R<UserBasicDTO> getInfoByCredential(@Parameter(name = "credentialName", description = "用户身份凭证")
                                        @RequestBody String credentialName);

    /**
     * 根据用户身份凭证查询用户基本信息
     *
     * @param queryParam 查询参数
     * @return 用户信息
     */
    @PostRequest(order = 27, value = "/info/social", summary = "根据用户身份凭证查询用户基本信息", description = "根据用户身份凭证查询用户基本信息")
    R<UserBasicDTO> getInfoBySocial(@Parameter(name = "queryParam", description = "查询参数")
                                    @RequestBody UserSocialQueryParamDTO queryParam);

    /**
     * 获取当前登录用户基本信息
     *
     * @return 当前登录用户信息
     */
    @PostRequest(order = 28, value = "/info/login-user", summary = "获取当前登录用户基本信息", description = "获取当前登录用户基本信息")
    R<UserBasicDTO> getLoginUser();

    /**
     * 更新用户配置信息
     *
     * @param userConfigDTO 配置信息
     * @return 更新结果
     */
    @PostRequest(order = 29, value = "/config/update", summary = "更新用户配置信息", description = "更新用户配置信息")
    R<Object> updateConfig(@Parameter(name = "userConfigDTO", description = "配置信息")
                           @RequestBody UserConfigDTO userConfigDTO);

    /**
     * 获取登录用户配置信息列表
     *
     * @param global 是否全局
     * @return 主题信息列表
     */
    @PostRequest(order = 30, value = "/configs/list/login-user", summary = "获取登录用户配置信息列表", description = "获取登录用户配置信息列表")
    R<UserConfigDTO> listConfigByLoginUser(@Parameter(name = "global", description = "是否为全局配置")
                                           @RequestParam(value = "global", required = false) Boolean global);

    /**
     * 获取前端灰度值
     *
     * @return 前端主题灰度值
     */
    @PostRequest(order = 31, value = "/front-gray-scale/value", summary = "获取前端灰度值", description = "获取前端灰度值")
    R<String> getFrontGrayScale();

    /**
     * 获取最近一次登录的租户信息
     *
     * @return 最近一次登录的租户信息
     */
    @PostRequest(order = 32, value = "/recent/tenant/login-user", summary = "获取最近一次登录的租户信息", description = "获取最近一次登录的租户信息")
    R<Object> getRecentTenantByLoginUser();

    /**
     * 记录最近一次登录的租户信息
     *
     * @param tenantCode 最近一次登录的租户信息
     * @return 更新结果
     */
    @PostRequest(order = 33, value = "/recent/tenant/remember", summary = "记录最近一次登录的租户信息", description = "记录最近一次登录的租户信息")
    R<Object> rememberUserLoginTenant(@Parameter(name = "tenantCode", description = "用户最近一次登录的租户信息")
                                      @RequestBody String tenantCode);

    /**
     * 获取登录用户类别信息列表
     *
     * @return 用户类别信息列表
     */
    @PostRequest(order = 34, value = "/category/list/login-user", summary = "获取登录用户类别信息列表", description = "获取登录用户类别信息列表")
    R<List<UserCategoryDTO>> listCategoryByLoginUser();

    /**
     * 根据用户名称查询用户ID列表
     *
     * @param name 用户名称
     * @return 用户ID列表
     */
    @PostRequest(order = 35, value = "/id/list/name", summary = "根据用户名称查询用户ID列表", description = "根据用户名称查询用户ID列表")
    R<List<String>> listIdByName(@Parameter(name = "name", description = "用户名称")
                                 @RequestBody String name);
}
