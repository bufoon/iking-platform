package com.ikingtech.framework.sdk.user.api;

import com.ikingtech.framework.sdk.user.model.UserRoleDTO;

import java.util.List;

/**
 * @author tie yan
 */
public interface UserRoleApi {

    /**
     * 根据角色ID列表加载角色信息
     *
     * @param roleIds 角色ID列表
     * @return 角色信息列表
     */
    List<UserRoleDTO> loadByIds(List<String> roleIds);

    /**
     * 根据菜单ID列表和租户代码加载角色ID列表
     *
     * @param menuIds  菜单ID列表
     * @param tenantCode 租户代码
     * @return 角色ID列表
     */
    List<String> loadIdByMenuIds(List<String> menuIds, String tenantCode);

    /**
     * 根据菜单ID和租户代码加载角色ID
     *
     * @param menuId  菜单ID
     * @param tenantCode 租户代码
     * @return 角色ID
     */
    List<String> loadIdByMenuId(String menuId, String tenantCode);

}
