package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserDeptDTO", description = "用户部门")
public class UserDeptDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -4472789713561231877L;

    @Schema(name = "deptId", description = "部门编号")
    private String deptId;

    @Schema(name = "deptName", description = "部门名称")
    private String deptName;

    @Schema(name = "deptFullPath", description = "部门全路径")
    private String deptFullPath;

    @Schema(name = "managerId", description = "主管编号")
    private String managerId;

    @Schema(name = "managerName", description = "主管姓名")
    private String managerName;

    @Schema(name = "managerAvatar", description = "主管头像")
    private String managerAvatar;
}
