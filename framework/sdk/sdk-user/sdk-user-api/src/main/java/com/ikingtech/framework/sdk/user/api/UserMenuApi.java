package com.ikingtech.framework.sdk.user.api;

import java.util.List;

/**
 * @author tie yan
 */
public interface UserMenuApi {

    /**
     * 根据菜单编码列表和租户编码加载菜单ID列表
     * @param menuCodes 菜单编码列表
     * @param tenantCode 租户编码
     * @return 菜单ID列表
     */
    List<String> loadIdByCodes(List<String> menuCodes, String tenantCode);

    /**
     * 根据菜单编码和租户编码加载菜单ID
     * @param menuCode 菜单编码
     * @param tenantCode 租户编码
     * @return 菜单ID
     */
    String loadIdByCode(String menuCode, String tenantCode);
}
