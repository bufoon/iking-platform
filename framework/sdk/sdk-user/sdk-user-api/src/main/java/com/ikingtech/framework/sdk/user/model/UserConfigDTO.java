package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 用户配置信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "UserConfigDTO", description = "用户配置信息")
public class UserConfigDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1930613922009046752L;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "global", description = "是否全局配置")
    private Boolean global;

    @Schema(name = "configInfoList", description = "用户详细配置信息")
    private List<UserConfigInfoDTO> configInfoList;
}
