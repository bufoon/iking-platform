package com.ikingtech.framework.sdk.user.api;

import com.ikingtech.framework.sdk.user.model.UserDeptDTO;

import java.util.List;

/**
 * @author tie yan
 */
public interface UserDeptApi {

    /**
     * 根据部门ID列表加载部门信息
     *
     * @param deptIds 部门ID列表
     * @return 部门信息列表
     */
    List<UserDeptDTO> loadByIds(List<String> deptIds);
}
