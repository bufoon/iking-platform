package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 用户绑定信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "UserTenantBindDTO", description = "用户绑定信息")
public class UserTenantBindDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6911499016633223658L;

    @Schema(name = "userIds", description = "用户编号集合")
    private List<String> userIds;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;
}
