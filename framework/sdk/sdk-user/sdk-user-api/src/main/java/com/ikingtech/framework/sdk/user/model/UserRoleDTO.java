package com.ikingtech.framework.sdk.user.model;

import com.ikingtech.framework.sdk.enums.system.role.DataScopeTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserRoleDTO", description = "用户角色")
public class UserRoleDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -4472789713561231877L;

    @Schema(name = "roleId", description = "角色编号")
    private String roleId;

    @Schema(name = "roleName", description = "角色名称")
    private String roleName;

    @Schema(name = "roleDataScopeType", description = "数据权限类型")
    private DataScopeTypeEnum roleDataScopeType;

    @Schema(name = "roleDataScopeCodes", description = "数据权限码集合")
    private List<String> roleDataScopeCodes;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;
}
