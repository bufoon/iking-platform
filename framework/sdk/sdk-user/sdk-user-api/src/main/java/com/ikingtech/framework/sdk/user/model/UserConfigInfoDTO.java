package com.ikingtech.framework.sdk.user.model;

import com.ikingtech.framework.sdk.enums.system.user.UserConfigTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserConfigInfoDTO", description = "用户详细配置信息")
public class UserConfigInfoDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 7625957963354225462L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "type", description = "类型")
    private UserConfigTypeEnum type;

    @Schema(name = "value", description = "用户配置内容")
    private String value;
}
