package com.ikingtech.framework.sdk.post.rpc.api;

import com.ikingtech.framework.sdk.post.api.PostApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "PostApi", path = "/system/post")
public interface PostRpcApi extends PostApi {
}
