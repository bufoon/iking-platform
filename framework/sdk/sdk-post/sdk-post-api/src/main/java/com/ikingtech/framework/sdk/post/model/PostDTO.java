package com.ikingtech.framework.sdk.post.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 岗位信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "PostDTO", description = "岗位信息")
public class PostDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5606985776069675794L;

    @Schema(name = "id", description = "岗位编号")
    private String id;

    @Schema(name = "tenantCode", description = "租户编号")
    private String tenantCode;

    @Schema(name = "name", description = "岗位名称")
    private String name;

    @Schema(name = "remark", description = "岗位描述")
    private String remark;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    @Schema(name = "delFlag", description = "是否删除")
    private Boolean delFlag;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
