package com.ikingtech.framework.sdk.post.api;

/**
 * @author zhangqiang
 */
public interface PostUserApi {

    void removeUserPost(String postId);
}
