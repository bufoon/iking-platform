package com.ikingtech.framework.sdk.limit.config.configuration;

import com.ikingtech.framework.sdk.limit.config.properties.RateLimitProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author zhangqiang
 */
@EnableConfigurationProperties(RateLimitProperties.class)
public class RateLimitConfiguration {
}
