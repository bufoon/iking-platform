package com.ikingtech.framework.sdk.limit.gateway.configuration;

import com.ikingtech.framework.sdk.limit.config.properties.RateLimitProperties;
import com.ikingtech.framework.sdk.limit.embedded.RateLimiterService;
import com.ikingtech.framework.sdk.limit.gateway.filter.GatewayRateLimitFilter;
import org.springframework.context.annotation.Bean;

/**
 * @author zhangqiang
 */
public class GatewayLimitConfiguration {

    @Bean
    public GatewayRateLimitFilter rateLimitFilter(RateLimiterService rateLimiterService, RateLimitProperties rateLimitProperties) {
        return new GatewayRateLimitFilter(rateLimiterService, rateLimitProperties);
    }
}
