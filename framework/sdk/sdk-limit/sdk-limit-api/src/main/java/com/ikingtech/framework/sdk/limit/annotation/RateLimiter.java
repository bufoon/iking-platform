package com.ikingtech.framework.sdk.limit.annotation;

import java.lang.annotation.*;

/**
 * @author zhangqiang
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RateLimiter {
    /**
     * 限流时间,单位秒
     */
    int time();

    /**
     * 限流次数
     */
    int count();
}
