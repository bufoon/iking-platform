package com.ikingtech.framework.sdk.gray.configuration;

import com.ikingtech.framework.sdk.gray.GrayLoadBalancerFilter;
import org.springframework.cloud.gateway.config.conditional.ConditionalOnEnabledFilter;
import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.context.annotation.Bean;

/**
 * @author tie yan
 */
public class GrayConfiguration {

    @Bean
    @ConditionalOnEnabledFilter
    public GrayLoadBalancerFilter grayLoadBalancerFilter(LoadBalancerClientFactory clientFactory) {
        return new GrayLoadBalancerFilter(clientFactory);
    }
}
