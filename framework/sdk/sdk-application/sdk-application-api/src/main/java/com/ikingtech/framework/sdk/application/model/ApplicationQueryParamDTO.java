package com.ikingtech.framework.sdk.application.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 岗位信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApplicationQueryParamDTO", description = "应用信息查询参数")
public class ApplicationQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -2343948336022247396L;

    @Schema(name = "enabled", description = "是否启用")
    private Boolean enabled;
    
    @Schema(name = "name", description = "应用名称")
    private String name;

    @Schema(name = "remark", description = "应用描述")
    private String remark;

    @Schema(name = "status", description = "应用状态")
    private String status;
}
