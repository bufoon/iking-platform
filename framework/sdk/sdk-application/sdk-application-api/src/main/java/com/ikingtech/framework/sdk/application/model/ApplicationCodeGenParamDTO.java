package com.ikingtech.framework.sdk.application.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ScaffoldModelCodeGenParamDTO", description = "工程代码生成参数")
public class ApplicationCodeGenParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 219994345022285482L;

    @Schema(name = "modelIds", description = "模型编号集合")
    private List<String> modelIds;

    @Schema(name = "packageName", description = "包名")
    private String  packageName;
}
