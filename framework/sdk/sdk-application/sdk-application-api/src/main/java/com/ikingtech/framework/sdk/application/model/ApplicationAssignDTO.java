package com.ikingtech.framework.sdk.application.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ApplicationAssignDTO extends ApplicationDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -8679516818127654520L;

    @Schema(name = "assignToCurrentTenant", description = "应用说明")
    private Boolean assignToCurrentTenant;
}
