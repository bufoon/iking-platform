package com.ikingtech.framework.sdk.application.model;

import com.ikingtech.framework.sdk.enums.application.ApplicationPageCategoryEnum;
import com.ikingtech.framework.sdk.enums.application.ApplicationPageTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author Administrator
 */
@Data
@Schema(name = "ApplicationPageDTO", description = "应用页面信息")
public class ApplicationPageDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5809228775126960506L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "appId", description = "应用编号")
    private String appId;

    @Schema(name = "appCode", description = "应用标识")
    private String appCode;

    @Schema(name = "parentId", description = "父页面编号")
    private String parentId;

    @Schema(name = "name", description = "页面名称")
    private String name;

    @Schema(name = "icon", description = "页面图标")
    private String icon;

    @Schema(name = "link", description = "页面链接")
    private String link;

    @Schema(name = "component", description = "页面组件")
    private String component;

    @Schema(name = "json", description = "页面JSON")
    private String json;

    @Schema(name = "type", description = "页面类型")
    private ApplicationPageTypeEnum type;

    @Schema(name = "typeName", description = "页面类型名称")
    private String typeName;

    @Schema(name = "category", description = "页面类别")
    private ApplicationPageCategoryEnum category;

    @Schema(name = "categoryName", description = "页面类别名称")
    private String categoryName;

    @Schema(name = "sortOrder", description = "页面排序值")
    private Integer sortOrder;

    @Schema(name = "modelIds", description = "模型编号集合")
    private List<String> modelIds;

    @Schema(name = "modelIds", description = "模型编号集合")
    private List<ApplicationPageModelDTO> models;
}
