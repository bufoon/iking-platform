package com.ikingtech.framework.sdk.application.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ScaffoldPagePublishParamDTO", description = "表单发布信息")
public class ScaffoldPagePublishParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 3688746820421975036L;

    @Schema(name = "pageId", description = "表单编号")
    private String pageId;

    @Schema(name = "parentMenuId", description = "父菜单编号")
    private String parentMenuId;

    @Schema(name = "menuIcon", description = "菜单图标")
    private String menuIcon;
}
