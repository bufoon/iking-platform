package com.ikingtech.framework.sdk.application.model;

import com.ikingtech.framework.sdk.enums.application.ApplicationStatusEnum;
import com.ikingtech.framework.sdk.enums.application.ApplicationTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApplicationDTO", description = "应用信息")
public class ApplicationDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5144151975625884301L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "name", description = "应用名称")
    private String name;

    @Schema(name = "code", description = "应用标识")
    private String code;

    @Schema(name = "icon", description = "应用图标")
    private String icon;

    @Schema(name = "iconBackground", description = "应用图标背景色")
    private String iconBackground;

    @Schema(name = "remark", description = "应用说明")
    private String remark;

    @Schema(name = "indexPath", description = "应用首页地址")
    private String indexPath;

    @Schema(name = "type", description = "应用类型")
    private ApplicationTypeEnum type;

    @Schema(name = "typeName", description = "应用类型名称")
    private String typeName;

    @Schema(name = "status", description = "应用状态")
    private ApplicationStatusEnum status;

    @Schema(name = "statusName", description = "应用状态名称")
    private String statusName;

    @Schema(name = "latestVersion", description = "是否为最新版本")
    private Boolean latestVersion;

    @Schema(name = "versionNo", description = "版本号")
    private Integer versionNo;
}
