package com.ikingtech.framework.sdk.application.api;

import com.ikingtech.framework.sdk.application.model.ApplicationDTO;
import com.ikingtech.framework.sdk.application.model.ApplicationQueryParamDTO;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface ApplicationApi {

    /**
     * 新增应用
     *
     * @param application 应用信息
     * @return 应用编号
     */
    @PostRequest(order = 1, value = "/add", summary = "新增应用", description = "新增应用")
    R<ApplicationDTO> add(@Parameter(name = "application", description = "应用信息")
                          @RequestBody ApplicationDTO application);

    /**
     * 删除应用
     *
     * @param id 应用编号
     * @return 删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除应用", description = "删除应用")
    R<Object> delete(@Parameter(name = "id", description = "应用编号")
                     @RequestBody String id);

    /**
     * 更新应用
     *
     * @param application 应用信息
     * @return 更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新应用", description = "更新应用")
    R<Object> update(@Parameter(name = "application", description = "应用信息")
                     @RequestBody ApplicationDTO application);

    /**
     * 分页查询应用
     *
     * @param queryParam 查询条件
     * @return 应用列表
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询应用", description = "分页查询应用")
    R<List<ApplicationDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                                 @RequestBody ApplicationQueryParamDTO queryParam);

    /**
     * 全量查询应用
     *
     * @return 应用列表
     */
    @PostRequest(order = 5, value = "/list/all", summary = "全量查询应用", description = "全量查询应用")
    R<List<ApplicationDTO>> all();

    /**
     * 查询应用详情
     *
     * @param id 应用编号
     * @return 应用详情
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "查询应用详情", description = "查询应用详情")
    R<ApplicationDTO> detail(@Parameter(name = "id", description = "应用编号")
                             @RequestBody String id);

    /**
     * 发布应用
     *
     * @param id 应用编号
     * @return 发布结果
     */
    @PostRequest(order = 7, value = "/publish", summary = "发布应用", description = "发布应用")
    R<Object> publish(@Parameter(name = "id", description = "应用编号")
                      @RequestBody String id);
}
