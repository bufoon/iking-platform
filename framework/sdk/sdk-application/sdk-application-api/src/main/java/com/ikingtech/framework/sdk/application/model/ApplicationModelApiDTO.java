package com.ikingtech.framework.sdk.application.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ScaffoldModelApiDTO", description = "模型API信息")
public class ApplicationModelApiDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6625224118422000655L;

    @Schema(name = "method", description = "请求方法")
    private String method;

    @Schema(name = "name", description = "api名称")
    private String name;

    @Schema(name = "uri", description = "请求地址")
    private String uri;

    @Schema(name = "requestHandler", description = "请求处理")
    private String requestHandler;

    @Schema(name = "responseHandler", description = "响应处理")
    private String responseHandler;

    @Schema(name = "errorHandler", description = "错误处理")
    private String errorHandler;
}
