package com.ikingtech.framework.sdk.application.model.code;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ScaffoldProjectCodeGenParamDTO", description = "工程代码生成参数")
public class ApplicationProjectCodeGenParam implements Serializable {

    private static final long serialVersionUID = -8329824927872703004L;

    @Schema(name = "code", description = "工程标识")
    private String code;

    @Schema(name = "groupId", description = "groupId")
    private String  groupId;

    @Schema(name = "artifactId", description = "artifactId")
    private String artifactId;

    @Schema(name = "basePackageName", description = "基础包名")
    private String  basePackageName;

    @Schema(name = "author", description = "作者")
    private String author;
}
