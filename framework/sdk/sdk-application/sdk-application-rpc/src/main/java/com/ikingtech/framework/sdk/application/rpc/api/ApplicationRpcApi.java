package com.ikingtech.framework.sdk.application.rpc.api;

import com.ikingtech.framework.sdk.application.api.ApplicationApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "ApplicationApi", path = "/application")
public interface ApplicationRpcApi extends ApplicationApi {
}
