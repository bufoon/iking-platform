package com.ikingtech.framework.sdk.datasource.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "DatasourceTableDTO", description = "数据源表信息")
public class DatasourceTableDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -7924322925169708651L;

    @Schema(name = "name", description = "表名")
    private String name;

    @Schema(name = "description", description = "描述")
    private String description;

    @Schema(name = "fields", description = "表字段信息集合")
    private List<DatasourceTableFieldDTO> fields;
}
