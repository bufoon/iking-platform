package com.ikingtech.framework.sdk.datasource.model;

import com.ikingtech.framework.sdk.datasource.model.enums.DatasourceSchemaTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class DatasourceSchemaQueryParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -4703777074821385000L;

    private DatasourceSchemaTypeEnum type;

    private String jdbcUrl;

    private String username;

    private String password;
}
