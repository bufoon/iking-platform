package com.ikingtech.framework.sdk.datasource.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "DatasourceDTO", description = "数据源信息查询参数")
public class DatasourceQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 8767303210649373544L;

    @Schema(name = "name", description = "数据源名称")
    private String name;

    @Schema(name = "type", description = "数据源类型")
    private String type;

    @Schema(name = "ip", description = "数据源IP地址")
    private String ip;

    @Schema(name = "`port`", description = "端口")
    private String port;

    @Schema(name = "schemaName", description = "数据库名称")
    private String schemaName;

    @Schema(name = "remark", description = "备注")
    private String remark;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
