package com.ikingtech.framework.sdk.wechat.embedded.mini.resposne;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WechatMiniResponseQrcode extends WechatMiniResponse<Object> implements Serializable {

    @Serial
    private static final long serialVersionUID = 2882060251750673906L;

    private byte[] buffer;
}
