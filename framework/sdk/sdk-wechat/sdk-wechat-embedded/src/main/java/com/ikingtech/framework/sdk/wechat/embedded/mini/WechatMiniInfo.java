package com.ikingtech.framework.sdk.wechat.embedded.mini;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatMiniInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1681181670039129945L;

    private String id;

    private String appId;

    private String appSecret;

    private String name;
}
