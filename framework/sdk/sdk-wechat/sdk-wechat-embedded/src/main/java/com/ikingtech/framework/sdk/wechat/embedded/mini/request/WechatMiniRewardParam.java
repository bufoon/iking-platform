package com.ikingtech.framework.sdk.wechat.embedded.mini.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class WechatMiniRewardParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -9126839413842380773L;

    @JsonProperty(value = "nonce_str")
    private String nonceStr;

    private String sign;

    @JsonProperty(value = "mch_billno")
    private String mchBillNo;

    @JsonProperty(value = "mch_id")
    private String mchId;

    private String wxappid;

    @JsonProperty(value = "send_name")
    private String sendName;

    @JsonProperty(value = "re_openid")
    private String reOpenid;

    @JsonProperty(value = "total_amount")
    private Integer totalAmount;

    @JsonProperty(value = "total_num")
    private Integer totalNum;

    private String wishing;

    @JsonProperty(value = "client_ip")
    private String clientIp;

    @JsonProperty(value = "act_name")
    private String actName;

    private String remark;
}
