package com.ikingtech.framework.sdk.wechat.embedded.mini.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatMiniUrlLinkGenerateParam implements Serializable {

    private static final long serialVersionUID = 353745186874785371L;

    private String path;

    private String query;

    @JsonProperty(value = "expire_type")
    private Integer expireType;

    @JsonProperty(value = "expire_interval")
    private Integer expireInterval;
}
