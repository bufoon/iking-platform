package com.ikingtech.framework.sdk.wechat.embedded.office;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class WechatMiniMessageTemplateInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = -2497252653710326563L;

    private String templateId;

    private String templateTitle;

    private String content;

    private Integer type;

    private List<String> params;
}
