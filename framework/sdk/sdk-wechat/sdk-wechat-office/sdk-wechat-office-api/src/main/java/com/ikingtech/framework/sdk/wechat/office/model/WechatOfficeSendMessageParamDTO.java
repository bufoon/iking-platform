package com.ikingtech.framework.sdk.wechat.office.model;

import lombok.Data;

import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class WechatOfficeSendMessageParamDTO {

    private String id;

    private String messageTemplateId;

    private String openId;

    private String redirectTo;

    private Map<String, Object> param;
}
