package com.ikingtech.framework.sdk.wechat.office;

import com.ikingtech.framework.sdk.wechat.office.api.WechatOfficeApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "WechatMiniRpcApi", path = "/wechat/mini")
public interface WechatOfficeRpcApi extends WechatOfficeApi {
}
