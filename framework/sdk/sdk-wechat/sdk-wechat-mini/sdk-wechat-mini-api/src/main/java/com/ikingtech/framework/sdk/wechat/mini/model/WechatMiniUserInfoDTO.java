package com.ikingtech.framework.sdk.wechat.mini.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatMiniUserInfoDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8521224914229245348L;

    private String unionId;

    private String openid;

    private String sessionKey;
}
