package com.ikingtech.framework.sdk.wechat.mini.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.framework.sdk.wechat.mini.model.*;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface WechatMiniApi {

    /**
     * 添加微信小程序信息
     *
     * @param wechatMini 微信小程序信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加微信小程序信息", description = "添加微信小程序信息")
    R<String> add(@Parameter(name = "wechatMini", description = "微信小程序信息")
                  @RequestBody WechatMiniDTO wechatMini);

    /**
     * 删除微信小程序信息
     *
     * @param id 编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除微信小程序信息", description = "删除微信小程序信息")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新微信小程序信息
     *
     * @param wechatMini 微信小程序信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新微信小程序信息", description = "更新微信小程序信息")
    R<Object> update(@Parameter(name = "wechatMini", description = "微信小程序信息")
                     @RequestBody WechatMiniDTO wechatMini);

    /**
     * 分页查询微信小程序信息
     *
     * @param queryParam 查询条件
     * @return 返回分页结果
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询微信小程序信息", description = "分页查询微信小程序信息")
    R<List<WechatMiniDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                                @RequestBody WechatMiniQueryParamDTO queryParam);

    /**
     * 查询所有微信小程序信息
     *
     * @return 返回所有微信小程序信息
     */
    @PostRequest(order = 5, value = "/list/all", summary = "查询所有微信小程序信息", description = "查询所有微信小程序信息")
    R<List<WechatMiniDTO>> all();

    /**
     * 查询指定编号的微信小程序信息
     *
     * @param id 编号
     * @return 返回指定编号的微信小程序信息
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "查询指定编号的微信小程序信息", description = "查询指定编号的微信小程序信息")
    R<WechatMiniDTO> detail(@RequestBody String id);

    /**
     * 发送订阅消息
     *
     * @param sendMessageParam 发送订阅消息参数
     * @return 返回发送结果
     */
    @PostRequest(order = 7, value = "/message/send", summary = "发送订阅消息", description = "发送订阅消息")
    R<Object> sendSubscribeMessage(@RequestBody WechatMiniSendMessageParamDTO sendMessageParam);

    /**
     * 根据授权码获取用户信息
     *
     * @param exchangeCodeParam 交换码参数
     * @return 返回交换码结果
     */
    @PostRequest(order = 8, value = "/exchange-code", summary = "根据授权码获取用户信息", description = "根据授权码获取用户信息")
    R<WechatMiniUserInfoDTO> exchangeCode(@RequestBody WechatMiniExchangeCodeParamDTO exchangeCodeParam);

    /**
     * 根据授权码获取手机号
     *
     * @param queryParam 查询参数
     * @return 返回手机号
     */
    @PostRequest(order = 9, value = "/phone", summary = "根据授权码获取手机号", description = "根据授权码获取手机号")
    R<WechatMiniPhoneDTO> getPhoneByCode(@RequestBody WechatMiniPhoneQueryParamDTO queryParam);

    /**
     * 生成二维码
     *
     * @param generateParam 生成二维码参数
     * @return 返回二维码
     */
    @PostRequest(order = 10, value = "/qrcode", summary = "生成二维码", description = "生成二维码")
    R<String> qrcode(@RequestBody WechatMiniQrcodeGenerateParamDTO generateParam);

    /**
     * 生成链接
     *
     * @param generateParam 生成链接参数
     * @return 返回链接
     */
    @PostRequest(order = 11, value = "/url-link", summary = "生成链接", description = "生成链接")
    R<String> urlLink(@RequestBody WechatMiniUrlLinkGenerateParamDTO generateParam);

    /**
     * 查询订阅消息模板列表
     *
     * @param wechatMiniId 微信小程序ID
     * @return 返回订阅消息模板列表
     */
    @PostRequest(order = 12, value = "/subscribe-message-template/list", summary = "查询订阅消息模板列表", description = "查询订阅消息模板列表")
    R<List<WechatMiniSubscribeMessageTemplateDTO>> listSubscribeMessageTemplate(@RequestBody String wechatMiniId);
}
