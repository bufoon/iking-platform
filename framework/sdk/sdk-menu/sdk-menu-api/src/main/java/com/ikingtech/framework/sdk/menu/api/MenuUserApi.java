package com.ikingtech.framework.sdk.menu.api;

import java.util.List;

/**
 * @author tie yan
 */
public interface MenuUserApi {

    List<String> loadRoleId(String userId, String domain, String tenantCode, String appId);
}
