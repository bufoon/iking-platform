package com.ikingtech.framework.sdk.menu.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "RemoveAppMenuParamDTO", description = "移除菜单参数")
public class RemoveAppMenuParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8882771717096677573L;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "appCode", description = "应用标识")
    private String appCode;
}
