package com.ikingtech.framework.sdk.menu.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.menu.MenuJumpTypeEnum;
import com.ikingtech.framework.sdk.enums.system.menu.MenuTypeEnum;
import com.ikingtech.framework.sdk.enums.system.menu.MenuViewTypeEnum;
import com.ikingtech.framework.sdk.enums.system.menu.RoleMenuAssignedStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 菜单信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "MenuDTO", description = "菜单信息")
public class MenuDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -4189424329647651747L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "parentId", description = "父菜单编号")
    private String parentId;

    @Schema(name = "tenantCode", description = "所属租户")
    private String tenantCode;

    @Schema(name = "domainCode", description = "所属域")
    private String domainCode;

    @Schema(name = "appCode", description = "所属应用编号")
    private String appCode;

    @Schema(name = "component", description = "是否为组件")
    private String component;

    @Schema(name = "framework", description = "是否为框架路由")
    private Boolean framework;

    @NotBlank(message = "menuName")
    @Length(max = 32, message = "menuName")
    @Schema(name = "name", description = "菜单名称")
    private String name;

    @Schema(name = "pagePath", description = "页面地址")
    private String pagePath;

    @Length(max = 64, message = "menuEuName")
    @Schema(name = "euName", description = "菜单英文名称")
    private String euName;

    @Schema(name = "permissionCode", description = "菜单权限标识")
    private String permissionCode;

    @Schema(name = "icon", description = "菜单图标")
    private String icon;

    @Schema(name = "activeIcon", description = "菜单激活时显示的图标")
    private String activeIcon;

    @Schema(name = "logo", description = "应用图标，可使用菜单图标配置或上传应用图标文件")
    private String logo;

    @Schema(name = "link", description = "外部网页跳转链接")
    private String link;

    @Schema(name = "iframe", description = "内嵌网页链接")
    private String iframe;

    @Schema(name = "defaultOpened", description = "是否")
    private Boolean defaultOpened;

    @Schema(name = "permanent", description = "是否常驻标签页")
    private Boolean permanent;

    @Schema(name = "menuViewType", description = "菜单可视类型")
    private MenuViewTypeEnum menuViewType;

    @Schema(name = "menuViewTypeName", description = "菜单可视类型名称")
    private String menuViewTypeName;

    @Schema(name = "menuType", description = "菜单类型")
    private MenuTypeEnum menuType;

    @Schema(name = "menuTypeName", description = "菜单类型名称")
    private String menuTypeName;

	@Schema(name = "jumpType", description = "菜单跳转类型")
	private MenuJumpTypeEnum jumpType;

	@Schema(name = "jumpTypeName", description = "菜单跳转类型名称")
	private String jumpTypeName;

    @Schema(name = "keepAlive", description = "路由缓冲")
    private Boolean keepAlive;

    @Schema(name = "fullPath", description = "菜单全路径，@分割")
    private String fullPath;

    @Schema(name = "visible", description = "菜单是否显示")
    private Boolean visible;

    @Schema(name = "autoRender", description = "是否自动渲染页面，自定义表单时指定")
    private Boolean autoRender;

    @Schema(name = "sidebar", description = "是否在侧边栏导航中展示")
    private Boolean sidebar;

    @Schema(name = "breadcrumb", description = "是否在面包屑导航中展示")
    private Boolean breadcrumb;

    @Schema(name = "activeMenu", description = "侧边栏高亮路由完整地址")
    private String activeMenu;

    @Schema(name = "cache", description = "页面缓存，内容为空时表示不缓存，内容为permanent时表示永久缓存，内容为其他时表示跳转到指定页面是缓存当前页面")
    private String cache;

    @Schema(name = "noCache", description = "清除页面缓存")
    private String noCache;

    @Schema(name = "badge", description = "导航徽标，内容为空时表示无徽标，内容为point时表示点形徽标，内容为其他时表示徽标文本内容，如果是数字字符串，则为0时不显示徽标")
    private String badge;

    @Schema(name = "copyright", description = "是否显示版权信息")
    private Boolean copyright;

    @Schema(name = "paddingBottom", description = "页面和底部的距离")
    private String paddingBottom;

    @Schema(name = "whitelist", description = "路由白名单，白名单内的路由不受权限控制")
    private String whitelist;

    @Schema(name = "roleAssigned", description = "角色管理页面查询角色菜单时使用，其他场合下忽略")
    private RoleMenuAssignedStatusEnum roleAssigned;

    @Length(max = 200, message = "menuRemark")
    @Schema(name = "remark", description = "菜单描述")
    private String remark;

	@Schema(name = "排序值")
	private Integer sortOrder;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
