package com.ikingtech.framework.sdk.meun.rpc.api;

import com.ikingtech.framework.sdk.menu.api.MenuApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "MenuRpcApi", path = "/system/menu")
public interface MenuRpcApi extends MenuApi {
}
