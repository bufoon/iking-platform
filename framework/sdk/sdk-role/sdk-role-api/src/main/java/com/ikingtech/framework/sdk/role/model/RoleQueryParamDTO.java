package com.ikingtech.framework.sdk.role.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 角色信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "RoleQueryParamDTO", description = "角色信息查询参数")
public class RoleQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 8767303210649373544L;

    @Schema(name = "name", description = "角色名称")
    private String name;

    @Schema(name = "dataScopeType", description = "数据权限类型")
    private String dataScopeType;

    @Schema(name = "remark", description = "角色描述")
    private String remark;
}
