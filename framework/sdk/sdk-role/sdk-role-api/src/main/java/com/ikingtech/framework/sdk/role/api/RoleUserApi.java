package com.ikingtech.framework.sdk.role.api;

import java.util.List;

/**
 * @author zhangqiang
 */
public interface RoleUserApi {

    void removeUserRole(String roleId);

    void bindUserRole(String roleId, List<String> userIds);

    void unbindUserRole(String roleId, List<String> userIds);
}
