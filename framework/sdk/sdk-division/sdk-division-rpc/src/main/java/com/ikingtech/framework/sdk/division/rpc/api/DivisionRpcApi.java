package com.ikingtech.framework.sdk.division.rpc.api;

import com.ikingtech.framework.sdk.division.api.DivisionApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "DivisionRpcApi", path = "/system/division")
public interface DivisionRpcApi extends DivisionApi {
}
