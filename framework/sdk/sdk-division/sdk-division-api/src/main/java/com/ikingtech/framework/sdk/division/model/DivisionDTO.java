package com.ikingtech.framework.sdk.division.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.division.DivisionLevelEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 行政区划信息
 * @author tie yan
 */
@Data
@Schema(name = "DivisionDTO", description = "行政区划信息")
public class DivisionDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6029782571539770702L;

    @Schema(name = "id", description = "行政区划编号")
    private String id;

    @Schema(name = "parentId", description = "父级行政区划编号")
    private String parentId;

    @NotBlank(message = "divisionNo")
    @Length(max = 32, message = "divisionNo")
    @Schema(name = "no", description = "行政区划编码")
    private String no;

    @NotBlank(message = "divisionName")
    @Length(max = 32, message = "divisionName")
    @Schema(name = "name", description = "行政区划名称")
    private String name;

    @Schema(name = "parentNo", description = "上级行政区划编号")
    private String parentNo;

    @Schema(name = "fullPath", description = "行政区划全路径")
    private String fullPath;

    @Schema(name = "divisionLevel", description = "行政区划级别")
    private DivisionLevelEnum divisionLevel;

    @Schema(name = "levelName", description = "级别名称")
    private String levelName;

    @Schema(name = "provincialCapital", description = "是否省会城市")
    private Boolean provincialCapital;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    @Schema(name = "delFlag", description = "是否删除")
    private Boolean delFlag;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
