package com.ikingtech.framework.sdk.workbench.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 岗位信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "TodoTaskQueryParamDTO", description = "待办任务信息查询参数")
public class TodoTaskQueryParamDTO extends PageParam implements Serializable {

    private static final long serialVersionUID = 8767303210649373544L;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "summary", description = "待办摘要")
    private String summary;
}
