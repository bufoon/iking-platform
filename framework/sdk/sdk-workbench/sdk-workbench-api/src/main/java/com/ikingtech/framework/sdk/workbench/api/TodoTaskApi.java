package com.ikingtech.framework.sdk.workbench.api;

import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.framework.sdk.workbench.model.TodoTaskDTO;
import com.ikingtech.framework.sdk.workbench.model.TodoTaskDeleteBatchParamDTO;
import com.ikingtech.framework.sdk.workbench.model.TodoTaskQueryParamDTO;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface TodoTaskApi {

    /**
     * 添加待办任务
     *
     * @param todoTask 待办任务信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加待办任务", description = "添加待办任务")
    R<String> add(@Parameter(name = "todoTask", description = "待办任务信息")
                  @RequestBody TodoTaskDTO todoTask);

    /**
     * 批量添加待办任务
     *
     * @param todoTasks 待办任务信息
     * @return 返回添加结果
     */
    @PostRequest(order = 2, value = "/add/batch", summary = "批量添加待办任务", description = "批量添加待办任务")
    R<Object> addBatch(@Parameter(name = "todoTasks", description = "待办任务信息")
                       @RequestBody BatchParam<TodoTaskDTO> todoTasks);

    /**
     * 删除待办任务
     *
     * @param id 待删除任务的编号
     * @return 返回删除结果
     */
    @PostRequest(order = 3, value = "/delete", summary = "删除待办任务", description = "删除待办任务")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 批量删除待办任务
     *
     * @param deleteParam 批量删除参数
     * @return 返回删除结果
     */
    @PostRequest(order = 4, value = "/delete/batch", summary = "批量删除待办任务", description = "批量删除待办任务")
    R<Object> deleteBatch(@Parameter(name = "deleteParam", description = "批量删除参数")
                          @RequestBody TodoTaskDeleteBatchParamDTO deleteParam);

    /**
     * 更新待办任务
     *
     * @param todoTask 待更新的待办任务信息
     * @return 返回更新结果
     */
    @PostRequest(order = 5, value = "/update", summary = "更新待办任务", description = "更新待办任务")
    R<Object> update(@Parameter(name = "todoTask", description = "待办任务信息")
                     @RequestBody TodoTaskDTO todoTask);

    /**
     * 分页查询待办任务
     *
     * @param queryParam 查询条件
     * @return 返回待办任务列表
     */
    @PostRequest(order = 6, value = "/list/page", summary = "分页查询待办任务", description = "分页查询待办任务")
    R<List<TodoTaskDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                              @RequestBody TodoTaskQueryParamDTO queryParam);

    /**
     * 查询全部待办任务
     *
     * @return 返回全部待办任务列表
     */
    @PostRequest(order = 7, value = "/list/all", summary = "查询全部待办任务", description = "查询全部待办任务")
    R<List<TodoTaskDTO>> all();

    /**
     * 查询单个待办任务
     *
     * @param id 待查询任务的编号
     * @return 返回待办任务信息
     */
    @PostRequest(order = 8, value = "/detail/id", summary = "查询单个待办任务", description = "查询单个待办任务")
    R<TodoTaskDTO> detail(@Parameter(name = "id", description = "编号")
                          @RequestBody String id);
}
