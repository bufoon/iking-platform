package com.ikingtech.framework.sdk.message.model.rpc;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 消息模板标识信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "MessageBusinessTypeQueryParamDTO", description = "消息业务类型信息")
public class MessageBusinessTypeQueryParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8506381701223479030L;

    @Schema(name = "excludeAllTemplateConfigured", description = "排除所有模板已配置的业务模块")
    private Boolean excludeAllTemplateConfigured;

    @Schema(name = "excludeNonTemplateConfigured", description = "排除所有模板都未配置的业务模块")
    private Boolean excludeNonTemplateConfigured;

    @Schema(name = "businessName", description = "消息业务类型名称")
    private String businessName;
}
