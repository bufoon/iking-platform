package com.ikingtech.framework.sdk.message.model.rpc;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class MessageChannelDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = 477174126307745018L;

    /**
     * 渠道类型
     */
    private MessageSendChannelEnum channel;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 渠道编号
     */
    private String channelId;

    /**
     * 渠道模板编号
     */
    private String channelTemplateId;

    /**
     * 是否展示消息提醒
     */
    private Boolean showNotification;

    /**
     * 消息跳转链接
     */
    private List<MessageRedirectDefinition> redirectDefinitions;
}
