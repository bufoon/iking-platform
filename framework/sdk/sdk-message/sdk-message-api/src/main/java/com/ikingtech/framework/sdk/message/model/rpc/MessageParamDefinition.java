package com.ikingtech.framework.sdk.message.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class MessageParamDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = -1160512517500611003L;

    /**
     * 参数名称
     */
    private String paramName;

    /**
     * 参数描述
     */
    private String paramDescription;

    /**
     * 映射参数名
     */
    private String mappedParamName;
}
