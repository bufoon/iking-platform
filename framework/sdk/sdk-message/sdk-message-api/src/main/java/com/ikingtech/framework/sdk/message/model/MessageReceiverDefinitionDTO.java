package com.ikingtech.framework.sdk.message.model;

import com.ikingtech.framework.sdk.enums.message.MessageReceiverTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "MessageReceiverDefinitionDTO", description = "消息接收者定义信息")
public class MessageReceiverDefinitionDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4779840980164987228L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "templateId", description = "所属模板编号")
    private String templateId;

    @Schema(name = "receiverType", description = "接收者类型")
    private MessageReceiverTypeEnum receiverType;

    @Schema(name = "receiverParamName", description = "接收者参数名称")
    private String receiverParamName;
}