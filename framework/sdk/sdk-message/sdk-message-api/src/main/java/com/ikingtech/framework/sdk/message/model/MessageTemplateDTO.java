package com.ikingtech.framework.sdk.message.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(name = "MessageTemplateDTO", description = "消息模板扩展信息")
public class MessageTemplateDTO extends MessageTemplateBasicDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6191932845188014770L;

    @Schema(name = "receiverDefinitions", description = "消息接收者定义信息集合")
    private List<MessageReceiverDefinitionDTO> receiverDefinitions;

    @Schema(name = "channelDefinitions", description = "消息发送渠道信息集合")
    private List<MessageChannelDefinitionDTO> channelDefinitions;
}
