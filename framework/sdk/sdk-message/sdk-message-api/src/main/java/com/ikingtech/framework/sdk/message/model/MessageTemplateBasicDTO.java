package com.ikingtech.framework.sdk.message.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "MessageTemplateBasicDTO", description = "消息模板基础信息")
public class MessageTemplateBasicDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2929437669990001024L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "businessName", description = "所属业务类型")
    private String businessName;

    @Schema(name = "businessKey", description = "所属业务类型标识")
    private String businessKey;

    @Schema(name = "messageTemplateKey", description = "消息标识")
    private String messageTemplateKey;

    @Schema(name = "messageTemplateTitle", description = "消息标题")
    private String messageTemplateTitle;

    @Schema(name = "channelNames", description = "消息推送渠道名称，多个名称用逗号分隔")
    private String channelNames;

    @Schema(name = "configured", description = "是否已配置")
    private Boolean configured;

    @Schema(name = "configurable", description = "能否配置")
    private Boolean configurable;
}
