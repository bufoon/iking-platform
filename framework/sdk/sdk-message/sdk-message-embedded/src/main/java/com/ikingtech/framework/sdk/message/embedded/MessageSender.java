package com.ikingtech.framework.sdk.message.embedded;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.message.embedded.annotation.MessageTemplate;
import com.ikingtech.framework.sdk.message.model.rpc.MessageSendParam;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentProxy;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.DEFAULT_TENANT_CODE;

/**
 * @author tie yan
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MessageSender {

    public static R<Object> send(Object body) {
        return send(body, DEFAULT_TENANT_CODE, new ArrayList<>());
    }

    public static R<Object> send(Object body, String tenantCode) {
        return send(body, tenantCode, new ArrayList<>());
    }

    public static R<Object> send(Object body, String tenantCode, List<String> ignoreUserIds) {
        MessageSendParam sendParam = new MessageSendParam();
        sendParam.setTenantCode(tenantCode);

        MessageTemplate annotation = body.getClass().getAnnotation(MessageTemplate.class);
        sendParam.setMessageTemplateKey(annotation.key());
        sendParam.setIgnoreUserIds(ignoreUserIds);
        sendParam.setMessage(body);
        return send(sendParam);
    }

    private static R<Object> send(MessageSendParam messageSendParam) {
        return FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.MESSAGE_SEND, messageSendParam);
    }
}
