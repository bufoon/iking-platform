package com.ikingtech.framework.sdk.message.embedded.runner;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.message.api.MessageTemplateApi;
import com.ikingtech.framework.sdk.message.model.rpc.MessageTemplateBeanDefinitionReportParam;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author tie yan
 */
@Service
@RequiredArgsConstructor
public class MessageTemplateReportAgentRunner implements FrameworkAgentRunner {

    private final MessageTemplateApi api;

    /**
     * 执行消息体注册信息
     *
     * @param data 请求数据
     * @return 执行结果
     */
    @Override
    public R<Object> run(Object data) {
        MessageTemplateBeanDefinitionReportParam reportParam = (MessageTemplateBeanDefinitionReportParam) data;
        this.api.report(reportParam);
        return R.ok();
    }

    /**
     * 获取客户端类型
     *
     * @return 客户端类型
     */
    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.MESSAGE_TEMPLATE_REPORT;
    }
}
