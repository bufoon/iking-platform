package com.ikingtech.framework.sdk.attachment.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "AttachmentDTO", description = "附件")
public class AttachmentDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1769249905465343378L;

    @Schema(name = "businessId", description = "业务编号")
    private String businessId;

    @Schema(name = "files", description = "附件文件集合")
    private List<AttachmentFileDTO> files;
}
