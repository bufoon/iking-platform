package com.ikingtech.framework.sdk.attachment.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "AttachmentQueryParamDTO", description = "附件查询参数")
public class AttachmentQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 3748571109846477088L;

    @Schema(name = "ids", description = "编号集合")
    private List<String> ids;

    @Schema(name = "businessId", description = "业务编号")
    private String businessId;

    @Schema(name = "originName", description = "文件名")
    private String originName;

    @Schema(name = "type", description = "类型")
    private String type;
}
