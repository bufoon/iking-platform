package com.ikingtech.framework.sdk.job.embedded.annotation;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Job {

    /**
     * 定时任务标识，服务内唯一
     * 创建定时任务时请指定该标识，用户任务执行时的回调
     *
     * @return 任务标识
     */
    String handler();
}
