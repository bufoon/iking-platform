package com.ikingtech.framework.sdk.job.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class JobExecuteInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = -3842420117889636238L;

    /**
     * 定时任务执行流水号
     */
    private String serialNo;

    /**
     * 执行客户端标识
     */
    private String executorClientId;

    /**
     * 执行器标识
     */
    private String executorHandler;

    /**
     * 执行参数
     */
    private String executeParam;

    /**
     * 定时任务所属租户
     */
    private String tenantCode;
}
