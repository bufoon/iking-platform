package com.ikingtech.framework.sdk.label.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "LabelAssignDTO", description = "标签分配信息")
public class LabelAssignDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -9034299438263433922L;

    @Schema(name = "labelId", description = "标签编号")
    private List<String> labelIds;

    @Schema(name = "businessId", description = "业务数据编号")
    private String businessId;
}
