package com.ikingtech.framework.sdk.label.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "LabelQueryParamDTO", description = "查询参数")
public class LabelQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -2032409331246679689L;

    @Schema(name = "name", description = "标签名称，分页查询时使用")
    private String name;

}
