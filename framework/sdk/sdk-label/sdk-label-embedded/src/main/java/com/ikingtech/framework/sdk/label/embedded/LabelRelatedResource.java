package com.ikingtech.framework.sdk.label.embedded;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "LabelRelationDTO", description = "标签关联资源信息")
public class LabelRelatedResource implements Serializable {

    @Serial
    private static final long serialVersionUID = 9017577275628591594L;

    @Schema(name = "id", description = "关联资源编号")
    private String id;

    @Schema(name = "name", description = "关联资源名称")
    private String name;
}
