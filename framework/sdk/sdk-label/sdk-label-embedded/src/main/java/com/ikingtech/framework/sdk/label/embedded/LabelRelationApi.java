package com.ikingtech.framework.sdk.label.embedded;

import com.ikingtech.framework.sdk.base.model.PageResult;

/**
 * @author tie yan
 */
public interface LabelRelationApi {

    String businessName();

    PageResult<LabelRelatedResource> relations(LabelRelationResourceQueryParam queryParam);
}
