package com.ikingtech.framework.sdk.cache.constants;

import com.ikingtech.framework.sdk.utils.Tools;

import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.TOKEN_PREFIX;

/**
 * 缓存常量
 *
 * @author tie yan
 */
public interface CacheConstants {

    /**
     * 数据权限信息缓存
     */
    String USER_AUTH_DETAILS = "user_auth_details";

    /**
     * 用户配置信息缓存
     */
    String USER_CONFIG = "user_config";

    /**
     * 访问令牌
     */
    String ACCESS_TOKEN = "access_token";

    /**
     * 已登录用户信息
     */
    String LOGIN_USER = "login_user";

    /**
     * 身份信息扩展上报服务名
     */
    String IDENTITY_EXTENSION_REPORTER = "identity_extension_reporter";

    /**
     * 审批回调服务名
     */
    String APPROVE_PROCESS_CALLBACK = "approve_callback";

    /**
     * 系统参数信息缓存
     */
    String SYSTEM_VARIABLE = "system_variable";

    /**
     * 非正常状态租户缓存前缀
     */
    String ABNORMAL_TENANT = "abnormal_tenants";

    /**
     * 接收用户身份信息推送的服务名
     */
    String USER_EVENT_REMINDER = "user_event_reminder";
    String VALIDATE_IDENTITY = "validate_identity";

    static String accessTokenKeyFormat(String token) {
        return Tools.Str.join(CacheConstants.ACCESS_TOKEN, token, ":");
    }

    static String accessTokenKeyFormatWithoutPrefix(String tokenWithPrefix) {
        if (Tools.Str.isBlank(tokenWithPrefix) ||
                !tokenWithPrefix.startsWith(TOKEN_PREFIX)) {
            return tokenWithPrefix;
        }
        return Tools.Str.join(CacheConstants.ACCESS_TOKEN, tokenWithPrefix.replace(TOKEN_PREFIX, ""), ":");
    }

    static String loginUserFormat(String userId, String endpoint) {
        return Tools.Str.join(Tools.Coll.newList(CacheConstants.LOGIN_USER, userId, endpoint), ":");
    }

    static String userAuthDetailsFormat(String username, String tenantCode) {
        return Tools.Str.isBlank(tenantCode) ? Tools.Str.EMPTY : Tools.Str.join(Tools.Coll.newList(CacheConstants.USER_AUTH_DETAILS, username, tenantCode), ":");
    }

    static String userConfigFormat(String userId, String tenantCode) {
        return Tools.Str.isBlank(tenantCode) ? Tools.Str.EMPTY : Tools.Str.join(Tools.Coll.newList(CacheConstants.USER_CONFIG, userId, tenantCode), ":");
    }

    static String systemVariableFormat(String tenantCode) {
        return Tools.Str.isBlank(tenantCode) ? Tools.Str.EMPTY : Tools.Str.join(CacheConstants.SYSTEM_VARIABLE, tenantCode, ":");
    }

    static String validatedIdentityFormat(String userId) {
        return Tools.Str.join(CacheConstants.VALIDATE_IDENTITY, userId, ":");
    }
}
