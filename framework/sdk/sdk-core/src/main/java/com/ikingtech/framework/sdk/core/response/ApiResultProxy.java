package com.ikingtech.framework.sdk.core.response;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ApiResultProxy implements Serializable {

    @Serial
    private static final long serialVersionUID = -673507442496947217L;

    private static ApiResult result;

    public static void accept(ApiResult result) {
        ApiResultProxy.result = result;
    }

    public static ApiResult result() {
        return result;
    }
}
