package com.ikingtech.framework.sdk.core.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageResult;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;


/**
 * 统一响应
 *
 * @author tie yan
 * @param <T> 响应数据泛型
 */
@Data
public class R<T> {

    /**
     * 响应码
     */
    private int code = 0;

    /**
     * 响应码
     */
    private String msg;

    /**
     * 响应码
     */
    private T data;

    /**
     * 响应码
     */
    private long pages;

    /**
     * 响应码
     */
    private long total;

    /**
     * 响应码
     */
    private boolean success;

    /**
     * 响应码
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime timestamp;

    public static <T> R<T> ok() {
        return ApiResultProxy.result().ok();
    }

    public static <T> R<T> ok(T data) {
        return ApiResultProxy.result().ok(data);
    }

    public static <T> R<List<T>> ok(PageResult<T> data) {
        return ok(data.getPages(), data.getTotal(), data.getData());
    }

    public static <T> R<List<T>> ok(long pages, long total, List<T> data) {
        return ApiResultProxy.result().ok(pages, total, data);
    }

    public static <T> R<T> failed() {
        return ApiResultProxy.result().failed();
    }

    public static <T> R<T> failed(String msg) {
        return ApiResultProxy.result().failed(msg);
    }
}
