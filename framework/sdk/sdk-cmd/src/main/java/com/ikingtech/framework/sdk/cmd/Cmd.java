package com.ikingtech.framework.sdk.cmd;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;

/**
 * @author tie yan
 */
@Slf4j
public class Cmd {

    public boolean execute(List<String> cmd) {
        ProcessBuilder processBuilder = new ProcessBuilder().command(cmd).inheritIO();
        processBuilder.redirectErrorStream(true);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.appendTo(CmdTracerManager.get()));
        int existCode = 0;
        try {
            existCode = processBuilder.start().waitFor();
        } catch (InterruptedException | IOException e) {
            log.error("等待进程结束时发生异常！[{}][{}]", cmd, e.getMessage());
            Thread.currentThread().interrupt();
        }
        return 0 == existCode;
    }
}
