package com.ikingtech.framework.sdk.dict.rpc.api;

import com.ikingtech.framework.sdk.dict.api.DictApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "DictRpcApi", path = "/system/dict")
public interface DictRpcApi extends DictApi {
}
