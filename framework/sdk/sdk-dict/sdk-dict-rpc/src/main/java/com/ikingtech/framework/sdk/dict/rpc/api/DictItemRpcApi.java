package com.ikingtech.framework.sdk.dict.rpc.api;

import com.ikingtech.framework.sdk.dict.api.DictItemApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "DictItemRpcApi", path = "/system/dict/item")
public interface DictItemRpcApi extends DictItemApi {
}
