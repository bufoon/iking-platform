package com.ikingtech.framework.sdk.dict.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.dict.model.DictDTO;
import com.ikingtech.framework.sdk.dict.model.DictQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface DictApi {

    /**
     * 添加字典信息
     *
     * @param dict 字典信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加字典信息", description = "添加字典信息")
    R<String> add(@Parameter(name = "dict", description = "字典信息")
                  @RequestBody DictDTO dict);

    /**
     * 删除字典信息
     *
     * @param id 编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除字典信息", description = "删除字典信息")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新字典信息
     *
     * @param dict 字典信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新字典信息", description = "更新字典信息")
    R<Object> update(@Parameter(name = "dict", description = "字典信息")
                     @RequestBody DictDTO dict);

    /**
     * 分页查询字典信息
     *
     * @param queryParam 查询条件
     * @return 返回分页结果
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询字典信息", description = "分页查询字典信息")
    R<List<DictDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                          @RequestBody DictQueryParamDTO queryParam);

    /**
     * 查询所有字典信息
     *
     * @return 返回所有字典信息
     */
    @PostRequest(order = 5, value = "/list/all", summary = "查询所有字典信息", description = "查询所有字典信息")
    R<List<DictDTO>> all();

    /**
     * 根据编号查询字典信息
     *
     * @param id 编号
     * @return 返回字典信息
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "根据编号查询字典信息", description = "根据编号查询字典信息")
    R<DictDTO> detail(@Parameter(name = "id", description = "编号")
                      @RequestBody String id);
}
