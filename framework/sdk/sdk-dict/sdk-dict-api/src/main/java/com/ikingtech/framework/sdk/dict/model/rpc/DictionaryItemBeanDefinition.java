package com.ikingtech.framework.sdk.dict.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class DictionaryItemBeanDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = 4161100552353988421L;

    /**
     * 主键
     */
    private String id;

    /**
     * 父字典项编号
     */
    private String parentId;

    /**
     * 租户标识
     */
    private String tenantCode;

    /**
     * 字典项值
     */
    private String value;

    /**
     * 字典项名称
     */
    private String label;

    /**
     * 所属字典编号
     */
    private String dictId;

    /**
     * 排序值
     */
    private Integer sortOrder;

    /**
     * 字典项描述
     */
    private String remark;
}
