package com.ikingtech.framework.sdk.dict.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "DictQueryParamDTO", description = "查询参数")
public class DictQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 7187513078088183210L;

    @Schema(name = "name", description = "字典名称")
    private String name;

    @Schema(name = "remark", description = "备注信息")
    private String remark;

    @Schema(name = "type", description = "字典类型")
    private String type;
}
