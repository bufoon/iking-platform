package com.ikingtech.framework.sdk.country.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.country.ContinentEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "CountryDTO", description = "国家信息")
public class CountryDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6029782571539770702L;

    @Schema(name = "id", description = "主键")
    private String id;

    @NotBlank(message = "countryCode")
    @Schema(name = "code", description = "国家编码")
    private String code;

    @NotBlank(message = "countryFullCode")
    @Schema(name = "fullCode", description = "国家完整编码")
    private String fullCode;

    @NotBlank(message = "countryName")
    @Schema(name = "name", description = "国家名称")
    private String name;

    @NotBlank(message = "countryEuName")
    @Schema(name = "euName", description = "国家英文名称")
    private String euName;

    @NotBlank(message = "countryContinent")
    @Schema(name = "continent", description = "所属洲")
    private ContinentEnum continent;

    @Schema(name = "continentName", description = "所属洲名称")
    private String continentName;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    @Schema(name = "delFlag", description = "是否删除")
    private Boolean delFlag;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;
}
