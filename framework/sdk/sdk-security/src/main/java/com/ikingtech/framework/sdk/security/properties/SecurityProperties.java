package com.ikingtech.framework.sdk.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;


/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".security")
public class SecurityProperties implements Serializable {

    @Serial
    private static final long serialVersionUID = 9156438866875224115L;

    private boolean enable;

    private List<String> ignore;
}
