package com.ikingtech.framework.sdk.oss.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "OssFileQueryParamDTO", description = "文件查询条件")
public class OssFileQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -6269537653889301884L;

    @Schema(name = "name", description = "文件名")
    private String name;

    @Schema(name = "dirName", description = "文件目录名称")
    private String dirName;
}
