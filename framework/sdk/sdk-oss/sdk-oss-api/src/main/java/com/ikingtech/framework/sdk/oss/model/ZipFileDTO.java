package com.ikingtech.framework.sdk.oss.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author li longlong
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(name = "ZipFileDTO", description = "ZIP文件上传返回类")
public class ZipFileDTO extends OssFileDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6884940931394888941L;

    @Schema(name = "zipRelativePath", description = "文件在ZIP文件中的全路径")
    private String zipRelativePath;
}
