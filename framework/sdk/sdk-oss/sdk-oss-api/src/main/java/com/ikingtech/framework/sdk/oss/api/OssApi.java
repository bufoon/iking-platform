package com.ikingtech.framework.sdk.oss.api;

import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.oss.model.OssFileDTO;
import com.ikingtech.framework.sdk.oss.model.OssFileQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * @author tie yan
 */
public interface OssApi {

    /**
     * 删除文件
     */
    @PostRequest(order = 1,value = "/delete", summary = "删除文件", description = "删除文件")
    R<Object> delete(@Parameter(name = "fileUrl", description = "文件相对路径")
                     @RequestParam("fileUrl") String fileUrl);
    
    /**
     * 更新文件
     */
    @PostRequest(order = 2,value = "/file/update", summary = "更新文件", description = "更新文件")
    R<OssFileDTO> update(@Parameter(name = "file", description = "文件信息")
                        @RequestBody OssFileDTO file);
    
    /**
     * 分页查询文件
     */
    @PostRequest(order = 3,value = "/file/list/page", summary = "分页查询文件", description = "分页查询文件")
    R<List<OssFileDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                           @RequestBody OssFileQueryParamDTO queryParam);
    
    /**
     * 查询所有文件
     */
    @PostRequest(order = 4,value = "/file/list/all", summary = "查询所有文件", description = "查询所有文件")
    R<List<OssFileDTO>> all();
    
    /**
     * 根据文件编号查询文件
     */
    @PostRequest(order = 5,value = "/file/list/ids", summary = "根据文件编号查询文件", description = "根据文件编号查询文件")
    R<List<OssFileDTO>> listByIds(@Parameter(name = "ids", description = "文件编号")
                                @RequestBody BatchParam<String> ids);
    
    /**
     * 上传文件
     */
    @PostRequest(order = 6,value = "/upload", summary = "上传文件", description = "上传文件")
    R<OssFileDTO> upload(@Parameter(name = "file", description = "文件对象")
                        @RequestPart("file") MultipartFile file,
                        @Parameter(name = "dir", description = "文件目录")
                        @RequestParam(value = "dir", required = false) String dir);
    
    /**
     * 上传文件字节数组
     */
    @PostRequest(order = 7,value = "/upload/byte", summary = "上传文件字节数组", description = "上传文件字节数组")
    R<OssFileDTO> uploadByte(@Parameter(name = "fileName", description = "文件名")
                           @RequestParam(value = "fileName") String fileName,
                           @Parameter(name = "dir", description = "文件目录")
                           @RequestParam(value = "dir", required = false) String dir,
                           @Parameter(name = "fileByte", description = "文件Byte数组")
                           @RequestBody byte[] fileByte);
    
    /**
     * 下载文件
     */
    @GetRequest(order = 8,value = "/download", summary = "下载文件", description = "下载文件")
    void download(@Parameter(name = "fileUrl", description = "文件相对路径")
                  @RequestParam(value = "fileUrl") String fileUrl,
                  @Parameter(name = "fileUrl", description = "文件相对路径")
                  @RequestParam(value = "preview", required = false) Boolean preview,
                  HttpServletResponse response);
    
    /**
     * 下载文件字节数组
     */
    @GetRequest(order = 9,value = "/download/byte", summary = "下载文件字节数组", description = "下载文件字节数组")
    R<byte[]> downloadByte(@Parameter(name = "fileUrl", description = "文件相对路径")
                          @RequestParam("fileUrl") String fileUrl);
}
