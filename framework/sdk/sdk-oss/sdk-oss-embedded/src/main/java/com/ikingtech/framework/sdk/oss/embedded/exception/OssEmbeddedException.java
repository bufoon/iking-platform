package com.ikingtech.framework.sdk.oss.embedded.exception;


import java.io.Serial;

/**
 * @author tie yan
 */
public class OssEmbeddedException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -371197085150069611L;

    public OssEmbeddedException(String message) {
        super(message);
    }
}
