package com.ikingtech.framework.sdk.oss.embedded.core;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@AllArgsConstructor
public class OssInitMultiUploadResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = -8458173671067150329L;

    private String uploadId;

    private String objectName;
}
