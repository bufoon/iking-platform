package com.iking.framework.sdk.authorization.api;

import com.iking.framework.sdk.authorization.model.AuthorizationRoleMenu;

import java.util.List;

/**
 * @author tie yan
 */
public interface AuthorizationRoleApi {

    List<AuthorizationRoleMenu> loadMenu(List<String> roleIds);
}
