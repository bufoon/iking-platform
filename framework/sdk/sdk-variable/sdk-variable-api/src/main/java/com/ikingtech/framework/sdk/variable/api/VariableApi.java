package com.ikingtech.framework.sdk.variable.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.variable.model.VariableDTO;
import com.ikingtech.framework.sdk.variable.model.VariableQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface VariableApi {

    /**
     * 添加系统参数
     *
     * @param variable 参数信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加系统参数", description = "添加系统参数")
    R<String> add(@Parameter(name = "variable", description = "参数信息")
                  @RequestBody VariableDTO variable);

    /**
     * 删除系统参数
     *
     * @param id 编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除系统参数", description = "删除系统参数")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新系统参数
     *
     * @param variable 参数信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新系统参数", description = "更新系统参数")
    R<Object> update(@Parameter(name = "variable", description = "参数信息")
                     @RequestBody VariableDTO variable);

    /**
     * 分页查询系统参数
     *
     * @param queryParam 查询条件
     * @return 返回参数列表
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询系统参数", description = "分页查询系统参数")
    R<List<VariableDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                              @RequestBody VariableQueryParamDTO queryParam);

    /**
     * 查询所有系统参数
     *
     * @return 返回参数列表
     */
    @PostRequest(order = 5, value = "/list/all", summary = "查询所有系统参数", description = "查询所有系统参数")
    R<List<VariableDTO>> all();

    /**
     * 根据编号查询系统参数详情
     *
     * @param id 编号
     * @return 返回参数详情
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "根据编号查询系统参数详情", description = "根据编号查询系统参数详情")
    R<VariableDTO> detail(@Parameter(name = "id", description = "编号")
                          @RequestBody String id);

    /**
     * 根据系统参数键查询参数值
     *
     * @param key 系统参数键
     * @return 返回参数值
     */
    @PostRequest(order = 7, value = "/value/key", summary = "根据系统参数键查询参数值", description = "根据系统参数键查询参数值")
    R<String> getValueByKey(@Parameter(name = "key", description = "系统参数键")
                            @RequestBody String key);
}
