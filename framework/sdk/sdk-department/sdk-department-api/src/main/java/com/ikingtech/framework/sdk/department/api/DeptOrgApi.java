package com.ikingtech.framework.sdk.department.api;

import com.ikingtech.framework.sdk.department.model.DeptOrgDTO;

import java.util.List;

/**
 * @author tie yan
 */
public interface DeptOrgApi {

    /**
     * 根据多个组织编号查询
     *
     * @param orgIds 组织编号集合
     * @return 执行结果
     */
    List<DeptOrgDTO> loadByIds(List<String> orgIds);

    /**
     * 根据单个组织编号查询
     *
     * @param orgId 组织编号
     * @return 执行结果
     */
    DeptOrgDTO loadById(String orgId);
}
