package com.ikingtech.framework.sdk.department.api;

import java.util.List;

/**
 * 部门操作role_data_scope中数据
 *
 * @author zhangqiang
 */
public interface DeptRoleDataScopeApi {

    /**
     * 根据部门id删除roleDataScope
     *
     * @param deptIds 部门id集合
     */
    void removeRoleDataScopeByDeptIds(List<String> deptIds, String tenantCode);

}
