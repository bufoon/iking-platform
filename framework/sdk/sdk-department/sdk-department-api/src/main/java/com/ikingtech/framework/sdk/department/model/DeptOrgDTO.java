package com.ikingtech.framework.sdk.department.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author yang
 */
@Data
@Schema(name = "DeptOrgDTO", description = "组织内容")
public class DeptOrgDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -24661474815619644L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "name", description = "组织名称")
    private String name;
}
