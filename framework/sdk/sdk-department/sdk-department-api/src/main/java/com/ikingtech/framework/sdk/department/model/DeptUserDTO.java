package com.ikingtech.framework.sdk.department.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author yang
 */
@Data
@Schema(name = "DeptUserDTO", description = "部门用户")
public class DeptUserDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 52198651685421654L;

    @Schema(name = "userId", description = "用户id")
    private String userId;

    @Schema(name = "name", description = "姓名")
    private String name;

    @Schema(name = "phone", description = "手机号")
    private String phone;

    @Schema(name = "deptId", description = "部门Id")
    private String deptId;

    @Schema(name = "email", description = "邮箱")
    private String email;
}
