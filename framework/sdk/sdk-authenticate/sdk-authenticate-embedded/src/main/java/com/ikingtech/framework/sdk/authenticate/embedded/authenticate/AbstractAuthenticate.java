package com.ikingtech.framework.sdk.authenticate.embedded.authenticate;

import com.ikingtech.framework.sdk.authenticate.embedded.core.Credential;
import com.ikingtech.framework.sdk.authenticate.extension.IdentityExtensionLoader;
import com.ikingtech.framework.sdk.cache.constants.CacheConstants;
import com.ikingtech.framework.sdk.context.security.Identity;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.enums.system.variable.AllowMultiSignInEnum;
import com.ikingtech.framework.sdk.enums.system.variable.VariableEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.TOKEN_PREFIX;

/**
 * 身份认证基类
 *
 * @author tie yan
 */
@RequiredArgsConstructor
public abstract class AbstractAuthenticate implements Authenticate {

    protected final StringRedisTemplate redisTemplate;

    private final List<IdentityExtensionLoader> loaders;

    public void resolveMultiSign(final String userId, final String tenantCode) {
        String multiSignStrategy = VariableEnum.resolveAllowMultiSignIn(this.redisTemplate.opsForHash()
                .get(CacheConstants.systemVariableFormat(tenantCode), VariableEnum.ALLOW_MULTI_SIGN_IN.name()));
        if (Tools.Str.isBlank(multiSignStrategy) || AllowMultiSignInEnum.ALLOW.name().equals(multiSignStrategy)) {
            return;
        }
        Set<String> loginUserKeys = this.redisTemplate.keys(CacheConstants.loginUserFormat(userId, "*"));
        if (null == loginUserKeys || loginUserKeys.isEmpty()) {
            return;
        }
        this.redisTemplate.delete(Tools.Coll.convertList(Tools.Coll.flatMap(new ArrayList<>(loginUserKeys), loginUserKey -> this.redisTemplate.opsForList().range(loginUserKey, 0, -1), Collection::stream),
                CacheConstants::accessTokenKeyFormat));
        this.redisTemplate.delete(loginUserKeys);
    }

    @Override
    public Identity verify(Credential credential) {
        Identity identity = this.doVerify(credential, Tools.Id.uuid());
        // 根据注册中心的服务名称,补充各业务需要在首次登录时写入的数据,主要用于各业务方
        identity.setExtensions(this.loadIdentityExtension(identity));
        identity.setPassword(Tools.Str.EMPTY);
        return this.verifyPass(identity);
    }

    abstract Identity doVerify(Credential credential, String token);

    protected Identity verifyPass(Identity identity) {
        identity.setAuthenticated(true);
        String systemVariableCacheKey = CacheConstants.systemVariableFormat(Me.tenantCode());
        long passwordExpire = VariableEnum.resolvePasswordExpire(
                this.redisTemplate.opsForHash().get(systemVariableCacheKey, VariableEnum.PASSWORD_EXPIRE.name()));
        this.redisTemplate.opsForHash().get(CacheConstants.systemVariableFormat(Me.tenantCode()), VariableEnum.PASSWORD_EXPIRE.name());
        //设置密码是否过期
        identity.setPasswordExpired(null != identity.getPasswordModifyTime() && LocalDateTime.now().minusDays(passwordExpire).isAfter(identity.getPasswordModifyTime()));

        long tokenExpire = VariableEnum.resolveTokenExpire(
                this.redisTemplate.opsForHash().get(systemVariableCacheKey, VariableEnum.TOKEN_EXPIRE.name()));
        this.redisTemplate.opsForHash().get(CacheConstants.systemVariableFormat(Me.tenantCode()), VariableEnum.TOKEN_EXPIRE.name());

        // 根据不同的endpoint分别存储token信息
        this.redisTemplate.opsForList().rightPush(CacheConstants.loginUserFormat(identity.getId(), identity.getEndpoint()), identity.getToken());
        // 设置过期时间
        this.redisTemplate.expire(CacheConstants.loginUserFormat(identity.getId(), identity.getEndpoint()), tokenExpire, TimeUnit.MINUTES);
        // 根据token缓存用户信息
        this.redisTemplate.opsForValue().set(CacheConstants.accessTokenKeyFormat(identity.getToken()), Tools.Json.toJsonStr(identity),
                tokenExpire, TimeUnit.MINUTES);
        // 格式化token
        identity.setToken(TOKEN_PREFIX + identity.getToken());
        return identity;
    }

    private Map<String, Object> loadIdentityExtension(Identity identity) {
        Map<String, Object> result = new HashMap<>();
        if (Tools.Coll.isBlank(this.loaders)) {
            return new HashMap<>();
        }
        this.loaders.forEach(loader -> result.putAll(loader.load(identity)));
        return result;
    }
}
