package com.ikingtech.framework.sdk.authenticate.extension;

import com.ikingtech.framework.sdk.context.security.Identity;

import java.util.Map;

/**
 * @author tie yan
 */
public interface IdentityExtensionLoader {

    Map<String, Object> load(Identity identity);
}
