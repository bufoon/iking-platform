package com.ikingtech.framework.sdk.tenant.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.tenant.model.TenantDTO;
import com.ikingtech.framework.sdk.tenant.model.TenantQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface TenantApi {

    /**
     * 添加租户信息
     *
     * @param tenant 租户信息
     * @return 返回租户编号
     */
    @PostRequest(order = 1, value = "/add", summary = "添加租户信息", description = "添加租户信息")
    R<String> add(@Parameter(name = "tenant", description = "租户信息")
                  @RequestBody TenantDTO tenant);

    /**
     * 删除租户
     *
     * @param tenantCode 租户标识
     * @return 返回结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除租户", description = "删除租户")
    R<Object> delete(@Parameter(name = "tenantCode", description = "租户标识")
                     @RequestBody String tenantCode);

    /**
     * 更新租户信息
     *
     * @param tenant 租户信息
     * @return 返回结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新租户信息", description = "更新租户信息")
    R<Object> update(@Parameter(name = "tenant", description = "租户信息")
                     @RequestBody TenantDTO tenant);

    /**
     * 分页查询租户信息
     *
     * @param queryParam 查询条件
     * @return 返回租户列表
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询租户信息", description = "分页查询租户信息")
    R<List<TenantDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                            @RequestBody TenantQueryParamDTO queryParam);

    /**
     * 根据编号查询租户信息
     *
     * @param id 编号
     * @return 返回租户列表
     */
    @PostRequest(order = 5, value = "/detail/id", summary = "根据编号查询租户信息", description = "根据编号查询租户信息")
    R<TenantDTO> detail(@Parameter(name = "id", description = "编号")
                        @RequestBody String id);

    /**
     * 根据租户标识查询租户信息
     *
     * @param code 租户标识
     * @return 返回租户信息
     */
    @PostRequest(order = 6, value = "/detail/code", summary = "根据租户标识查询租户信息", description = "根据租户标识查询租户信息")
    R<TenantDTO> getByCode(@Parameter(name = "code", description = "租户标识")
                           @RequestBody String code);

    /**
     * 根据登录用户查询租户信息
     *
     * @return 返回租户信息
     */
    @PostRequest(order = 7, value = "/detail/login-user", summary = "根据登录用户查询租户信息", description = "根据登录用户查询租户信息")
    R<TenantDTO> getByLoginUser();

    /**
     * 根据登录用户查询租户列表
     *
     * @return 返回租户列表
     */
    @PostRequest(order = 8, value = "/list/login-user", summary = "根据登录用户查询租户列表", description = "根据登录用户查询租户列表")
    R<List<TenantDTO>> listByLoginUser();

    /**
     * 重试操作
     *
     * @param tenantId 租户id
     * @return 返回结果
     */
    @PostRequest(order = 9, value = "/retry", summary = "重试操作", description = "重试操作")
    R<Object> retry(@Parameter(name = "tenantId", description = "租户id")
                    @RequestBody String tenantId);
}
