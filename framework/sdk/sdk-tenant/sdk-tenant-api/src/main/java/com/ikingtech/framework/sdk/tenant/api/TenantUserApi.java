package com.ikingtech.framework.sdk.tenant.api;

import java.util.List;

/**
 * @author tie yan
 */
public interface TenantUserApi {

    List<String> loadCodeByUserId(String userId);
}
