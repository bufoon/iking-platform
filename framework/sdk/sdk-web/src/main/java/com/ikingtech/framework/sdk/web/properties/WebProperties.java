package com.ikingtech.framework.sdk.web.properties;

import com.ikingtech.framework.sdk.enums.common.FrameworkArchitectureTypeEnum;
import com.ikingtech.framework.sdk.enums.common.FrameworkServerModeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".web")
public class WebProperties implements Serializable {

    @Serial
    private static final long serialVersionUID = 2693966506724313868L;

    private FrameworkArchitectureTypeEnum arch;

    private FrameworkServerModeEnum mode;

    private String version;

    private List<String> xssIgnore;
}
