package com.ikingtech.framework.sdk.web.support.agent;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class LocalFrameworkAgent implements FrameworkAgent {

    private final List<FrameworkAgentRunner> runners;

    private Map<FrameworkAgentTypeEnum, FrameworkAgentRunner> runnerMap;

    @Override
    public R<Object> execute(FrameworkAgentTypeEnum agentType, Object data) {
        if (!this.runnerMap.containsKey(agentType)) {
            return R.failed("agentRunnerNotFound");
        }
        return runnerMap.get(agentType).run(data);
    }

    public void init() {
        this.runnerMap = Tools.Coll.convertMap(this.runners, FrameworkAgentRunner::type);
        FrameworkAgentProxy.accept(this);
    }
}
