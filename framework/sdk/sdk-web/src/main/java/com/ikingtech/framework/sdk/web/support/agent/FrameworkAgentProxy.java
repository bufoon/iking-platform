package com.ikingtech.framework.sdk.web.support.agent;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class FrameworkAgentProxy implements Serializable {

    @Serial
    private static final long serialVersionUID = -673507442496947217L;

    private static FrameworkAgent agent;

    public static void accept(FrameworkAgent agent) {
        FrameworkAgentProxy.agent = agent;
    }

    public static FrameworkAgent agent() {
        return agent;
    }
}
