package com.ikingtech.framework.sdk.pay.embedded.supplier;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class PayOrder implements Serializable {

    @Serial
    private static final long serialVersionUID = 5070601065820706085L;

    /**
     * 支付平台交易编号
     */
    private String supplierTradeId;

    /**
     * 请求编号
     */
    private String requestId;

    /**
     * 支付平台渠道交易编号
     */
    private String channelTradeId;

    /**
     * 交易详情
     */
    private String payInfo;
}
