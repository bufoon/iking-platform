package com.ikingtech.framework.sdk.pay.embedded.supplier;

import com.ikingtech.framework.sdk.enums.pay.PayTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class CancelArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = 8848761207960411671L;

    public CancelArgs(Builder builder) {
        this.requestId = builder.requestId;
        this.amount = builder.amount;
        this.payType = builder.payType;
    }

    private String requestId;

    private Double amount;

    private PayTypeEnum payType;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String requestId;

        private Double amount;

        private PayTypeEnum payType;

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder amount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Builder payType(PayTypeEnum payType) {
            this.payType = payType;
            return this;
        }

        public CancelArgs build() {
            return new CancelArgs(this);
        }
    }
}
