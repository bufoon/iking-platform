package com.ikingtech.framework.sdk.pay.embedded.supplier;

import com.ikingtech.framework.sdk.pay.embedded.Cashier;

/**
 * @author tie yan
 */
public abstract class AbstractCashier implements Cashier {

    protected CashierSupplierConfig config;

    @Override
    public Cashier config(CashierSupplierConfig config) {
        this.config = config;
        return this;
    }
}
