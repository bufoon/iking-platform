package com.ikingtech.framework.sdk.pay.embedded.configuration;

import com.ikingtech.framework.sdk.pay.embedded.Cashier;
import com.ikingtech.framework.sdk.pay.embedded.CashierBuilder;
import com.ikingtech.framework.sdk.pay.embedded.properties.PayProperties;
import com.ikingtech.framework.sdk.pay.embedded.supplier.ali.AliCashier;
import com.ikingtech.framework.sdk.pay.embedded.supplier.allinpay.AllInPayCashier;
import com.ikingtech.framework.sdk.pay.embedded.supplier.wechat.WechatCashier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author tie yan
 */
@Configuration
@EnableConfigurationProperties({PayProperties.class})
public class PayEmbeddedConfiguration {

    @Bean
    public Cashier allInPayCashier() {
        return new AllInPayCashier();
    }

    @Bean
    public Cashier wechatPayCashier() {
        return new WechatCashier();
    }

    @Bean
    public Cashier aliCashier() {
        return new AliCashier();
    }

    @Bean
    public CashierBuilder cashierBuilder(List<Cashier> cashiers) {
        return new CashierBuilder(cashiers);
    }
}
