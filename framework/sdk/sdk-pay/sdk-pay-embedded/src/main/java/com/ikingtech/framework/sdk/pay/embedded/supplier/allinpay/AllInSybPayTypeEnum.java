package com.ikingtech.framework.sdk.pay.embedded.supplier.allinpay;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor

public enum AllInSybPayTypeEnum {

    WECHAT_SCAN("W01", "微信扫码支付"),

    WECHAT_JS("W02", "微信JS支付"),

    WECHAT_MINI("W06", "微信小程序支付"),

    ALI_PAY_SCAN("A01", "支付宝扫码支付"),

    ALI_PAY_JS("A02", "支付宝JS支付"),

    ALI_PAY_APP("A03", "支付宝APP支付"),

    QQ_SCAN("Q01", "手机QQ扫码支付"),

    QQ_JS("Q02", "手机QQ JS支付"),

    UNION_SCAN("U01", "银联扫码支付(CSB)"),

    UNION_JS("U02", "银联JS支付"),

    DIGITAL_CURRENCY("S03", "数字货币H5支付");

    public final String type;

    public final String description;
}
