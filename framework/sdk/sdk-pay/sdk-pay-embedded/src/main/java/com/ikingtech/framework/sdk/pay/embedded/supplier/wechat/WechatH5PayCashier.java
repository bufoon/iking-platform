package com.ikingtech.framework.sdk.pay.embedded.supplier.wechat;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.enums.pay.CashierSupplierEnum;
import com.ikingtech.framework.sdk.pay.embedded.supplier.CashierSupplierConfig;
import com.ikingtech.framework.sdk.pay.embedded.supplier.PayArgs;
import com.ikingtech.framework.sdk.pay.embedded.supplier.PayOrder;
import com.ikingtech.framework.sdk.utils.Tools;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.service.payments.h5.H5Service;
import com.wechat.pay.java.service.payments.h5.model.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.servlet.http.HttpServletRequest;

/**
 * @author tie yan
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WechatH5PayCashier {

    public static PayOrder prepay(PayArgs payArgs, CashierSupplierConfig config, RSAAutoCertificateConfig rsaConfig) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            throw new FrameworkException("微信H5支付失败[http request attributes is null]");
        }
        PrepayRequest request = new PrepayRequest();
        Amount amount = new Amount();
        amount.setTotal((int) (payArgs.getAmount() * 100));
        request.setAmount(amount);
        request.setMchid(config.getCustomId());
        request.setAppid(config.getAppId());
        request.setDescription(payArgs.getSubject());
        request.setNotifyUrl(config.getNotifyUrl() + "/" + CashierSupplierEnum.WECHAT_PAY.name() + "/" + payArgs.getRequestId());
        request.setOutTradeNo(payArgs.getRequestId());
        SceneInfo sceneInfo = new SceneInfo();
        HttpServletRequest httpRequest = requestAttributes.getRequest();
        sceneInfo.setPayerClientIp(Tools.Network.ip(httpRequest));
        H5Info h5Info = new H5Info();
        String userAgent = httpRequest.getHeader("User-Agent");
        if (userAgent.contains("iPhone")) {
            h5Info.setType("iOS");
        } else if (userAgent.contains("Android")) {
            h5Info.setType("Android");
        } else {
            h5Info.setType("Wap");
        }
        request.setSceneInfo(sceneInfo);
        H5Service payService = new H5Service.Builder().config(rsaConfig).build();
        PrepayResponse prepayRes = payService.prepay(request);
        PayOrder payOrder = new PayOrder();
        payOrder.setRequestId(payArgs.getRequestId());
        payOrder.setPayInfo(prepayRes.getH5Url());
        return payOrder;
    }

    public static void closeOrder(String requestId, CashierSupplierConfig config, RSAAutoCertificateConfig rsaConfig) {
        CloseOrderRequest request = new CloseOrderRequest();
        request.setMchid(config.getCustomId());
        request.setOutTradeNo(requestId);
        H5Service payService = new H5Service.Builder().config(rsaConfig).build();
        payService.closeOrder(request);
    }
}
