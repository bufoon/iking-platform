package com.ikingtech.framework.sdk.pay.embedded.supplier;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class RedPackArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = -8825510035412380674L;

    public RedPackArgs(Builder builder) {
        this.requestId = builder.requestId;
        this.amount = builder.amount;
        this.receiverCount = builder.receiverCount;
        this.wishingWords = builder.wishingWords;
        this.title = builder.title;
        this.remark = builder.remark;
        this.channelUserId = builder.channelUserId;
    }

    private String requestId;

    private Double amount;

    private Integer receiverCount;

    private String wishingWords;

    private String title;

    private String remark;

    private String channelUserId;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String requestId;

        private Double amount;

        private Integer receiverCount;

        private String wishingWords;

        private String title;

        private String remark;

        private String channelUserId;

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder amount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Builder receiverCount(Integer receiverCount) {
            this.receiverCount = receiverCount;
            return this;
        }

        public Builder wishingWords(String wishingWords) {
            this.wishingWords = wishingWords;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder remark(String remark) {
            this.remark = remark;
            return this;
        }

        public Builder channelUserId(String channelUserId) {
            this.channelUserId = channelUserId;
            return this;
        }

        public RedPackArgs build() {
            return new RedPackArgs(this);
        }
    }
}
