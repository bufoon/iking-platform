package com.ikingtech.framework.sdk.pay.embedded.supplier;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
public class PayResult implements Serializable {

    @Serial
    private static final long serialVersionUID = 5070601065820706085L;

    private String supplierTradeId;

    private String requestId;

    private String channelTradeId;

    private LocalDateTime tradeTime;

    private Boolean paying;

    private Boolean success;

    private String cause;

    private String supplierResponse;
}
