package com.ikingtech.framework.sdk.pay.model;

import com.ikingtech.framework.sdk.enums.pay.CashierSupplierEnum;
import com.ikingtech.framework.sdk.enums.pay.PayStatusEnum;
import com.ikingtech.framework.sdk.enums.pay.PayTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PayRecordDTO", description = "交易记录信息")
public class PayRecordDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1742050424194694376L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "requestId", description = "请求编号")
    private String requestId;

    @Schema(name = "amount", description = "交易金额")
    private Double amount;

    @Schema(name = "supplier_id", description = "支付平台编号")
    private String supplierId;

    @Schema(name = "supplier", description = "支付平台类型")
    private CashierSupplierEnum supplierType;

    @Schema(name = "supplierTypeName", description = "支付平台类型名称")
    private String supplierTypeName;

    @Schema(name = "payType", description = "支付类型")
    private PayTypeEnum payType;

    @Schema(name = "payTypeName", description = "支付类型名称")
    private String payTypeName;

    @Schema(name = "supplierTradeId", description = "支付平台交易编号")
    private String supplierTradeId;

    @Schema(name = "channelTradeId", description = "支付平台渠道交易编号")
    private String channelTradeId;

    @Schema(name = "tradeTime", description = "交易时间")
    private LocalDateTime tradeTime;

    @Schema(name = "cause", description = "失败原因")
    private String cause;

    @Schema(name = "status", description = "交易状态")
    private PayStatusEnum status;

    @Schema(name = "statusName", description = "交易状态名称")
    private String statusName;

    private LocalDateTime createTime;

    private String createBy;

    private String createName;

    private LocalDateTime updateTime;

    private String updateBy;

    private String updateName;
}
