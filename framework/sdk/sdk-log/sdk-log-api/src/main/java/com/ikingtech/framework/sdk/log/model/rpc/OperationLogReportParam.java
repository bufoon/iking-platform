package com.ikingtech.framework.sdk.log.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志信息
 *
 * @author wangbo
 */
@Data
public class OperationLogReportParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -9175130347752782987L;

    private String domainCode;

    private String tenantCode;

    private String appCode;

    /**
     * 业务模块
     */
    private String module;

    /**
     * 操作内容
     */
    private String operation;

    /**
     * 方法名称
     */
    private String method;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 请求方式
     */
    private String requestUrl;

    /**
     * 请求参数
     */
    private String requestParam;

    /**
     * 响应体
     */
    private String responseBody;

    /**
     * 操作用户名id
     */
    private String operateUserId;

    /**
     * 操作用户名
     */
    private String operateUsername;

    /**
     * 客户端IP地址
     */
    private String ip;

    /**
     * 客户端IP归属地
     */
    private String location;

    /**
     * 是否执行成功
     */
    private Boolean success;

    /**
     * 返回消息
     */
    private String message;

    /**
     * 操作时间
     */
    private LocalDateTime operationTime;
}
