package com.ikingtech.framework.sdk.log.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
public class SystemLogReportParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 1356731577566688159L;

    private String domainCode;

    private String tenantCode;

    private String appCode;

    /**
     * 链路编号
     */
    private String traceId;

    /**
     * 包名
     */
    private String packageName;

    /**
     * 方法
     */
    private String method;

    /**
     * 日志内容
     */
    private String message;

    /**
     * 异常堆栈
     */
    private String exceptionStack;

    /**
     * 执行时间
     */
    private LocalDateTime executeTime;
}
