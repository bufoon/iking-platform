package com.ikingtech.framework.sdk.log.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.log.model.*;
import com.ikingtech.framework.sdk.log.model.rpc.AuthLogReportParam;
import com.ikingtech.framework.sdk.log.model.rpc.OperationLogReportParam;
import com.ikingtech.framework.sdk.log.model.rpc.SystemLogBatchReportParam;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface LogApi {

    /**
     * 报告登录日志
     *
     * @param authLog 登录日志信息
     * @return 返回结果
     */
    @PostRequest(order = 1, value = "/auth/report", summary = "报告登录日志", description = "报告登录日志")
    R<Object> reportAuthLog(@Parameter(name = "queryParam", description = "日志信息")
                            @RequestBody AuthLogReportParam authLog);

    /**
     * 删除登录日志
     *
     * @param authLogId 日志编号
     * @return 返回结果
     */
    @PostRequest(order = 2, value = "/auth/delete", summary = "删除登录日志", description = "删除登录日志")
    R<Object> deleteAuthLog(@Parameter(name = "authLogId", description = "日志编号")
                            @RequestBody String authLogId);

    /**
     * 分页查询登录日志
     *
     * @param queryParam 登录日志信息查询参数，包含分页参数、排序参数、查询条件
     * @return 返回登录日志列表
     */
    @PostRequest(order = 3, value = "/auth/list/page", summary = "分页查询登录日志", description = "分页查询登录日志")
    R<List<AuthLogDTO>> authLogPage(@Parameter(name = "queryParam", description = "登录日志信息查询参数，包含分页参数、排序参数、查询条件")
                                    @RequestBody AuthLogQueryParamDTO queryParam);

    /**
     * 根据条件查询登录日志
     *
     * @param queryParam 登录日志信息查询参数，包含分页参数、排序参数、查询条件
     * @return 返回登录日志列表
     */
    @PostRequest(order = 4, value = "/auth/list/conditions", summary = "根据条件查询登录日志", description = "根据条件查询登录日志")
    R<List<AuthLogDTO>> listAuthLogByConditions(@Parameter(name = "queryParam", description = "登录日志信息查询参数，包含分页参数、排序参数、查询条件")
                                                @RequestBody AuthLogQueryParamDTO queryParam);

    /**
     * 报告操作日志
     *
     * @param operationLog 操作日志信息
     * @return 返回结果
     */
    @PostRequest(order = 5, value = "/operation/report", summary = "报告操作日志", description = "报告操作日志")
    R<Object> reportOperationLog(@Parameter(name = "queryParam", description = "日志信息")
                                 @RequestBody OperationLogReportParam operationLog);

    /**
     * 删除操作日志
     *
     * @param operationLogId 日志编号
     * @return 返回结果
     */
    @PostRequest(order = 6, value = "/operation/delete", summary = "删除操作日志", description = "删除操作日志")
    R<Object> deleteOperationLog(@Parameter(name = "operationLogId", description = "日志编号")
                                 @RequestBody String operationLogId);

    /**
     * 分页查询操作日志
     *
     * @param queryParam 操作日志信息查询参数，包含分页参数、排序参数、查询条件
     * @return 返回操作日志列表
     */
    @PostRequest(order = 7, value = "/operation/list/page", summary = "分页查询操作日志", description = "分页查询操作日志")
    R<List<OperationLogDTO>> operationLogPage(@Parameter(name = "queryParam", description = "操作日志信息查询参数，包含分页参数、排序参数、查询条件")
                                              @RequestBody OperationLogQueryParamDTO queryParam);

    /**
     * 根据条件查询操作日志
     *
     * @param queryParam 操作日志信息查询参数，包含分页参数、排序参数、查询条件
     * @return 返回操作日志列表
     */
    @PostRequest(order = 8, value = "/operation/list/conditions", summary = "根据条件查询操作日志", description = "根据条件查询操作日志")
    R<List<OperationLogDTO>> listOperationLogByConditions(@Parameter(name = "queryParam", description = "操作日志信息查询参数，包含分页参数、排序参数、查询条件")
                                                          @RequestBody OperationLogQueryParamDTO queryParam);

    /**
     * 获取操作日志详情
     *
     * @param operationLogId 操作日志编号
     * @return 返回操作日志详情
     */
    @PostRequest(order = 9, value = "/operation/detail/id", summary = "获取操作日志详情", description = "获取操作日志详情")
    R<OperationLogDTO> operationLogDetail(@Parameter(name = "operationLogId", description = "操作日志编号")
                                          @RequestBody String operationLogId);

    /**
     * 报告系统日志
     *
     * @param systemLogs 系统日志信息
     * @return 返回结果
     */
    @PostRequest(order = 10, value = "/system/report", summary = "报告系统日志", description = "报告系统日志")
    R<Object> reportSystemLog(@Parameter(name = "queryParam", description = "日志信息")
                              @RequestBody SystemLogBatchReportParam systemLogs);

    /**
     * 删除系统日志
     *
     * @param systemLogId 日志编号
     * @return 返回结果
     */
    @PostRequest(order = 11, value = "/system/delete", summary = "删除系统日志", description = "删除系统日志")
    R<Object> deleteSystemLog(@Parameter(name = "systemLogId", description = "日志编号")
                              @RequestBody String systemLogId);

    /**
     * 分页查询系统日志
     *
     * @param queryParam 系统日志信息查询参数，包含分页参数、排序参数、查询条件
     * @return 返回系统日志列表
     */
    @PostRequest(order = 12, value = "/system/list/page", summary = "分页查询系统日志", description = "分页查询系统日志")
    R<List<SystemLogDTO>> systemLogPage(@Parameter(name = "queryParam", description = "系统日志信息查询参数，包含分页参数、排序参数、查询条件")
                                        @RequestBody SystemLogQueryParamDTO queryParam);

    /**
     * 获取系统日志详情
     *
     * @param systemLogId 系统日志编号
     * @return 返回系统日志详情
     */
    @PostRequest(order = 13, value = "/system/detail/id", summary = "获取系统日志详情", description = "获取系统日志详情")
    R<SystemLogDTO> systemLogDetail(@Parameter(name = "systemLogId", description = "系统日志编号")
                                    @RequestBody String systemLogId);
}
