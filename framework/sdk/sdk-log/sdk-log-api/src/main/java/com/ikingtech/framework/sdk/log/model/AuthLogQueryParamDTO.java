package com.ikingtech.framework.sdk.log.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 登录日志查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "AuthLogQueryParamDTO", description = "登录日志查询参数")
public class AuthLogQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 1068741107174560567L;

    @Schema(name = "userId", description = "用户id")
    private String userId;

    @Schema(name = "username", description = "用户账号")
    private String username;

    @Schema(name = "type", description = "登录类型")
    private String type;

    @Schema(name = "ip", description = "登录IP地址")
    private String ip;

    @Schema(name = "location", description = "登录IP归属地")
    private String location;

    @Schema(name = "browser", description = "浏览器")
    private String browser;

    @Schema(name = "os", description = "操作系统")
    private String os;

    @Schema(name = "success", description = "是否登录/登出成功")
    private Boolean success;

    @Schema(name = "message", description = "返回信息")
    private String message;

    @Schema(name = "signStartTime", description = "开始时间 yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime signStartTime;

    @Schema(name = "signEndTime", description = "结束时间 yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime signEndTime;
}
