package com.ikingtech.framework.sdk.log.model.rpc;

import com.ikingtech.framework.sdk.enums.log.SignTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 登录日志信息
 *
 * @author zhangqiang
 */
@Data
public class AuthLogReportParam implements Serializable {
    @Serial
    private static final long serialVersionUID = 4042947818764431489L;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户账号
     */
    private String username;
    /**
     * 登录类型
     */
    private SignTypeEnum type;
    /**
     * 登录类型名称
     */
    private String typeName;
    /**
     * 登录IP地址
     */
    private String ip;
    /**
     * 登录IP归属地
     */
    private String location;
    /**
     * 浏览器
     */
    private String browser;
    /**
     * 操作系统
     */
    private String os;
    /**
     * 是否执行成功
     */
    private Boolean success;
    /**
     * 返回信息
     */
    private String message;
    /**
     * 登录/登出时间
     */
    private LocalDateTime signTime;
    /**
     * 创建人编号
     */
    private String createBy;
    /**
     * 创建人姓名
     */
    private String createName;
    /**
     * 更新人编号
     */
    private String updateBy;
    /**
     * 更新人姓名
     */
    private String updateName;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
