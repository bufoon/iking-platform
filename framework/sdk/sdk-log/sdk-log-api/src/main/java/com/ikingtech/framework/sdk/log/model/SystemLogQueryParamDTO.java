package com.ikingtech.framework.sdk.log.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "SystemLogQueryParamDTO", description = "系统日志查询参数")
public class SystemLogQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -6547062484109627663L;

    @Schema(name = "method", description = "方法名称")
    private String method;

    @Schema(name = "traceId", description = "追踪编号")
    private String traceId;

    @Schema(name = "packageName", description = "包名")
    private String packageName;

    @Schema(name = "executeStartTime", description = "开始时间（yyyy-MM-dd HH:mm:ss）")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime executeStartTime;

    @Schema(name = "executeEndTime", description = "结束时间（yyyy-MM-dd HH:mm:ss）")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime executeEndTime;
}
