package com.ikingtech.framework.sdk.sms.embedded.configuration;

import com.ikingtech.framework.sdk.sms.embedded.SmsRequest;
import com.ikingtech.framework.sdk.sms.embedded.SmsRequestBuilder;
import com.ikingtech.framework.sdk.sms.embedded.core.sanyihengxin.SanYiHengXinSmsRequest;
import com.ikingtech.framework.sdk.sms.embedded.core.tencent.TencentSmsRequest;
import com.ikingtech.framework.sdk.sms.embedded.properties.SmsProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * @author tie yan
 */
@EnableConfigurationProperties({SmsProperties.class})
public class SmsEmbeddedConfiguration {

    @Bean
    public SmsRequestBuilder smsRequestBuilder(SmsProperties properties, List<SmsRequest> requests) {
        return new SmsRequestBuilder(properties, requests);
    }

    @Bean
    public SmsRequest sanYiHengXinSmsRequest() {
        return new SanYiHengXinSmsRequest();
    }

    @Bean
    public SmsRequest tencentSmsRequest () {
        return new TencentSmsRequest();
    }
}
