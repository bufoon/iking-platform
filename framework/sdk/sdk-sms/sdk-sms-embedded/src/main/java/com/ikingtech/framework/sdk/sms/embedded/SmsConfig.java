package com.ikingtech.framework.sdk.sms.embedded;

import com.ikingtech.framework.sdk.enums.sms.SmsPlatformTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class SmsConfig implements Serializable {

    @Serial
    private static final long serialVersionUID = 4929895083239804981L;

    private SmsPlatformTypeEnum type;

    private String appId;

    private String appKey;

    private String appSecret;

    private String extendCode;

    private String endpoint;

    private Boolean supportTemplate;
}
