package com.ikingtech.framework.sdk.sms.embedded;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.sms.embedded.properties.SmsProperties;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class SmsRequestBuilder {

    private final SmsProperties properties;

    private final List<SmsRequest> requests;

    public SmsRequest build(String smsId) {
        for (SmsProperties.Platform platform : this.properties.getPlatform()) {
            if (Tools.Str.equals(smsId, platform.getId())) {
                return this.build(Tools.Bean.copy(platform, SmsConfig.class));
            }
        }
        throw new FrameworkException("不支持的短信渠道");
    }

    public SmsRequest build(SmsConfig smsConfig) {
        for (SmsRequest request : this.requests) {
            if (request.platform().equals(smsConfig.getType())) {
                return request.config(smsConfig);
            }
        }
        throw new FrameworkException("不支持的短信渠道");
    }
}
