package com.ikingtech.framework.sdk.sms.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.sms.SmsTemplateStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "SmsTemplateDTO", description = "短信模板信息")
public class SmsTemplateDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -270255548079801980L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "name", description = "消息模板名称")
    private String name;

    @Schema(name = "smsId", description = "消息模板所属短信平台编号")
    private String smsId;

    @Schema(name = "title", description = "模版标题")
    private String title;

    @Schema(name = "content", description = "模版内容")
    private String content;

    @Schema(name = "status", description = "模版状态")
    private SmsTemplateStatusEnum status;

    @Schema(name = "statusName", description = "模版状态名称")
    private String statusName;

    @Schema(name = "params", description = "模版参数集合")
    private List<String> params;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
