package com.ikingtech.framework.sdk.sms.model;

import com.ikingtech.framework.sdk.enums.sms.SmsPlatformTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class SmsDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1652680709246391368L;

    private String id;

    private String code;

    private String name;

    private SmsPlatformTypeEnum type;

    private String typeName;

    private String appId;

    private String appKey;

    private String appSecret;

    private String endpoint;

    private Boolean supportTemplate;
}
