package com.ikingtech.framework.sdk.context.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 下载文件异常
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DownloadException extends RuntimeException{

    private static final long serialVersionUID = -5574221182172068059L;

    /**
     * 异常信息
     */
    private final String message;

    /**
     * 异常的模块名称
     */
    private final String moduleName;

    public DownloadException(FrameworkExceptionInfo frameworkExceptionInfo) {
        this(frameworkExceptionInfo.moduleName(), frameworkExceptionInfo.message());
    }

    public DownloadException(String message) {
        this("unknown", message);
    }

    public DownloadException(String moduleName, String message) {
        super(message);
        this.message = message;
        this.moduleName = moduleName;
    }
}
