package com.ikingtech.framework.sdk.context.security;

import com.ikingtech.framework.sdk.enums.authenticate.SignEndpointTypeEnum;
import com.ikingtech.framework.sdk.enums.domain.DomainEnum;
import com.ikingtech.framework.sdk.enums.system.user.UserCategoryEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.GLOBAL_TOKEN;

/**
 * @author tie yan
 */
@Data
public class Identity implements Serializable {

    @Serial
    private static final long serialVersionUID = -928777645979007550L;

    public static final String ADMIN_USER_ID = "00000000";

    public static final String ADMIN_USER_NAME = "admin";

    public static final String ADMIN_NAME = "系统管理员";

    /**
     * 用户id
     */
    private String id;

    /**
     * 用户账户
     */
    private String username;

    /**
     * 密码（密文）
     */
    private String password;

    /**
     * 密码是否过期
     */
    private Boolean passwordExpired;

    /**
     * 密码修改时间
     */
    private LocalDateTime passwordModifyTime;

    /**
     * 用户昵称
     */
    private String name;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 是否锁定
     */
    private Boolean locked;

    /**
     * 锁类型
     */
    private String lockType;

    /**
     * 是否通过了身份验证
     */
    private Boolean authenticated;

    /**
     * 失败原因
     */
    private String cause;

    /**
     * 数据权限
     */
    private List<String> dataScopeCodes;

    /**
     * token
     */
    private String token;

    /**
     * 语言环境
     */
    private String lang;

    /**
     * 租户code
     */
    private String tenantCode;

    /**
     * 菜单id
     */
    private String menuId;

    /**
     * 客户端类型
     */
    private String endpoint;

    /**
     * 用户扩展信息
     */
    private Map<String, Object> extensions;

    /**
     * 请求的扩展头
     */
    private Map<String, Object> extensionHeaders;

    private String requestId;

    /**
     * 用户类别标识集合
     */
    private List<String> categoryCodes;

    /**
     * 用户登录的域
     */
    private String domainCode;

    /**
     * 当前应用
     */
    private String appCode;

    public static Identity defaultUser() {
        Identity identity = new Identity();
        identity.setId("99999999");
        identity.setUsername("unknown");
        identity.setName("未知用户");
        identity.setLang(Locale.SIMPLIFIED_CHINESE.toLanguageTag());
        return identity;
    }
    public static Identity admin() {
        Identity identity = new Identity();
        identity.setId(ADMIN_USER_ID);
        identity.setUsername(ADMIN_USER_NAME);
        identity.setName(ADMIN_NAME);
        identity.setDataScopeCodes(new ArrayList<>());
        identity.setDomainCode(DomainEnum.PLATFORM.name());
        identity.setCategoryCodes(Tools.Coll.newList(UserCategoryEnum.PLATFORM_ADMINISTRATOR.name()));
        identity.setLang(Locale.SIMPLIFIED_CHINESE.toLanguageTag());
        identity.setToken(GLOBAL_TOKEN);
        identity.setEndpoint(SignEndpointTypeEnum.PC.name());
        return identity;
    }
    public static Identity tenantAdmin(String token) {
        String tenantCode = Tools.Str.split(token, "_").get(1);
        Identity identity = new Identity();
        identity.setId(Tools.Str.join(tenantCode, ADMIN_USER_ID, "_"));
        identity.setUsername(Tools.Str.join(tenantCode, ADMIN_USER_NAME, "_"));
        identity.setName(Tools.Str.join(tenantCode, ADMIN_NAME, "_"));
        identity.setDataScopeCodes(new ArrayList<>());
        identity.setDomainCode(DomainEnum.TENANT.name());
        identity.setCategoryCodes(Tools.Coll.newList(UserCategoryEnum.TENANT_ADMINISTRATOR.name()));
        identity.setLang(Locale.SIMPLIFIED_CHINESE.toLanguageTag());
        identity.setToken(GLOBAL_TOKEN);
        identity.setEndpoint(SignEndpointTypeEnum.PC.name());
        return identity;
    }
}
