package com.ikingtech.framework.sdk.context.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Getter
public class TenantInitEvent extends ApplicationEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = -8201841854355727234L;

    private final String code;

    private final String name;

    public TenantInitEvent(Object source, String code, String name) {
        super(source);
        this.code = code;
        this.name = name;
    }
}
