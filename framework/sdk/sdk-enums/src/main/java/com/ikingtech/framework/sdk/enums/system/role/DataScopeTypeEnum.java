package com.ikingtech.framework.sdk.enums.system.role;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DataScopeTypeEnum {

    /**
     * 本部门数据
     */
    DEPT("本部门数据"),

    /**
     * 本部门及以下数据
     */
    DEPT_WITH_CHILD("本部门及以下数据"),

    /**
     * 指定部门数据
     */
    DEFINE("指定部门数据"),

    /**
     * 全部数据
     */
    ALL("全部数据");

    public final String description;
}
