package com.ikingtech.framework.sdk.enums.system.organization;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum OrganizationStatusEnum {

    ENABLE("已启用"),

    DISABLE("已停用");

    public final String description;
}
