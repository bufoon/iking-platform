package com.ikingtech.framework.sdk.enums.system.menu;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * 菜单类型
 *
 * @author tie yan
 */
@AllArgsConstructor
public enum MenuTypeEnum {

	/**
	 * 菜单
	 */
	MENU("菜单", 1),

	/**
	 * 页面Tab
	 */
	PAGE_TAB("页面Tab", 2),

	/**
	 * 页面按钮
	 */
	PAGE_BUTTON("页面按钮", 3),

	/**
	 * 全局按钮
	 */
	GLOBAL_BUTTON("全局按钮", 4);

	/**
	 * 描述
	 */
	public final String description;

	public final Integer sortOrder;

	public static List<String> sortList() {
		return Arrays.stream(MenuTypeEnum.values()).sorted(Comparator.comparing(type -> type.sortOrder)).map(MenuTypeEnum::name).collect(Collectors.toList());
	}
}
