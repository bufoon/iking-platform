package com.ikingtech.framework.sdk.enums.application;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum EmbeddedApplicationEnum {

    MESSAGE_CENTER("消息中心",
            "自定义消息模板，多渠道消息触达",
            "message",
            "ikflow-duanxin",
            "#1e90ff",
            ApplicationTypeEnum.EMBEDDED,
            "news-center/personal-news"),

    APPROVE_CENTER("审批中心",
            "简单、高效的建立人事/财务等审批流程",
            "approve",
            "ikflow-shenhe",
            "#1e90ff",
            ApplicationTypeEnum.EMBEDDED,
            "iking-flow/approve-list"),

    SCHEDULE("日程管理",
            "会议、任务、行程一表管理",
            "schedule",
            "ikflow-daka",
            "#1e90ff",
            ApplicationTypeEnum.EMBEDDED,
            "/schedule");

    public final String description;

    public final String remark;

    public final String code;

    public final String icon;

    public final String iconBackground;

    public final ApplicationTypeEnum type;

    public final String indexPath;
}
