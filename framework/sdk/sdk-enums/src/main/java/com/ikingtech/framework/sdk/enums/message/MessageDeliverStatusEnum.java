package com.ikingtech.framework.sdk.enums.message;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum MessageDeliverStatusEnum {

    /**
     * 未发送
     */
    NO_DELIVER("未发送"),

    /**
     * 已发送
     */
    DELIVERED("成功"),

    /**
     * 发送失败
     */
    DELIVER_FAILED("发送失败");

    public final String description;
}
