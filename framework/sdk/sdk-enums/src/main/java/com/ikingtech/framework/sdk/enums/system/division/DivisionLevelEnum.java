package com.ikingtech.framework.sdk.enums.system.division;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DivisionLevelEnum {

    /**
     * 省
     */
    PROVINCE("省", 1),

    /**
     * 市
     */
    CITY("市", 2),

    /**
     * 区
     */
    DISTRICT("区", 3),

    /**
     * 镇/街道
     */
    TOWN("镇/街道", 4),

    /**
     * 村
     */
    VILLAGE("村", 5);

    public final String description;

    public final Integer code;

    public static DivisionLevelEnum valueOf(int code) {
        for (DivisionLevelEnum value : DivisionLevelEnum.values()) {
            if (value.code.equals(code)) {
                return value;
            }
        }
        return null;
    }
}
