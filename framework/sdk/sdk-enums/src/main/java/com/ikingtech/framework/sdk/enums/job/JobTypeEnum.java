package com.ikingtech.framework.sdk.enums.job;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum JobTypeEnum {

    /**
     * 仅执行一次
     */
    ONCE("仅执行一次"),

    /**
     * 周期性执行
     */
    REPEATEDLY("周期性执行");

    public final String description;
}
