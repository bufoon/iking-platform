package com.ikingtech.framework.sdk.enums.job;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum JobStatusEnum {

    /**
     * 执行中
     */
    RUNNING("执行中"),

    /**
     * 已结束
     */
    END("已结束");

    public final String description;
}
