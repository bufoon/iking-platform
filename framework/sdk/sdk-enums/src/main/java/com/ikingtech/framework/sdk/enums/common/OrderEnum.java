package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author Administrator
 */

@RequiredArgsConstructor
public enum OrderEnum {

    /**
     * 升序
     */
    ASC("升序"),

    /**
     * 升序
     */
    DESC("降序");

    public final String description;
}
