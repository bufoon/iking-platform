package com.ikingtech.framework.sdk.enums.authorization;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ResourcePermissionEnum {

    /**
     * 写入操作
     */
    WRITE("写入", ResourceTypeEnum.DATA),

    /**
     * 移除操作
     */
    REMOVE("移除", ResourceTypeEnum.DATA),

    /**
     * 更新操作
     */
    UPDATE("更新", ResourceTypeEnum.DATA),

    /**
     * 读取操作
     */
    READ("读取", ResourceTypeEnum.DATA),

    /**
     * 调用操作
     */
    CALL("调用", ResourceTypeEnum.API),

    /**
     * 访问操作
     */
    ACCESS("访问", ResourceTypeEnum.MENU);


    public final String description;

    public final ResourceTypeEnum resourceType;
}
