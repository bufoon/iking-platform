package com.ikingtech.framework.sdk.enums.authorization;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ClientTypeEnum {

    ROLE("角色"),

    THIRD_PARTY("第三方");

    public final String description;
}
