package com.ikingtech.framework.sdk.enums.scffold;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ScaffoldQueryCompareTypeEnum {

    EQ("相等", "eq"),

    LIKE("模糊匹配", "like"),

    IN("在范围内", "in"),

    GT("大于", "gt"),

    GE("大于等于", "ge"),

    LT("小于", "lt"),

    LE("小于等于", "le");

    public final String description;

    public final String expression;
}
