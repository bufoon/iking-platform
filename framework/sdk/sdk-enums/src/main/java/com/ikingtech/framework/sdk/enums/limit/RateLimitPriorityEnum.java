package com.ikingtech.framework.sdk.enums.limit;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author zhangqiang
 */
@Getter
@RequiredArgsConstructor
public enum RateLimitPriorityEnum {

    COVER("覆盖"),

    COMPLEMENT("补充");

    private final String description;
}
