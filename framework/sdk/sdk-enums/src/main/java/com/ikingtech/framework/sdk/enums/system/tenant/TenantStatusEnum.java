package com.ikingtech.framework.sdk.enums.system.tenant;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum TenantStatusEnum {

    /**
     * 正常
     */
    NORMAL("正常"),

    /**
     * 初始化中
     */
    DATA_MIGRATING("初始化中"),

    /**
     * 初始化失败
     */
    DATA_MIGRATE_FAIL("初始化失败"),

    /**
     * 冻结
     */
    FREEZE("冻结");

    public final String description;
}
