package com.ikingtech.framework.sdk.enums.system.menu;

import lombok.AllArgsConstructor;

/**
 * @author tie yan
 */

@AllArgsConstructor
public enum MenuJumpTypeEnum {

    NONE("无"),

    FRONT_ROUTE("前端路由"),

    IFRAME("页面嵌套"),

    EXTERNAL_LINK("外部链接");

    public final String description;
}
