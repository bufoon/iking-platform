package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveFormStatusEnum {

    /**
     * 启用
     */
    ENABLE("启用"),

    /**
     * 停用
     */
    DISABLE("停用");

    public final String description;
}
