package com.ikingtech.framework.sdk.enums.approve;

        import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveExecutorEmptyStrategyEnum {

    /**
     * 自动通过
     */
    AUTO_PASS("自动通过"),

    /**
     * 自动转交管理员
     */
    TRANSMIT_TO_MANAGER("自动转交管理员"),

    /**
     * 指定人员审批
     */
    SPECIFIED_USER("指定人员审批");

    public final String description;
}
