package com.ikingtech.framework.sdk.enums.plugin;

import lombok.RequiredArgsConstructor;

/**
 * @author Administrator
 */

@RequiredArgsConstructor
public enum PluginTypeEnum {

    CRUD("通用业务");

    public final String description;
}
