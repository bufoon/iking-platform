package com.ikingtech.framework.sdk.enums.system.variable;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum VariableTypeEnum {

    /**
     * 系统类
     */
    SYSTEM("系统类"),

    /**
     * 业务类
     */
    BUSINESS("业务类");

    public final String description;
}
