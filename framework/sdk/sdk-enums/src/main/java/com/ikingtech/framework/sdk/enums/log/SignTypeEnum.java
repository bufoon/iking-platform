package com.ikingtech.framework.sdk.enums.log;

import lombok.RequiredArgsConstructor;

/**
 * 登录类型枚举
 *
 * @author zhangqiang
 */
@RequiredArgsConstructor
public enum SignTypeEnum {
    /**
     * 登录
     */
    SIGN_IN("登录"),
    /**
     * 登出
     */
    SIGN_OUT("登出");

    public final String description;
}
