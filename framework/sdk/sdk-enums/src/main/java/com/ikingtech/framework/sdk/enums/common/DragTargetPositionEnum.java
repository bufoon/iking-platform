package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DragTargetPositionEnum {

    /**
     * 上方
     */
    NONE("无"),

    /**
     * 上方
     */
    BEFORE("上方"),

    /**
     * 内部
     */
    INNER("内部"),

    /**
     * 下方
     */
    AFTER("下方");

    public final String description;
}
