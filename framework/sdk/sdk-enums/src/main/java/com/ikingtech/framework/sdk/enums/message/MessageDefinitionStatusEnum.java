package com.ikingtech.framework.sdk.enums.message;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum MessageDefinitionStatusEnum {

    ENABLED("已启用"),

    DISABLED("已停用"),

    NOT_CONFIGURED("未配置");

    public final String description;
}
