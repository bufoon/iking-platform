package com.ikingtech.framework.sdk.enums.scffold;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ScaffoldPagePublishStatusEnum {

    NOT_PUBLISHED("未发布"),

    PUBLISHED("已发布");

    public final String description;
}
