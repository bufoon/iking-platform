package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveFormInstanceCabonCopyReadStatusEnum {

    READ("已读"),

    UNREAD("未读");

    public final String description;
}
