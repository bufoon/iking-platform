package com.ikingtech.framework.sdk.enums.application;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApplicationPageTemplateTypeEnum {

    FORM("表单"),

    PAGE("页面");

    public final String description;
}
