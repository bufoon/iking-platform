package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * 默认表单分组
 *
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveDefaultFormGroupEnum {

    /**
     * 其他
     */
    OTHER("其他"),

    /**
     * 已停用
     */
    DISABLED("已停用");

    public final String description;
}
