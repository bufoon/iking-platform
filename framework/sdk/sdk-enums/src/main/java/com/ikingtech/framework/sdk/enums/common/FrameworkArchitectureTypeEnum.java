package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum FrameworkArchitectureTypeEnum {

    /**
     * 微服务
     */
    MICRO_SERVICE("微服务"),

    /**
     * 单体服务
     */
    SINGLE("单体");

    public final String description;
}
