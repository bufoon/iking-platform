package com.ikingtech.framework.sdk.enums.scffold;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ScaffoldPageTypeEnum {

    GROUP("页面分组"),

    NORMAL("普通页面"),

    APPROVE("流程页面"),

    CUSTOMIZE("自定义页面");

    public final String description;
}
