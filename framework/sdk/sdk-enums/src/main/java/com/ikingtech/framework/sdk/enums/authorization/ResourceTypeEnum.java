package com.ikingtech.framework.sdk.enums.authorization;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ResourceTypeEnum {

    MENU("菜单"),

    DATA("数据"),

    API("接口");

    public final String description;
}
