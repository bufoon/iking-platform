package com.ikingtech.framework.sdk.enums.system.variable;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum TokenDelayTypeEnum {

    /**
     * 自动
     */
    AUTO("自动续期"),

    /**
     * 不续期
     */
    NO_DELAY("不续期");

    public final String description;
}
