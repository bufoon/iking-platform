package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveExecutorTypeEnum {

    /**
     * 全部用户
     */
    ALL("全部用户"),

    /**
     * 用户
     */
    USER("用户"),

    /**
     * 角色
     */
    ROLE("角色"),

    /**
     * 部门
     */
    DEPT("部门"),

    /**
     * 岗位
     */
    POST("岗位");

    public final String description;
}
