package com.ikingtech.framework.sdk.enums.system.organization;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum OrganizationStatusEnums {

    ENABLE("启用"),

    DISABLE("停用");

    public final String description;
}
