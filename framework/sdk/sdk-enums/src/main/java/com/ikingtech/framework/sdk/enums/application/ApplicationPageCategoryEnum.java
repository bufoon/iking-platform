package com.ikingtech.framework.sdk.enums.application;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApplicationPageCategoryEnum {

    MOBILE("移动端"),

    PC("PC端"),

    GENERAL("双端通用");

    public final String description;
}
