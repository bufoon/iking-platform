package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApprovalCategoryEnum {

    /**
     * 指定成员
     */
    SPECIFIED_USER("指定成员"),

    /**
     * 发起人自选
     */
    INITIATOR_SPECIFIED("发起人自选"),

    /**
     * 指定角色
     */
    ROLE("指定角色"),

    /**
     * 发起人部门主管
     */
    INITIATOR_DEPT_MANAGER("发起人部门主管"),

    /**
     * 发起人自己
     */
    INITIATOR_SELF("发起人自己"),

    /**
     * 部门控件关联角色
     */
    DEPT_SELECT_COMPONENT("部门控件关联角色"),

    /**
     * 联系人控件关联成员
     */
    USER_SELECT_COMPONENT("联系人控件关联成员");

    public final String description;
}
