package com.ikingtech.framework.sdk.enums.system.organization;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum OrganizationEquityRatioEnum {

    WHOLLY_OWNED("全资"),

    PARTICIPATION("参股"),

    HOLDING("控股");

    public final String description;
}
