package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveViewPermissionTypeEnum {

    /**
     * 可编辑
     */
    EDITABLE("可编辑"),

    /**
     * 只读
     */
    READ_ONLY("只读"),

    /**
     * 隐藏
     */
    INVISIBLE("隐藏");

    public final String description;
}
