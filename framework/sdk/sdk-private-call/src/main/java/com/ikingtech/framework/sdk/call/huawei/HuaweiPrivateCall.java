package com.ikingtech.framework.sdk.call.huawei;

import com.ikingtech.framework.sdk.call.CallBindParam;
import com.ikingtech.framework.sdk.call.PrivateCall;
import com.ikingtech.framework.sdk.call.properties.PrivateCallProperties;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class HuaweiPrivateCall implements PrivateCall {

    private final PrivateCallProperties properties;

    public static final String MODULE_NAME = "Huawei-private call";

    @Override
    public String bind(CallBindParam bindParam) {
        HuaweiPrivateCallBindParam huaweiParam = new HuaweiPrivateCallBindParam();

        huaweiParam.setCallerNum(bindParam.getCallerPhone());
        huaweiParam.setCalleeNum(bindParam.getCalleePhone());
        huaweiParam.setRelationNum(bindParam.getRelationNumber());
        huaweiParam.setAreaCode(this.properties.getHuawei().getAreaCode());
        huaweiParam.setCallDirection(Optional.ofNullable(this.properties.getHuawei().getDirection()).orElse(0));
        huaweiParam.setDuration(Optional.ofNullable(this.properties.getHuawei().getBindDuration()).orElse(30));
        huaweiParam.setMaxDuration(Optional.ofNullable(this.properties.getHuawei().getCallDuration()).orElse(0));
        huaweiParam.setPrivateSms(null == this.properties.getHuawei().getSms() ? Boolean.FALSE.toString() : this.properties.getHuawei().getSms().toString());
        if (null != this.properties.getHuawei().getRecordFlag()) {
            huaweiParam.setRecordFlag(this.properties.getHuawei().getRecordFlag().toString());
            huaweiParam.setRecordHintTone(this.properties.getHuawei().getRecordHint());
        }

        String resultStr = HuaweiRequest.post(this.properties.getAppUrl() + "/rest/caas/relationnumber/partners/v1.0",
                Tools.Json.toJsonStr(huaweiParam),
                this.properties.getAppKey(),
                this.properties.getSecretKey());
        HuaweiPrivateCallResponse result = Tools.Json.toBean(resultStr, HuaweiPrivateCallResponse.class);
        if (null == result) {
            throw new FrameworkException(MODULE_NAME, "blank response");
        }
        if (result.fail()) {
            throw new FrameworkException(MODULE_NAME, result.getResultdesc());
        }
        return result.getSubscriptionId();
    }

    @Override
    public void unbind(String bindId) {
        String resultStr = HuaweiRequest.delete(this.properties.getAppUrl() + "/rest/caas/relationnumber/partners/v1.0",
                Tools.Http.toQueryStr(Tools.Coll.newMap(Tools.Coll.Kv.of("subscriptionId", bindId)), true),
                this.properties.getAppKey(),
                this.properties.getSecretKey());
        HuaweiResponse result = Tools.Json.toBean(resultStr, HuaweiPrivateCallResponse.class);
        if (null == result) {
            throw new FrameworkException(MODULE_NAME, "blank response");
        }
        if (result.fail()) {
            throw new FrameworkException(MODULE_NAME, result.getResultdesc());
        }
    }
}
