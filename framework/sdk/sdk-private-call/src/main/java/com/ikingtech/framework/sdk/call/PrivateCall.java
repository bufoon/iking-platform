package com.ikingtech.framework.sdk.call;

/**
 * @author tie yan
 */
public interface PrivateCall {

    /**
     * 绑定手机号隐私通话服务
     *
     * @param bindParam 入参
     * @return 绑定后的唯一标识
     */
    String bind(CallBindParam bindParam);

    /**
     * 解除绑定
     *
     * @param relationNumber 隐私关系编号
     */
    void unbind(String relationNumber);
}
