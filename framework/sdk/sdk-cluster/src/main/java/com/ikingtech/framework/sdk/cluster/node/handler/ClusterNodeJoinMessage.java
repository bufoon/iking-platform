package com.ikingtech.framework.sdk.cluster.node.handler;

import com.ikingtech.framework.sdk.cluster.node.ClusterNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ClusterNodeJoinMessage extends ClusterNodeMessage implements Serializable {

    @Serial
    private static final long serialVersionUID = -356630201893475806L;

    public ClusterNodeJoinMessage(ClusterNode node) {
        this.node = node;
    }

    private ClusterNode node;
}
