package com.ikingtech.framework.sdk.cluster.node.handler;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ClusterNodeMessage implements Serializable {

    @Serial
    private static final long serialVersionUID = -356630201893475806L;

    private String address;

    private Integer port;
}
