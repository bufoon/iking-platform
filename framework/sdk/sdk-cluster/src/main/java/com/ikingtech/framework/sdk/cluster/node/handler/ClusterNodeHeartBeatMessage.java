package com.ikingtech.framework.sdk.cluster.node.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ClusterNodeHeartBeatMessage extends ClusterNodeMessage implements Serializable {

    @Serial
    private static final long serialVersionUID = -356630201893475806L;
}
