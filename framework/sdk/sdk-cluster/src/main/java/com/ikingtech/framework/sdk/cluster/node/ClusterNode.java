package com.ikingtech.framework.sdk.cluster.node;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ClusterNode implements Serializable {

    @Serial
    private static final long serialVersionUID = -6569840375268972595L;

    private String address;

    private Integer port;

    private Long lastUpdateTimestamp;
}
