package com.ikingtech.framework.sdk.approve.embedded;

import com.ikingtech.framework.sdk.approve.model.ApproveFormInstanceBasicDTO;
import com.ikingtech.framework.sdk.approve.model.ApproveFormInstanceDTO;
import com.ikingtech.framework.sdk.approve.model.ApproveFormOperationDTO;
import com.ikingtech.framework.sdk.approve.model.ApproveRecordNodeAttachmentDTO;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentProxy;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author tie yan
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApproveOps {

    public static ApproveFormInstanceBasicDTO submit(ApproveFormInstanceDTO submitParam) {
        R<Object> result = FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.APPROVE_SUBMIT, submitParam);
        if (!result.isSuccess()) {
            throw new FrameworkException(result.getMsg());
        }
        return (ApproveFormInstanceBasicDTO) result.getData();
    }

    public static ApproveFormInstanceBasicDTO pass(ApproveOpsArgs args) {
        R<Object> result = FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.APPROVE_PASS, commonOperation(args));
        if (!result.isSuccess()) {
            throw new FrameworkException(result.getMsg());
        }
        return (ApproveFormInstanceBasicDTO) result.getData();
    }

    public static void passBatchAsync(List<ApproveOpsArgs> args) {
        FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.APPROVE_PASS, commonOperation(args));
    }

    public static ApproveFormInstanceBasicDTO reject(ApproveOpsArgs args) {
        R<Object> result = FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.APPROVE_REJECT, commonOperation(args));
        if (!result.isSuccess()) {
            throw new FrameworkException(result.getMsg());
        }
        return (ApproveFormInstanceBasicDTO) result.getData();
    }

    public static ApproveFormInstanceBasicDTO back(ApproveBackOpsArgs args) {
        ApproveFormOperationDTO operation = commonOperation(args);
        operation.setBackToInstanceNodeId(args.getBackToInstanceNodeId());
        operation.setBackToInstanceNodeUserIds(args.getBackToInstanceNodeUserIds());
        R<Object> result = FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.APPROVE_BACK, operation);
        if (!result.isSuccess()) {
            throw new FrameworkException(result.getMsg());
        }
        return (ApproveFormInstanceBasicDTO) result.getData();
    }

    public static ApproveFormInstanceBasicDTO reSubmit(ApproveOpsArgs args) {
        R<Object> result = FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.APPROVE_RE_SUBMIT, commonOperation(args));
        if (!result.isSuccess()) {
            throw new FrameworkException(result.getMsg());
        }
        return (ApproveFormInstanceBasicDTO) result.getData();
    }

    private static List<ApproveFormOperationDTO> commonOperation(List<ApproveOpsArgs> args) {
        return Tools.Coll.convertList(args, ApproveOps::commonOperation);
    }

    private static ApproveFormOperationDTO commonOperation(ApproveOpsArgs args) {
        ApproveFormOperationDTO operation = new ApproveFormOperationDTO();
        operation.setFormInstanceId(args.getFormInstanceId());
        operation.setFormData(args.getFormData());
        operation.setApproveComment(args.getApproveComment());
        operation.setAttachments(Tools.Coll.convertList(args.getAttachments(), attachmentArgs -> Tools.Bean.copy(attachmentArgs, ApproveRecordNodeAttachmentDTO.class)));
        operation.setImages(args.getImages());
        return operation;
    }
}
