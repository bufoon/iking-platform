package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveFormInstanceDraftQueryParamDTO", description = "审批表单实例草稿查询参数")
public class ApproveFormInstanceDraftQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 3618030857973170795L;

    @Schema(name = "formId", description = "表单编号")
    private String formId;
}
