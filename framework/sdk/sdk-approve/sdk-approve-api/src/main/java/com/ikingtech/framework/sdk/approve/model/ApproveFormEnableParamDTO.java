package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormEnableParamDTO", description = "启用表单参数")
public class ApproveFormEnableParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5654297383494262464L;

    @Schema(name = "formGroupId", description = "表单分组编号")
    private String formGroupId;

    @Schema(name = "formId", description = "表单编号")
    private String formId;
}
