package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveProcessInstanceNodeDTO", description = "审批流程实例节点信息")
public class ApproveProcessInstanceNodeDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1057954524813793827L;

    @Schema(name = "id", description = "节点编号")
    private String id;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "nodeFormId", description = "所属节点表单编号")
    private String nodeFormId;

    @Schema(name = "processId", description = "所属审批流编号")
    private String processId;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "processInstanceId", description = "审批流实例编号")
    private String processInstanceId;

    @Schema(name = "nodeId", description = "节点配置编号")
    private String nodeId;

    @Schema(name = "name", description = "节点名称")
    private String name;

    @Schema(name = "type", description = "节点类型")
    private ApproveProcessNodeTypeEnum type;

    @Schema(name = "typeName", description = "节点类型名称")
    private String typeName;

    @Schema(name = "approveType", description = "审批对象类别")
    private ApproveTypeEnum approveType;

    @Schema(name = "approveType", description = "审批对象类别名称")
    private String approveTypeName;

    @Schema(name = "approvalCategory", description = "审批对象类别")
    private ApprovalCategoryEnum approvalCategory;

    @Schema(name = "organizationWidgetName", description = "组织选择组件名称")
    private String organizationWidgetName;

    @Schema(name = "deptWidgetName", description = "部门选择组件名称")
    private String deptWidgetName;

    @Schema(name = "userWidgetName", description = "用户选择组件名称")
    private String userWidgetName;

    @Schema(name = "formData", description = "节点表单数据")
    private String formData;

    @Schema(name = "initiatorSpecifiedScopeType", description = "发起人自选-选择范围类型")
    private ApproveInitiatorSpecifiedScopeTypeEnum initiatorSpecifiedScopeType;

    @Schema(name = "initiatorSpecify", description = "发起审批时是否能够自选审批人")
    private Boolean initiatorSpecify;

    @Schema(name = "initiatorSpecifiedScope", description = "发起人自选-选择范围类型")
    private List<ApproveProcessInstanceUserDTO> initiatorSpecifiedScope;

    @Schema(name = "singleApproval", description = "发起人自选-选人方式是否单选")
    private Boolean singleApproval;

    @Schema(name = "multiExecutorType", description = "多人审批方式")
    private ApproveMultiExecutorTypeEnum multiExecutorType;

    @Schema(name = "multiExecutorTypeName", description = "多人审批方式名称")
    private String multiExecutorTypeName;

    @Schema(name = "executorEmptyStrategy", description = "审批人为空时的策略")
    private ApproveExecutorEmptyStrategyEnum executorEmptyStrategy;

    @Schema(name = "executorEmptyStrategy", description = "审批人为空时的策略名称")
    private String executorEmptyStrategyName;

    @Schema(name = "reSubmitToBack", description = "再次发起时是否由退回人直接审批")
    private Boolean reSubmitToBack;

    @Schema(name = "backToInitiator", description = "退回时是否直接退回到发起节点")
    private Boolean backToInitiator;

    @Schema(name = "executorEmpty", description = "审批人是否为空")
    private Boolean executorEmpty;

    @Schema(name = "initiatorSpecifyCarbonCopy", description = "是否由发起人指定抄送人")
    private Boolean initiatorSpecifyCarbonCopy;

    @Schema(name = "autoExecuteComment", description = "节点自动执行(通过/拒绝)时的审批意见")
    private String autoExecuteComment;

    @Schema(name = "status", description = "节点执行状态")
    private ApproveProcessInstanceNodeStatusEnum status;

    @Schema(name = "statusName", description = "节点执行状态名称")
    private String statusName;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    @Schema(name = "executorUsers", description = "执行者")
    private List<ApproveProcessInstanceUserDTO> executorUsers;
}
