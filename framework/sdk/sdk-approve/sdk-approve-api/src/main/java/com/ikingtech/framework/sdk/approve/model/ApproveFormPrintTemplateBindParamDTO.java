package com.ikingtech.framework.sdk.approve.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ApproveFormPrintTemplateBindParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5005770763499878207L;

    private String formId;

    private String reportTemplateId;
}
