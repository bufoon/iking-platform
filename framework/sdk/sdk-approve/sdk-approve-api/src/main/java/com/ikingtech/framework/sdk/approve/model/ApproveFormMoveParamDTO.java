package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormMoveParamDTO", description = "表单移动参数")
public class ApproveFormMoveParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 9090066516141610733L;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "formGroupId", description = "移动目标表单分组编号")
    private String formGroupId;
}
