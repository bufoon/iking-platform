package com.ikingtech.framework.sdk.approve.api;

import com.ikingtech.framework.sdk.approve.model.rpc.ApproveFormBeanDefinitionReportParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author tie yan
 */
public interface ApproveFormApi {

    /**
     * 上报流程表单定义信息
     *
     * @param reportParam 流程表单定义信息
     * @return 上报结果
     */
    @PostRequest(order = 1,value = "/report", summary = "上报流程表单定义信息", description = "上报流程表单定义信息")
    R<Object> report(@Parameter(name = "reportParam", description = "流程表单定义信息")
                     @RequestBody ApproveFormBeanDefinitionReportParam reportParam);
}
