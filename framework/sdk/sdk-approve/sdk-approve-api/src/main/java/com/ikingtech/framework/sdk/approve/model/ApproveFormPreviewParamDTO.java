package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveProcessPreviewParamDTO", description = "审批流程预览参数")
public class ApproveFormPreviewParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4541505965478312971L;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "formData", description = "表单数据")
    private String formData;
}
