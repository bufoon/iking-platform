package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveFormQueryParamDTO", description = "审批表单查询参数")
public class ApproveFormQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -1302835110679714700L;

    @Schema(name = "businessType", description = "表单业务类型名称")
    private String businessType;

    @Schema(name = "name", description = "表单名称")
    private String name;

    @Schema(name = "type", description = "表单类型")
    private String type;

    @Schema(name = "configured", description = "是否已配置")
    private Boolean configured;

    @Schema(name = "initiatorViewOnly", description = "发起审批页面/表单管理页面")
    private Boolean initiatorViewOnly;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "createTime", description = "创建时间")
    private LocalDateTime createTime;

    @Schema(name = "executorUserId", description = "审批人编号")
    private String executorUserId;

    @Schema(name = "approvedByExecutorUser", description = "待审/已审")
    private Boolean approvedByExecutorUser;

    @Schema(name = "carbonCopyUserId", description = "抄送人编号")
    private String carbonCopyUserId;

    @Schema(name = "title", description = "表单标题")
    private String title;

    @Schema(name = "initiatorIds", description = "发起人编号")
    private List<String> initiatorIds;

    @Schema(name = "processStatus", description = "流程状态")
    private List<String> processStatus;

    @Schema(name = "serialNo", description = "审批流水号")
    private String serialNo;
}
