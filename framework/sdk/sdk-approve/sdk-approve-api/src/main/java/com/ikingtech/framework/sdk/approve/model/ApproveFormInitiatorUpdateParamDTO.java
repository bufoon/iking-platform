package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormInitiatorUpdateParamDTO", description = "更新表单发起人参数")
public class ApproveFormInitiatorUpdateParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1625551667500524676L;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "initiators", description = "表单发起人列表")
    private List<ApproveFormInitiatorDTO> initiators;
}
