package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveFormInstanceDTO", description = "表单实例详细信息")
public class ApproveFormInstanceDTO extends ApproveFormInstanceBasicDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 9026197117127428389L;

    @Schema(name = "processRecords", description = "审批记录列表")
    private List<ApproveRecordDTO> processRecords;

    @Schema(name = "processInstance", description = "审批流程实例列表")
    private ApproveProcessInstanceDTO processInstance;
}
