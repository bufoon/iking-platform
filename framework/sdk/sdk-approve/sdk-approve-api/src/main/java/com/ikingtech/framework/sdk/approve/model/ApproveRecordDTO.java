package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveRecordDTO", description = "审批记录信息")
public class ApproveRecordDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1950718245815401154L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "processInstanceId", description = "审批流程实例编号")
    private String processInstanceId;

    @Schema(name = "name", description = "审批记录列表名称")
    private String name;

    @Schema(name = "recordNodes", description = "审批记录节点列表")
    private List<ApproveRecordNodeDTO> recordNodes;
}
