package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.approve.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveProcessNodeDTO", description = "审批流程配置节点信息")
public class ApproveProcessNodeDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -156907688322379165L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "所属发起表单编号")
    private String formId;

    @Schema(name = "nodeFormId", description = "所属节点表单编号")
    private String nodeFormId;

    @Schema(name = "processId", description = "所属审批流程编号")
    private String processId;

    @Schema(name = "parentId", description = "前一节点编号")
    private String parentId;

    @Schema(name = "name", description = "节点名称")
    private String name;

    @Schema(name = "type", description = "节点类型")
    private ApproveProcessNodeTypeEnum type;

    @Schema(name = "approveType", description = "审批方式")
    private ApproveTypeEnum approveType;

    @Schema(name = "approvalCategory", description = "审批对象类别")
    private ApprovalCategoryEnum approvalCategory;

    @Schema(name = "organizationWidgetName", description = "组织选择组件名称")
    private String organizationWidgetName;

    @Schema(name = "deptWidgetName", description = "部门选择组件名称")
    private String deptWidgetName;

    @Schema(name = "userWidgetName", description = "用户选择组件名称")
    private String userWidgetName;

    @Schema(name = "approvals", description = "审批对象列表")
    private List<ApproveProcessExecutorDTO> approvals;

    @Schema(name = "initiatorSpecifiedScopeType", description = "发起人自选-选择范围类型")
    private ApproveInitiatorSpecifiedScopeTypeEnum initiatorSpecifiedScopeType;

    @Schema(name = "singleApproval", description = "发起人自选-选人方式是否单选")
    private Boolean singleApproval;

    @Schema(name = "multiExecutorType", description = "多人审批方式")
    private ApproveMultiExecutorTypeEnum multiExecutorType;

    @Schema(name = "executorEmptyStrategy", description = "审批人为空时的策略")
    private ApproveExecutorEmptyStrategyEnum executorEmptyStrategy;

    @Schema(name = "initiatorSpecifyCarbonCopy", description = "是否由发起人指定抄送人")
    private Boolean initiatorSpecifyCarbonCopy;

    @Schema(name = "reSubmitToBack", description = "再次发起时是否由退回人直接审批")
    private Boolean reSubmitToBack;

    @Schema(name = "backToInitiator", description = "退回时是否直接退回到发起节点")
    private Boolean backToInitiator;

    @Schema(name = "reserveApprovals", description = "审批人为空时指定的审批人列表")
    private List<ApproveProcessExecutorDTO> reserveApprovals;

    @Schema(name = "conditionOrder", description = "条件优先级")
    private Integer conditionOrder;

    @Schema(name = "conditionGroups", description = "工作流条件节点条件组集合")
    private List<ApproveCondGroupDTO> conditionGroups;

    @Schema(name = "permissions", description = "审批节点表单权限")
    private List<ApproveProcessPermissionDTO> permissions;

    @Schema(name = "autoExecuteComment", description = "节点自动执行(通过/拒绝)时的审批意见")
    private String autoExecuteComment;

    @Schema(name = "formView", description = "节点表单视图信息")
    private ApproveFormViewDTO formView;

    @Schema(name = "conditions", description = "条件节点列表")
    private List<ApproveProcessNodeDTO> conditions;

    @Schema(name = "children", description = "子节点")
    private ApproveProcessNodeDTO children;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
