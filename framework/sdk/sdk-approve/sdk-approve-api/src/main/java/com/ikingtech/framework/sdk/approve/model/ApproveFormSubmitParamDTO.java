package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormSubmitParamDTO", description = "审批流程提交参数")
public class ApproveFormSubmitParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8648024288477161561L;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "formData", description = "表单数据")
    private String formData;

    @Schema(name = "processInstance", description = "审批流程实例")
    private ApproveProcessInstanceDTO processInstance;
}
