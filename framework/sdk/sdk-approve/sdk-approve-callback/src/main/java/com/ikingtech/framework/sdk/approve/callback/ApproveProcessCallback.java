package com.ikingtech.framework.sdk.approve.callback;

/**
 * @author tie yan
 */
public interface ApproveProcessCallback {

    void invoke(ApproveCallbackParam param);

    String businessType();
}
