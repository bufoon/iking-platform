package com.ikingtech.framework.sdk.approve.callback.runner;

import com.ikingtech.framework.sdk.approve.callback.ApproveCallbackParam;
import com.ikingtech.framework.sdk.approve.callback.ApproveCallbackResolver;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.core.support.LogHelper;
import com.ikingtech.framework.sdk.enums.common.FrameworkServerFeedbackTypeEnum;
import com.ikingtech.framework.sdk.web.support.server.FrameworkServerFeedbackRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class ApproveCallbackFeedbackRunner implements FrameworkServerFeedbackRunner {

    private final ApproveCallbackResolver resolver;

    @Override
    public R<Object> run(Object data) {
        ApproveCallbackParam param = (ApproveCallbackParam) data;
        R<Object> result = this.resolver.resolve(param);
        LogHelper.info("APPROVE CALLBACK FEEDBACK", "审批回调执行结果[{}]", result);
        return result;
    }

    @Override
    public FrameworkServerFeedbackTypeEnum type() {
        return FrameworkServerFeedbackTypeEnum.APPROVE_PROCESS_CALLBACK_FEEDBACK;
    }
}
