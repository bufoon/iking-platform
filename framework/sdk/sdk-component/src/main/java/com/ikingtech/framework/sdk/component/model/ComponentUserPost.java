package com.ikingtech.framework.sdk.component.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ComponentUserPost", description = "业务组件-用户岗位信息")
public class ComponentUserPost implements Serializable {

    @Serial
    private static final long serialVersionUID = -335079907050285925L;

    @Schema(name = "ComponentUserDept", description = "主键")
    private String id;

    @Schema(name = "ComponentUserDept", description = "用户编号")
    private String userId;

    @Schema(name = "ComponentUserDept", description = "岗位编号")
    private String postId;
}
