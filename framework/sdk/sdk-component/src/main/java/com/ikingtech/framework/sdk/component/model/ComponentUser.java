package com.ikingtech.framework.sdk.component.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ComponentUser", description = "业务组件-用户信息")
public class ComponentUser extends PickerElementDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 3304747527031969066L;

    @Schema(name = "avatar", description = "用户头像")
    private String avatar;

    @Schema(name = "posts", description = "用户岗位集合")
    private List<ComponentPost> posts;

    @Schema(name = "departments", description = "用户部门集合")
    private List<ComponentDepartment> departments;

    @Schema(name = "departmentFullNames", description = "用户部门名称(全路径)集合")
    private List<String> departmentFullNames;
}
