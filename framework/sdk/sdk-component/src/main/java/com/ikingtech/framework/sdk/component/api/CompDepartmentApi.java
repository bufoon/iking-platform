package com.ikingtech.framework.sdk.component.api;

import com.ikingtech.framework.sdk.component.model.ComponentDepartment;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@Service
public interface CompDepartmentApi {

    List<ComponentDepartment> listByName(String name);

    List<ComponentDepartment> listByParentId(String parentId);

    ComponentDepartment getParentDepartment(String parentId);

    Map<String, String> extractDepartmentFullName(Map<String, String> deptFullPathMap);
}
