package com.ikingtech.framework.sdk.component.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ComponentOrganization", description = "业务组件-组织信息")
public class ComponentOrganization extends PickerElementDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1808159082429157203L;

    @Schema(name = "fullPath", description = "组织全路径")
    private String fullPath;

    @Schema(name = "fullName", description = "组织名称(全路径)")
    private String fullName;

    @Schema(name = "grade", description = "组织级别")
    private Integer grade;
}
