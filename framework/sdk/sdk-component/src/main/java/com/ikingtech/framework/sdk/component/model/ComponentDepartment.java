package com.ikingtech.framework.sdk.component.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ComponentDepartment", description = "业务组件-部门信息")
public class ComponentDepartment extends PickerElementDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 889988166549158768L;

    @Schema(name = "fullPath", description = "部门全路径")
    private String fullPath;

    @Schema(name = "fullName", description = "部门名称(全路径)")
    private String fullName;

    @Schema(name = "managerId", description = "部门主管编号")
    private String managerId;

    @Schema(name = "managerName", description = "部门主管名称")
    private String managerName;

    @Schema(name = "userCount", description = "用户数")
    private Integer userCount;
}
