package com.ikingtech.framework.sdk.excel.model;

import com.ikingtech.framework.sdk.utils.Tools;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public class TableWorkSheet<T extends Serializable> extends WorkSheet<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = 1218051010211432395L;

    @Override
    public void writeContent() {
        Map<Integer, List<TemplateCell>> templateMap = Tools.Coll.convertGroup(this.getTemplate(), TemplateCell::getStartLine);
        for (int i = 0; i < this.getData().size(); i++) {
            Map<String, T> cellDataMap = Tools.Bean.convertToMap(this.getData().get(i));
            for (Map.Entry<Integer, List<TemplateCell>> entry : templateMap.entrySet()) {
                this.writeCell(i + entry.getKey(), entry.getValue(), cellDataMap);
            }
        }
    }
}
