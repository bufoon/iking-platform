package com.ikingtech.framework.sdk.excel.enums;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum WorkCellFontPositionEnum {

    TOP(null, VerticalAlignment.TOP),

    TOP_RIGHT(HorizontalAlignment.RIGHT, VerticalAlignment.TOP),

    TOP_LEFT(HorizontalAlignment.LEFT, VerticalAlignment.TOP),

    RIGHT(HorizontalAlignment.RIGHT, null),

    LEFT(HorizontalAlignment.LEFT, null),

    BOTTOM(null, VerticalAlignment.BOTTOM),

    BOTTOM_RIGHT(HorizontalAlignment.RIGHT, VerticalAlignment.TOP),

    BOTTOM_LEFT(HorizontalAlignment.LEFT, VerticalAlignment.TOP),

    CENTER(HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

    public final HorizontalAlignment horizon;

    public final VerticalAlignment vertical;
}
