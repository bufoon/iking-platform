package com.ikingtech.framework.sdk.excel.enums;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.BorderStyle;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum WorkCellBorderStyleEnum {

    TOP(BorderStyle.THIN, BorderStyle.NONE, BorderStyle.NONE, BorderStyle.NONE),

    LEFT(BorderStyle.NONE, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.NONE),

    RIGHT(BorderStyle.NONE, BorderStyle.THIN, BorderStyle.NONE, BorderStyle.NONE),

    BOTTOM(BorderStyle.NONE, BorderStyle.NONE, BorderStyle.NONE, BorderStyle.THIN),

    TOP_BOTTOM(BorderStyle.THIN, BorderStyle.NONE, BorderStyle.NONE, BorderStyle.THIN),

    TOP_RIGHT(BorderStyle.THIN, BorderStyle.THIN, BorderStyle.NONE, BorderStyle.NONE),

    TOP_LEFT(BorderStyle.THIN, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.NONE),

    TOP_RIGHT_BOTTOM(BorderStyle.THIN, BorderStyle.THIN, BorderStyle.NONE, BorderStyle.THIN),

    TOP_LEFT_BOTTOM(BorderStyle.THIN, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THIN),

    RIGHT_BOTTOM(BorderStyle.NONE, BorderStyle.THIN, BorderStyle.NONE, BorderStyle.THIN),

    RIGHT_LEFT(BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.NONE),

    RIGHT_BOTTOM_LEFT(BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN),

    LEFT_BOTTOM(BorderStyle.NONE, BorderStyle.NONE, BorderStyle.THIN, BorderStyle.THIN),

    ALL(BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN),

    NONE(BorderStyle.NONE, BorderStyle.NONE, BorderStyle.NONE, BorderStyle.NONE);

    public final BorderStyle topStyle;

    public final BorderStyle rightStyle;

    public final BorderStyle leftStyle;

    public final BorderStyle bottomStyle;
}
