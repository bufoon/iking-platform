package com.ikingtech.framework.sdk.plugin.core;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class PluggableClass implements Serializable {

    @Serial
    private static final long serialVersionUID = 3668041205571716871L;

    private String name;

    private Boolean bean;

    private Boolean controller;
}
