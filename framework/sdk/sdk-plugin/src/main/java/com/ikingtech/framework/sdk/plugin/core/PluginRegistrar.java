package com.ikingtech.framework.sdk.plugin.core;

import com.ikingtech.framework.sdk.enums.plugin.PluginTypeEnum;

/**
 * @author tie yan
 */
public interface PluginRegistrar {

    void register(PluginRegistrarArgs args);

    void unregister(PluginRegistrarArgs args);

    default PluginTypeEnum type() {
        return null;
    }
}
