package com.ikingtech.framework.sdk.plugin.core;

import com.ikingtech.framework.sdk.enums.plugin.PluginTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class PluginRegistrarArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = 3438401481279480454L;

    private String jarFileName;

    private PluginTypeEnum type;

    private List<PluggableClass> classes;
}
