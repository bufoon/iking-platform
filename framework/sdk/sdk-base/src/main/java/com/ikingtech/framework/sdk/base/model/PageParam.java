package com.ikingtech.framework.sdk.base.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 排序参数基类
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "PageParam", description = "分页参数基类")
public class PageParam extends OrderParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -264593652041552884L;

    @Schema(name = "page", description = "页码")
    private int page;

    @Schema(name = "rows", description = "每页行数")
    private int rows;
}
