package com.ikingtech.framework.sdk.base.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 排序参数基类
 *
 * @author tie yan
 */
@Data
@Schema(name = "PageParam", description = "排序参数基类")
public class OrderParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -4011176057806905278L;

    @Schema(name = "order", description = "排序方向 升序ASC 降序DESC")
    private String order;

    @Schema(name = "orderBy", description = "排序字段名")
    private String orderBy;
}
