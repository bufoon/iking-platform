package com.ikingtech.platform.gateway.configuration;

import com.ikingtech.framework.sdk.authenticate.operation.IdentityValidator;
import com.ikingtech.platform.gateway.filter.AuthenticationGlobalFilter;
import com.ikingtech.platform.gateway.properties.GatewayProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.config.conditional.ConditionalOnEnabledFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author tie yan
 */
@Configuration
@EnableConfigurationProperties({GatewayProperties.class})
public class GlobalFilterConfiguration {

    @Bean
    @ConditionalOnEnabledFilter
    public AuthenticationGlobalFilter authenticationGlobalFilter(IdentityValidator identityValidator,
                                                                 GatewayProperties gatewayProperties,
                                                                 StringRedisTemplate stringRedisTemplate) {
        return new AuthenticationGlobalFilter(identityValidator, gatewayProperties, stringRedisTemplate);
    }
}
