package com.ikingtech.platform.service.pay.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cashier_supplier")
public class CashierSupplierDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -3403527898838309070L;

    @TableField(value = "type")
    private String type;

    @TableField(value = "name")
    private String name;

    @TableField(value = "custom_id")
    private String customId;

    @TableField(value = "custom_name")
    private String customName;

    @TableField(value = "custom_serial_no")
    private String customSerialNo;

    @TableField(value = "app_id")
    private String appId;

    @TableField(value = "api_key")
    private String apiKey;

    @TableField(value = "extra_api_key")
    private String extraApiKey;

    @TableField(value = "private_key")
    private String privateKey;

    @TableField(value = "cert_key")
    private String certKey;

    @TableField(value = "public_key")
    private String publicKey;

    @TableField(value = "notify_url")
    private String notifyUrl;
}
