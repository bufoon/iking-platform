package com.ikingtech.platform.service.pay.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.pay.entity.PayRecordDO;
import com.ikingtech.platform.service.pay.mapper.PayRecordMapper;

/**
 * @author tie yan
 */
public class PayRecordRepository extends ServiceImpl<PayRecordMapper, PayRecordDO> {
}
