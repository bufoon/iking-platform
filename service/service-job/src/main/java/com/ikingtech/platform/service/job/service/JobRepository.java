package com.ikingtech.platform.service.job.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.job.entity.JobDO;
import com.ikingtech.platform.service.job.mapper.JobMapper;

/**
 * @author tie yan
 */
public class JobRepository extends ServiceImpl<JobMapper, JobDO> {
}
