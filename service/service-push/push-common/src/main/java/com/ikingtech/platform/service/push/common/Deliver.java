package com.ikingtech.platform.service.push.common;

/**
 * @author tie yan
 */
public interface Deliver {

    DeliverResult send(PushRequest body, String receiverId);

    DeliverTypeEnum type();
}
