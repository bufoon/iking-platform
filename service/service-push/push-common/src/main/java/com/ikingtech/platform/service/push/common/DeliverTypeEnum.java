package com.ikingtech.platform.service.push.common;

/**
 * @author tie yan
 */

public enum DeliverTypeEnum {

    WEBSOCKET,

    WECHAT_MINI,

    DING_TALK,

    SMS
}
