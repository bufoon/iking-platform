package com.ikingtech.platform.service.push.common;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@AllArgsConstructor
public class DeliverResult implements Serializable {

    @Serial
    private static final long serialVersionUID = 4043723037225562587L;

    /**
     * 是否发送成功
     */
    private Boolean success;

    /**
     * 失败原因
     */
    private String cause;

    public static DeliverResult success() {
        return new DeliverResult(true, Tools.Str.EMPTY);
    }

    public static DeliverResult fail(String cause) {
        return new DeliverResult(false, cause);
    }
}
