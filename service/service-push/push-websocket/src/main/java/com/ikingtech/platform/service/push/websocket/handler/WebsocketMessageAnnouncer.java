package com.ikingtech.platform.service.push.websocket.handler;

/**
 * @author tie yan
 */
public interface WebsocketMessageAnnouncer {

    void broadcast(String userId, String message);
}
