package com.ikingtech.platform.service.push.websocket.configuration;

import com.ikingtech.platform.service.push.websocket.handler.WebsocketHandler;
import com.ikingtech.platform.service.push.websocket.interceptor.WebsocketInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class WebsocketHandlerConfigure implements WebSocketConfigurer {

    private final WebsocketHandler websocketHandler;

    private final WebsocketInterceptor websocketInterceptor;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry
                .addHandler(this.websocketHandler, "ws/message")
                .addInterceptors(this.websocketInterceptor)
                .setAllowedOrigins("*");
    }
}
