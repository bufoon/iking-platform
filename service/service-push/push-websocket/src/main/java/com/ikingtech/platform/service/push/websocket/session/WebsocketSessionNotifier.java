package com.ikingtech.platform.service.push.websocket.session;

/**
 * @author tie yan
 */
public interface WebsocketSessionNotifier {

    /**
     * 上线提醒
     *
     * @param userId  用户编号
     */
    default void online(String userId) {
        // do noting
    }

    /**
     * 离线提醒
     *
     * @param userId  用户编号
     */
    default void offline(String userId) {
        // do noting
    }
}
