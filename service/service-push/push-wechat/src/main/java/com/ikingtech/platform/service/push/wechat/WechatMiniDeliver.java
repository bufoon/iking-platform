package com.ikingtech.platform.service.push.wechat;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.wechat.mini.api.WechatMiniApi;
import com.ikingtech.framework.sdk.wechat.mini.model.WechatMiniSendMessageParamDTO;
import com.ikingtech.platform.service.push.common.Deliver;
import com.ikingtech.platform.service.push.common.DeliverResult;
import com.ikingtech.platform.service.push.common.DeliverTypeEnum;
import com.ikingtech.platform.service.push.common.PushRequest;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class WechatMiniDeliver implements Deliver {

    private final WechatMiniApi wechatMiniApi;

    @Override
    public DeliverResult send(PushRequest pushRequest, String miniOpenId) {
        WechatMiniSendMessageParamDTO sendParam = new WechatMiniSendMessageParamDTO();
        sendParam.setWechatMiniId(pushRequest.getAppConfigId());
        sendParam.setTemplateId(pushRequest.getTemplateId());
        sendParam.setOpenId(miniOpenId);
        sendParam.setRedirectTo(Tools.Coll.isBlank(pushRequest.getBody().getRedirectTo()) ? Tools.Str.EMPTY : pushRequest.getBody().getRedirectTo().get(0).getRedirectLink());
        sendParam.setParam(pushRequest.getTemplateParams());
        R<Object> result = this.wechatMiniApi.sendSubscribeMessage(sendParam);
        return new DeliverResult(result.isSuccess(), result.getMsg());
    }

    @Override
    public DeliverTypeEnum type() {
        return DeliverTypeEnum.WECHAT_MINI;
    }
}
