package com.ikingtech.platform.service.push.dingtalk.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * created on 2023-05-24 16:42
 *
 * @author wub
 */

@Getter
@AllArgsConstructor
public enum DingTalkConfig {

    /**
     * 独立跳转ActionCard类型
     */
    ACTION_CARD("actionCard"),
    /**
     * 卡片按钮横向
     */
    BTN_ACROSS("1"),
    /**
     * 卡片按钮竖向
     */
    BTN_VERTICAL("0"),
    /**
     * 加签使用的算法
     */
    HMAC_ALGORITHM("HmacSHA256");

    private final String value;
}
