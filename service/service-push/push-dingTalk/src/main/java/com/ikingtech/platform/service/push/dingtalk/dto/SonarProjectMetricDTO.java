package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@NoArgsConstructor
@Data
public class SonarProjectMetricDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -8961207300471998017L;

    private ComponentDTO component;

    @Data
    public static class ComponentDTO implements Serializable {

        @Serial
        private static final long serialVersionUID = 6104976010107652091L;

        private String key;
        private String name;
        private String qualifier;
        private List<MeasuresDTO> measures;

        @Data
        public static class MeasuresDTO implements Serializable {

            @Serial
            private static final long serialVersionUID = 7233413675995812571L;

            private String metric;
            private String value;
            private Boolean bestValue;
        }
    }
}
