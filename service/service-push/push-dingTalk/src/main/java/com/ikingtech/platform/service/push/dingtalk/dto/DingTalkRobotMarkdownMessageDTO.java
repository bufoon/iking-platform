package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 普通消息类型
 *
 * @author tie yan
 */
@Data
public class DingTalkRobotMarkdownMessageDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -8212866174443332742L;

    private String title;

    private String text;
}
