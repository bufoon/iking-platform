package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author wub
 */
@Data
@AllArgsConstructor
public class DeliverResult implements Serializable {


    @Serial
    private static final long serialVersionUID = -5543741968255938340L;
    /**
     * 是否发送成功
     */
    private Boolean success;

    /**
     * 失败原因
     */
    private String cause;
}
