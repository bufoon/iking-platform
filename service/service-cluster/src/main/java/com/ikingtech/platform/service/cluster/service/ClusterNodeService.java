package com.ikingtech.platform.service.cluster.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.cluster.entity.ClusterNodeDO;
import com.ikingtech.platform.service.cluster.mapper.ClusterNodeMapper;
import org.springframework.stereotype.Service;

/**
 * @author tie yan
 */
@Service
public class ClusterNodeService extends ServiceImpl<ClusterNodeMapper, ClusterNodeDO> {
}
