package com.ikingtech.platform.service.cluster.server;

import com.ikingtech.framework.sdk.cluster.node.handler.ClusterNodeMessageHandlerProxy;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class ClusterServerHandler extends ChannelInboundHandlerAdapter {

    private final ClusterNodeMessageHandlerProxy nodeMessageHandlerProxy;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        this.nodeMessageHandlerProxy.invoke(msg).handle(msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.fireExceptionCaught(cause);
    }
}
