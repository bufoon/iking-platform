package com.ikingtech.platform.service.cluster.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.cluster.entity.ClusterNodeDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface ClusterNodeMapper extends BaseMapper<ClusterNodeDO> {
}
