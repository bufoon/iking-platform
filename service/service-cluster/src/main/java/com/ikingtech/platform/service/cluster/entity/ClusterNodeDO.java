package com.ikingtech.platform.service.cluster.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cluster_node")
public class ClusterNodeDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -7768325102248014033L;

    @TableField(value = "name")
    private String name;

    @TableField(value = "address")
    private String address;

    @TableField(value = "port")
    private Integer port;

    @TableField(value = "alive")
    private Boolean alive;

    @TableField(value = "last_update_timestamp")
    private Long lastUpdateTimestamp;
}
