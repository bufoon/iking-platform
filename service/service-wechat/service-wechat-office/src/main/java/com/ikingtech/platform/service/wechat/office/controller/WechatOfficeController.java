package com.ikingtech.platform.service.wechat.office.controller;

import com.ikingtech.framework.sdk.web.annotation.ApiController;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
@ApiController(value = "/wechat/office", name = "微信公众号管理", description = "微信公众号管理")
public class WechatOfficeController {
}
