package com.ikingtech.platform.service.wechat.mini.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wechat_mini")
public class WechatMiniDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1169180175362779861L;

    @TableField(value = "tenant_code")
    private String tenantCode;

    @TableField(value = "code")
    private String code;

    @TableField(value = "name")
    private String name;

    @TableField(value = "app_id")
    private String appId;

    @TableField(value = "app_secret")
    private String appSecret;
}
