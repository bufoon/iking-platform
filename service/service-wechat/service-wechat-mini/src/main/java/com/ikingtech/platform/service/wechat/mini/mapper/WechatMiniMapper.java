package com.ikingtech.platform.service.wechat.mini.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.wechat.mini.entity.WechatMiniDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface WechatMiniMapper extends BaseMapper<WechatMiniDO> {
}
