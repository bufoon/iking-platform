package com.ikingtech.platform.service.wechat.mini.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.log.embedded.annotation.OperationLog;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.wechat.embedded.mini.*;
import com.ikingtech.framework.sdk.wechat.embedded.mini.resposne.WechatMiniResponseSession;
import com.ikingtech.framework.sdk.wechat.mini.api.WechatMiniApi;
import com.ikingtech.framework.sdk.wechat.mini.model.*;
import com.ikingtech.platform.service.wechat.mini.entity.WechatMiniDO;
import com.ikingtech.platform.service.wechat.mini.exception.WechatMiniExceptionInfo;
import com.ikingtech.platform.service.wechat.mini.service.WechatMiniRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
@ApiController(value = "/wechat/mini", name = "微信管理-微信小程序管理", description = "微信管理-微信小程序管理")
public class WechatMiniController implements WechatMiniApi {

    private final WechatMiniRepository repo;

    private final WechatMiniRequestBuilder wechatMiniRequestBuilder;

    /**
     * 添加微信小程序
     *
     * @param wechatMini 微信小程序信息
     * @return 添加结果
     */
    @Override
    @OperationLog(value = "新增微信小程序", dataId = "#_res.getData()")
    @Transactional(rollbackFor = Exception.class)
    public R<String> add(WechatMiniDTO wechatMini) {
        // 将微信小程序信息转换为WechatMiniDO对象
        WechatMiniDO entity = Tools.Bean.copy(wechatMini, WechatMiniDO.class);
        // 生成唯一ID
        entity.setId(Tools.Id.uuid());
        // 生成唯一code
        entity.setCode(entity.getId());
        // 设置租户code
        entity.setTenantCode(Me.tenantCode());
        // 保存WechatMiniDO对象到数据库
        this.repo.save(entity);
        // 返回添加结果
        return R.ok(entity.getId());
    }

    /**
     * 删除指定id的数据
     *
     * @param id 要删除的数据的id
     * @return 删除结果
     */
    @Override
    @OperationLog(value = "删除微信小程序")
    @Transactional(rollbackFor = Exception.class)
    public R<Object> delete(String id) {
        // 通过id删除数据
        this.repo.removeById(id);
        // 返回删除成功的结果
        return R.ok();
    }

    /**
     * 更新微信小程序信息
     * @param wechatMini 微信小程序信息
     * @return 更新结果
     */
    @Override
    @OperationLog(value = "更新微信小程序")
    @Transactional(rollbackFor = Exception.class)
    public R<Object> update(WechatMiniDTO wechatMini) {
        // 检查微信小程序是否存在
        if (!this.repo.exists(Wrappers.<WechatMiniDO>lambdaQuery().eq(WechatMiniDO::getId, wechatMini.getId()))) {
            throw new FrameworkException(WechatMiniExceptionInfo.WECHAT_MINI_NOT_FOUND);
        }
        // 更新微信小程序信息
        this.repo.updateById(Tools.Bean.copy(wechatMini, WechatMiniDO.class));
        return R.ok();
    }

    /**
     * 分页查询微信小程序
     *
     * @param queryParam 查询参数
     * @return 返回分页结果
     */
    @Override
    public R<List<WechatMiniDTO>> page(WechatMiniQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<WechatMiniDO>lambdaQuery()
                .eq(Tools.Str.isNotBlank(queryParam.getWechatMiniId()), WechatMiniDO::getCode, queryParam.getWechatMiniId())
                .like(Tools.Str.isNotBlank(queryParam.getName()), WechatMiniDO::getName, queryParam.getName())
                .eq(Tools.Str.isNotBlank(queryParam.getAppId()), WechatMiniDO::getAppId, queryParam.getAppId())
                .eq(Tools.Str.isNotBlank(Me.tenantCode()), WechatMiniDO::getTenantCode, Me.tenantCode()))).convert(entity -> Tools.Bean.copy(entity, WechatMiniDTO.class)));
    }

    /**
     * 获取所有微信小程序信息
     *
     * @return 返回所有微信小程序信息
     */
    @Override
    public R<List<WechatMiniDTO>> all() {
        return R.ok(Tools.Coll.convertList(this.repo.list(Wrappers.<WechatMiniDO>lambdaQuery().eq(Tools.Str.isNotBlank(Me.tenantCode()), WechatMiniDO::getTenantCode, Me.tenantCode())), entity -> Tools.Bean.copy(entity, WechatMiniDTO.class)));
    }

    /**
     * 获取微信小程序详情
     * @param id 小程序ID
     * @return 小程序详情
     */
    @Override
    public R<WechatMiniDTO> detail(String id) {
        // 根据ID查询小程序实体
        WechatMiniDO entity = this.repo.getById(id);
        // 如果查询结果为空，则抛出异常
        if (null == entity) {
            throw new FrameworkException(WechatMiniExceptionInfo.WECHAT_MINI_NOT_FOUND);
        }
        // 将实体转换为微信小程序DTO并返回
        return R.ok(Tools.Bean.copy(entity, WechatMiniDTO.class));
    }

    /**
     * 发送订阅消息
     * @param sendMessageParam 发送消息参数
     * @return 操作结果
     */
    @Override
    public R<Object> sendSubscribeMessage(WechatMiniSendMessageParamDTO sendMessageParam) {
        // 构建微信请求对象
        this.wechatMiniRequestBuilder.build(this.loadConfig(sendMessageParam.getWechatMiniId()))
                // 发送订阅消息
                .sendSubscribeMessage(WechatMiniSendSubscribeMessageArgs.builder()
                        .templateId(sendMessageParam.getTemplateId())
                        .openId(sendMessageParam.getOpenId())
                        .redirectTo(sendMessageParam.getRedirectTo())
                        .params(sendMessageParam.getParam())
                        .build());
        return R.ok();
    }

    /**
     * 通过授权码换取微信小程序用户信息
     * @param exchangeCodeParam 交换code参数
     * @return 换取的微信小程序用户信息
     */
    @Override
    public R<WechatMiniUserInfoDTO> exchangeCode(WechatMiniExchangeCodeParamDTO exchangeCodeParam) {
        // 构建微信小程序请求对象
        WechatMiniResponseSession wechatMiniSessionInfo = this.wechatMiniRequestBuilder.build(this.loadConfig(exchangeCodeParam.getWechatMiniId())).exchangeCode(exchangeCodeParam.getCode());
        // 创建微信小程序用户信息对象
        WechatMiniUserInfoDTO userInfo = new WechatMiniUserInfoDTO();
        // 设置微信小程序用户信息
        userInfo.setUnionId(wechatMiniSessionInfo.getUnionid());
        userInfo.setOpenid(wechatMiniSessionInfo.getOpenid());
        userInfo.setSessionKey(wechatMiniSessionInfo.getSessionKey());
        // 返回换取的微信小程序用户信息
        return R.ok(userInfo);
    }

    /**
     * 根据授权码获取手机号
     * @param queryParam 查询参数
     * @return 返回手机号信息
     */
    @Override
    public R<WechatMiniPhoneDTO> getPhoneByCode(WechatMiniPhoneQueryParamDTO queryParam) {
        // 构建微信小程序请求
        WechatMiniUserPhoneInfo userPhoneInfo = this.wechatMiniRequestBuilder.build(this.loadConfig(queryParam.getWechatMiniId())).getPhone(queryParam.getCode());
        // 创建手机号对象
        WechatMiniPhoneDTO phone = new WechatMiniPhoneDTO();
        // 设置手机号
        phone.setPhoneNo(userPhoneInfo.getPhone());
        // 设置手机号（无国家码）
        phone.setPurePhoneNo(userPhoneInfo.getPurePhone());
        // 设置国家码
        phone.setCountryCode(userPhoneInfo.getCountryCode());
        // 返回手机号信息
        return R.ok(phone);
    }

    /**
     * 生成微信小程序二维码
     *
     * @param generateParam 二维码生成参数
     * @return 二维码的Base64字符串
     */
    @Override
    public R<String> qrcode(WechatMiniQrcodeGenerateParamDTO generateParam) {
        // 构建微信小程序请求参数
        String qrcodeBase64Str = this.wechatMiniRequestBuilder.build(this.loadConfig(generateParam.getWechatMiniId()))
                .qrcodeGenerate(WechatMiniQrcodeGenerateArgs.builder()
                        .page(generateParam.getPage())
                        .width(generateParam.getWidth())
                        .param(generateParam.getParam())
                        .version(generateParam.getVersion())
                        .build());
        // 返回二维码的Base64字符串
        return R.ok(qrcodeBase64Str);
    }

    /**
     * 生成微信小程序的链接
     * @param generateParam 生成链接的参数
     * @return 生成的链接
     */
    @Override
    public R<String> urlLink(WechatMiniUrlLinkGenerateParamDTO generateParam) {
        // 通过微信小程序ID加载配置
        String urlLink = this.wechatMiniRequestBuilder.build(this.loadConfig(generateParam.getWechatMiniId()))
                // 生成链接
                .urlLinkGenerate(WechatMiniUrlLinkGenerateArgs.builder()
                        .page(generateParam.getPage())
                        .param(generateParam.getParam())
                        .build());
        return R.ok(urlLink);
    }

    /**
     * 获取订阅消息模板列表
     * @param wechatMiniId 微信小程序ID
     * @return 返回订阅消息模板列表
     */
    @Override
    public R<List<WechatMiniSubscribeMessageTemplateDTO>> listSubscribeMessageTemplate(String wechatMiniId) {
        // 构建微信小程序请求
        List<WechatMiniSubscribeMessageTemplateInfo> messageTemplates = this.wechatMiniRequestBuilder.build(this.loadConfig(wechatMiniId)).listSubscribeMessageTemplate();
        // 将模板信息转换为订阅消息模板DTO对象
        return R.ok(Tools.Coll.convertList(messageTemplates, templateInfo -> {
            WechatMiniSubscribeMessageTemplateDTO template = new WechatMiniSubscribeMessageTemplateDTO();
            template.setId(templateInfo.getTemplateId());
            template.setWechatMiniId(wechatMiniId);
            template.setName(templateInfo.getTemplateTitle());
            template.setTitle(templateInfo.getTemplateTitle());
            template.setContent(templateInfo.getContent());
            template.setType(templateInfo.getType());
            template.setTypeName(templateInfo.getType().description);
            template.setParams(templateInfo.getParams());
            return template;
        }));
    }

    /**
     * 加载微信小程序配置
     * @param wechatMiniId 微信小程序ID
     * @return 微信小程序配置对象
     */
    private WechatMiniConfig loadConfig(String wechatMiniId) {
        // 通过ID查询微信小程序实体对象
        WechatMiniDO entity = this.repo.getById(wechatMiniId);

        // 如果实体对象为空，则从属性文件中读取微信小程序配置信息
        if (null == entity) {
            throw new FrameworkException("wechatMiniNotFound");
        }
        // 如果实体对象不为空，则将实体对象转换为微信小程序配置对象
        return Tools.Bean.copy(entity, WechatMiniConfig.class);
    }
}
