package com.ikingtech.platform.service.log.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.log.model.OperationLogQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.log.entity.OperationLogDO;
import com.ikingtech.platform.service.log.mapper.OperationLogMapper;

/**
 * @author tie yan
 */
public class OperationLogRepository extends ServiceImpl<OperationLogMapper, OperationLogDO> {

    public static LambdaQueryWrapper<OperationLogDO> createWrapper(OperationLogQueryParamDTO queryParam) {
        return Wrappers.<OperationLogDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getMethod()), OperationLogDO::getMethod, queryParam.getMethod())
                .eq(Tools.Str.isNotBlank(queryParam.getRequestMethod()), OperationLogDO::getRequestMethod, queryParam.getRequestMethod())
                .like(Tools.Str.isNotBlank(queryParam.getRequestUrl()), OperationLogDO::getRequestUrl, queryParam.getRequestUrl())
                .eq(Tools.Str.isNotBlank(queryParam.getOperateUserId()), OperationLogDO::getOperateUserId, queryParam.getOperateUserId())
                .like(Tools.Str.isNotBlank(queryParam.getOperateUsername()), OperationLogDO::getOperateUsername, queryParam.getOperateUsername())
                .like(Tools.Str.isNotBlank(queryParam.getIp()), OperationLogDO::getIp, queryParam.getIp())
                .like(Tools.Str.isNotBlank(queryParam.getLocation()), OperationLogDO::getLocation, queryParam.getLocation())
                .like(Tools.Str.isNotBlank(queryParam.getMessage()), OperationLogDO::getMessage, queryParam.getMessage())
                .eq(null != queryParam.getSuccess(), OperationLogDO::getSuccess, queryParam.getSuccess())
                .ge(null != queryParam.getOperationStartTime(), OperationLogDO::getOperationTime, queryParam.getOperationStartTime())
                .le(null != queryParam.getOperationEndTime(), OperationLogDO::getOperationTime, queryParam.getOperationEndTime())
                .orderByDesc(OperationLogDO::getCreateTime);
    }
}
