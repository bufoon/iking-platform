package com.ikingtech.platform.service.log.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("log_repo")
public class LogRepoDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -6462669895841243124L;

    @TableField(value = "log_type")
    private String logType;

    @TableField(value = "log_table_name")
    private String logTableName;

    @TableField("tenant_code")
    private String tenantCode;
}
