package com.ikingtech.platform.service.log.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("auth_log")
public class AuthLogDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -8784030943992976403L;

    @TableField("domain_code")
    private String domainCode;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("app_code")
    private String appCode;

    @TableField(value = "user_id")
    private String userId;

    @TableField(value = "username")
    private String username;

    @TableField(value = "type")
    private String type;

    @TableField(value = "ip")
    private String ip;

    @TableField(value = "location")
    private String location;

    @TableField(value = "browser")
    private String browser;

    @TableField(value = "os")
    private String os;

    @TableField(value = "success")
    private Boolean success;

    @TableField(value = "message")
    private String message;

    @TableField(value = "sign_time")
    private LocalDateTime signTime;
}
