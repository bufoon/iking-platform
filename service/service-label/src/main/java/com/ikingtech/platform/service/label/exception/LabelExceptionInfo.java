package com.ikingtech.platform.service.label.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum LabelExceptionInfo implements FrameworkExceptionInfo {

    LABEL_NOT_FOUND("labelNotFound"),

    DUPLICATE_LABEL_NAME("duplicateLabelName");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-label";
    }
}
