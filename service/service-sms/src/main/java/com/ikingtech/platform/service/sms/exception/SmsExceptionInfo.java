package com.ikingtech.platform.service.sms.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum SmsExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 指定微信小程序不存在
     */
    SMS_NOT_FOUND("smsNotFound"),

    /**
     * 已存在相同名称的微信小程序
     */
    DUPLICATE_SMS_NAME("duplicateSmsName"),

    /**
     * 已存在相同AppId的微信小程序
     */
    DUPLICATE_SMS_APP_ID("duplicateSmsAppId");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-post";
    }
}
