package com.ikingtech.platform.service.system.role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("role_data_scope")
public class RoleDataScopeDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 896387653521596311L;

    @TableField(value = "role_id")
    private String roleId;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField(value = "data_scope_code")
    private String dataScopeCode;
}
