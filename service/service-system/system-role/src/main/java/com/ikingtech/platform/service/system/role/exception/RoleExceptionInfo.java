package com.ikingtech.platform.service.system.role.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum RoleExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 角色不存在异常
     */
    ROLE_NOT_FOUND("roleNotFound"),

    /**
     * 角色名称重复异常
     */
    DUPLICATE_ROLE_NAME("duplicateRoleName"),

    /**
     * 角色名称不应该包含空白字符异常
     */
    ROLE_NAME_SHOULD_NOT_CONTAIN_BLANK("roleNameShouldNotContainBlank");


    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-system-role";
    }
}
