package com.ikingtech.platform.service.system.role.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.role.entity.RoleMenuDO;
import com.ikingtech.platform.service.system.role.mapper.RoleMenuMapper;

/**
 * @author tie yan
 */
public class RoleMenuRepository extends ServiceImpl<RoleMenuMapper, RoleMenuDO> {
}
