package com.ikingtech.platform.service.system.division.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.division.entity.DivisionDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface DivisionMapper extends BaseMapper<DivisionDO> {
}
