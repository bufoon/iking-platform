package com.ikingtech.platform.service.system.division.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.division.entity.DivisionDO;
import com.ikingtech.platform.service.system.division.mapper.DivisionMapper;

/**
 * @author tie yan
 */
public class DivisionRepository extends ServiceImpl<DivisionMapper, DivisionDO> {
}
