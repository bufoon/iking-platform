/*
 *    Copyright (c) 2018-2025, base All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: base
 */

package com.ikingtech.platform.service.system.division.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("division")
public class DivisionDO extends SortEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 8793014108614424494L;

	@TableField("full_path")
	private String fullPath;

	@TableField("parent_id")
	private String parentId;

	@TableField("no")
	private String no;

	@TableField("name")
	private String name;

	@TableField("parent_no")
	private String parentNo;

	@TableField("division_level")
	private String divisionLevel;

	@TableField("provincial_capital")
	private Boolean provincialCapital;

	@TableLogic
	@TableField("del_flag")
	private Boolean delFlag;
}
