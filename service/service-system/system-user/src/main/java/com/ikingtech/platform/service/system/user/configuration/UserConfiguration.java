package com.ikingtech.platform.service.system.user.configuration;

import com.iking.framework.sdk.authorization.api.AuthorizationUserApi;
import com.ikingtech.framework.sdk.component.api.CompDepartmentApi;
import com.ikingtech.framework.sdk.component.api.CompUserApi;
import com.ikingtech.framework.sdk.department.api.DeptUserApi;
import com.ikingtech.framework.sdk.menu.api.MenuUserApi;
import com.ikingtech.framework.sdk.post.api.PostUserApi;
import com.ikingtech.framework.sdk.role.api.RoleUserApi;
import com.ikingtech.framework.sdk.tenant.api.TenantUserApi;
import com.ikingtech.framework.sdk.user.api.*;
import com.ikingtech.platform.service.system.user.service.*;
import com.ikingtech.platform.service.system.user.service.repository.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class UserConfiguration {

    @Bean
    public UserRepository userRepository() {
        return new UserRepository();
    }

    @Bean
    public UserRoleRepository userRoleRepository() {
        return new UserRoleRepository();
    }

    @Bean
    public UserDeptRepository userDeptRepository() {
        return new UserDeptRepository();
    }

    @Bean
    public UserPostRepository userPostRepository() {
        return new UserPostRepository();
    }

    @Bean
    public UserConfigRepository userConfigRepository() {
        return new UserConfigRepository();
    }

    @Bean
    public UserCategoryRepository userCategoryRepository() {
        return new UserCategoryRepository();
    }

    @Bean
    public UserTenantRepository userTenantRepository() {
        return new UserTenantRepository();
    }

    @Bean
    public UserSocialRepository userSocialRepository() {
        return new UserSocialRepository();
    }

    @Bean
    @ConditionalOnMissingBean({CompUserApi.class})
    public CompUserApi compUserApi(UserRepository service, UserDeptRepository userDeptService, UserTenantRepository userTenantService, CompDepartmentApi departmentApi, ModelConverter converter) {
        return new CompUserService(service, userDeptService, userTenantService, departmentApi, converter);
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.dept.service.UserDeptService"})
    public UserDeptApi userDeptApi() {
        return new DefaultUserDeptService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.role.service.UserRoleService"})
    public UserRoleApi userRoleApi() {
        return new DefaultUserRoleService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.menu.service.UserMenuService"})
    public UserMenuApi userMenuApi() {
        return new DefaultUserMenuService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.post.service.UserPostService"})
    public UserPostApi userPostApi() {
        return new DefaultUserPostService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.tenant.service.UserTenantService"})
    public UserTenantApi userTenantApi() {
        return new DefaultUserTenantService();
    }

    @Bean
    public AuthorizationUserApi authorizationUserApi(UserRepository repo,
                                                     UserRoleRepository userRoleRepo,
                                                     UserDeptRepository userDeptRepo,
                                                     UserRoleApi userRoleApi,
                                                     UserDeptApi userDeptApi) {
        return new AuthorizationUserService(repo, userRoleRepo, userDeptRepo, userRoleApi, userDeptApi);
    }

    @Bean
    public DeptUserApi deptUserApi(UserRepository repo, UserDeptRepository userDeptRepo) {
        return new DeptUserService(repo, userDeptRepo);
    }

    @Bean
    public MenuUserApi menuUserApi(UserRoleRepository userRoleRepo) {
        return new MenuUserService(userRoleRepo);
    }

    @Bean
    public PostUserApi postUserApi(UserPostRepository userPostRepo) {
        return new PostUserService(userPostRepo);
    }

    @Bean
    public RoleUserApi roleUserApi(UserRoleRepository userRoleRepo) {
        return new RoleUserService(userRoleRepo);
    }

    @Bean
    public TenantUserApi tenantUserApi(UserTenantRepository userTenantRepo) {
        return new TenantUserService(userTenantRepo);
    }
}
