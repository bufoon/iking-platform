package com.ikingtech.platform.service.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.user.entity.UserRoleDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRoleDO> {
}
