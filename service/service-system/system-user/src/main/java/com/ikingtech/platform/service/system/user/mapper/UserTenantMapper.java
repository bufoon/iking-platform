package com.ikingtech.platform.service.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.user.entity.UserTenantDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhangqiang
 */
@Mapper
public interface UserTenantMapper extends BaseMapper<UserTenantDO> {
}
