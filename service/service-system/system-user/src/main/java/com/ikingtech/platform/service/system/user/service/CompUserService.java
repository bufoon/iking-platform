package com.ikingtech.platform.service.system.user.service;

import com.ikingtech.framework.sdk.component.api.CompDepartmentApi;
import com.ikingtech.framework.sdk.component.model.ComponentUser;
import com.ikingtech.platform.service.system.user.service.repository.ModelConverter;
import com.ikingtech.platform.service.system.user.service.repository.UserDeptRepository;
import com.ikingtech.platform.service.system.user.service.repository.UserRepository;
import com.ikingtech.platform.service.system.user.service.repository.UserTenantRepository;

import java.util.List;

/**
 * @author tie yan
 */
public class CompUserService extends AbstractCompUserService {

    public CompUserService(UserRepository service, UserDeptRepository userDeptService, UserTenantRepository userTenantService, CompDepartmentApi departmentApi, ModelConverter converter) {
        super(service, userDeptService, userTenantService, departmentApi, converter);
    }

    @Override
    public List<ComponentUser> listByIdsAndName(List<String> ids, String name) {
        return super.listByIdsAndName(ids, name);
    }

    @Override
    public List<ComponentUser> listByDeptId(String deptId) {
        return super.listByDeptId(deptId);
    }
}
