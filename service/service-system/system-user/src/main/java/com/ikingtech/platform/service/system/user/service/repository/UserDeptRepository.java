package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.user.entity.UserDeptDO;
import com.ikingtech.platform.service.system.user.mapper.UserDeptMapper;

/**
 * @author tie yan
 */
public class UserDeptRepository extends ServiceImpl<UserDeptMapper, UserDeptDO> {
}
