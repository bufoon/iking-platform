package com.ikingtech.platform.service.system.user.service;

import com.ikingtech.framework.sdk.user.api.UserMenuApi;
import com.ikingtech.framework.sdk.utils.Tools;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tie yan
 */
public class DefaultUserMenuService implements UserMenuApi {

    @Override
    public List<String> loadIdByCodes(List<String> menuCodes, String tenantCode) {
        return new ArrayList<>();
    }

    @Override
    public String loadIdByCode(String menuCode, String tenantCode) {
        return Tools.Str.EMPTY;
    }
}
