package com.ikingtech.platform.service.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 用户表
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
public class UserDO extends BaseEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 2730971823022925927L;

	@TableField(value = "username")
	private String username;

	@TableField(value = "password")
	private String password;

	@TableField(value = "password_modify_time")
	private LocalDateTime passwordModifyTime;

	@TableField(value = "nickname")
	private String nickname;

	@TableField(value = "name")
	private String name;

	@TableField(value = "avatar")
	private String avatar;

	@TableField(value = "birth_day")
	private LocalDate birthday;

	@TableField(value = "phone")
	private String phone;

	@TableField(value = "email")
	private String email;

	@TableField(value = "identity_no")
	private String identityNo;

	@TableField(value = "sex")
	private String sex;

	@TableField(value = "locked")
	private Boolean locked;

	@TableField(value = "lock_type")
	private String lockType;

	@TableField(value = "platform_user")
	private Boolean platformUser;

	@TableField(value = "admin_user")
	private Boolean adminUser;

	@TableLogic
	@TableField(value = "del_flag")
	private Boolean delFlag;
}
