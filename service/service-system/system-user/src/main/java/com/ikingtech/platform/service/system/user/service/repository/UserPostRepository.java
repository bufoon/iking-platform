package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.user.entity.UserPostDO;
import com.ikingtech.platform.service.system.user.mapper.UserPostMapper;

/**
 * @author tie yan
 */
public class UserPostRepository extends ServiceImpl<UserPostMapper, UserPostDO> {
}
