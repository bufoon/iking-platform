package com.ikingtech.platform.service.system.variable.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum VariableExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 指定参数不存在
     */
    VARIABLE_NOT_FOUND("variableNotFound"),

    /**
     * 无效的参数值
     */
    INVALID_VARIABLE_VALUE("invalidVariableValue");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-variable";
    }
}
