package com.ikingtech.platform.service.system.post.service;

import com.ikingtech.framework.sdk.user.api.UserPostApi;
import com.ikingtech.framework.sdk.user.model.UserPostDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.post.service.repository.PostRepository;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class UserPostService implements UserPostApi {

    private final PostRepository repo;

    /**
     * 根据岗位ID列表加载岗位信息
     *
     * @param postIds 岗位ID列表
     * @return 岗位信息列表
     */
    @Override
    public List<UserPostDTO> loadByIds(List<String> postIds) {
        // 如果岗位ID列表为空，则返回空列表
        if(Tools.Coll.isBlank(postIds)){
            return new ArrayList<>();
        }
        // 将岗位实体列表转换为岗位信息列表
        return Tools.Coll.convertList(this.repo.listByIds(postIds), entity -> {
            UserPostDTO userPost = new UserPostDTO();
            userPost.setPostId(entity.getId());
            userPost.setPostName(entity.getName());
            return userPost;
        });
    }
}
