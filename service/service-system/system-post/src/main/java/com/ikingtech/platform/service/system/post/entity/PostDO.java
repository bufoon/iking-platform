package com.ikingtech.platform.service.system.post.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("post")
public class PostDO extends SortEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 8793014108614424494L;

	@TableField(value = "name")
	private String name;

	@TableField(value = "tenant_code", fill = FieldFill.INSERT)
	private String tenantCode;

	@TableField(value = "remark")
	private String remark;

	@TableLogic
	@TableField(value = "del_flag")
	private Boolean delFlag;
}
