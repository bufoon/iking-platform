package com.ikingtech.platform.service.system.dept.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.dept.entity.DepartmentDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface DeptMapper extends BaseMapper<DepartmentDO> {
}
