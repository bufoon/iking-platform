package com.ikingtech.platform.service.system.dept.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.department.model.DeptQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.dept.entity.DepartmentDO;
import com.ikingtech.platform.service.system.dept.mapper.DeptMapper;

import java.util.List;

/**
 * @author tie yan
 */
public class DeptRepository extends ServiceImpl<DeptMapper, DepartmentDO> {

    public static LambdaQueryWrapper<DepartmentDO> createQueryWrapper(DeptQueryParamDTO queryParam, List<String> dataScope, String tenantCode) {
        return Wrappers.<DepartmentDO>lambdaQuery()
                .in(Tools.Coll.isNotBlank(dataScope), DepartmentDO::getId, dataScope)
                .eq(Tools.Str.isNotBlank(tenantCode), DepartmentDO::getTenantCode, tenantCode)
                .in(Tools.Str.isNotBlank(queryParam.getParentDeptId()), DepartmentDO::getParentId, queryParam.getParentDeptId())
                .like(Tools.Str.isNotBlank(queryParam.getName()), DepartmentDO::getName, queryParam.getName())
                .eq(Tools.Str.isNotBlank(queryParam.getManagerId()), DepartmentDO::getManagerId, queryParam.getManagerId())
                .orderByDesc(DepartmentDO::getCreateTime);
    }
}
