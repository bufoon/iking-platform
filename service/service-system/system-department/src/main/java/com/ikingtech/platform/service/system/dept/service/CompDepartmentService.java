package com.ikingtech.platform.service.system.dept.service;

import com.ikingtech.framework.sdk.component.model.ComponentDepartment;
import com.ikingtech.platform.service.system.dept.service.repository.DeptRepository;
import com.ikingtech.platform.service.system.dept.service.repository.ModelConverter;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public class CompDepartmentService extends AbstractCompDepartmentService {

    public CompDepartmentService(DeptRepository service, ModelConverter converter) {
        super(service, converter);
    }

    @Override
    public List<ComponentDepartment> listByName(String name) {
        return super.listByName(name);
    }

    @Override
    public List<ComponentDepartment> listByParentId(String parentId) {
        return super.listByParentId(parentId);
    }

    @Override
    public ComponentDepartment getParentDepartment(String parentId) {
        return super.getParentDepartment(parentId);
    }

    @Override
    public Map<String, String> extractDepartmentFullName(Map<String, String> deptFullPathMap) {
        return super.extractDepartmentFullName(deptFullPathMap);
    }
}
