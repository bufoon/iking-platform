package com.ikingtech.platform.service.system.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.dict.entity.DictDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface DictMapper extends BaseMapper<DictDO> {
}
