package com.ikingtech.platform.service.system.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.dict.entity.DictItemDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface DictItemMapper extends BaseMapper<DictItemDO> {
}
