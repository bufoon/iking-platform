package com.ikingtech.platform.service.system.menu.service;

import com.ikingtech.framework.sdk.menu.api.MenuRoleApi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author tie yan
 */
public class DefaultMenuRoleService implements MenuRoleApi {

    @Override
    public void removeRoleMenu(Collection<String> menuIds) {

    }

    @Override
    public List<String> loadMenuId(String roleId) {
        return new ArrayList<>();
    }

    @Override
    public List<String> loadMenuId(List<String> roleIds) {
        return new ArrayList<>();
    }
}
