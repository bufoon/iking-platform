package com.ikingtech.platform.service.system.menu.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum MenuExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 菜单不存在
     */
    MENU_NOT_FOUND("menuNotFound"),

    /**
     * 父菜单不存在
     */
    PARENT_MENU_NOT_FOUND("parentMenuNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-menu";
    }
}
