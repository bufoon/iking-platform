package com.ikingtech.platform.service.system.country.configuration;

import com.ikingtech.platform.service.system.country.service.CountryRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class CountryConfiguration {

    @Bean
    public CountryRepository countryRepository() {
        return new CountryRepository();
    }
}
