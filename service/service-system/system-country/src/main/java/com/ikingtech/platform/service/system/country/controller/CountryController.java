package com.ikingtech.platform.service.system.country.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.country.api.CountryApi;
import com.ikingtech.framework.sdk.country.model.CountryDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.system.country.entity.CountryDO;
import com.ikingtech.platform.service.system.country.service.CountryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
@ApiController(value = "/system/country", name = "系统管理-世界国家管理", description = "系统管理-世界国家管理")
public class CountryController implements CountryApi {

    private final CountryRepository repo;

    /**
     * 获取所有国家信息
     *
     * @return 返回包含所有国家信息的列表
     */
    @Override
    public R<List<CountryDTO>> all() {
        return R.ok(Tools.Coll.convertList(this.repo.list(Wrappers.<CountryDO>lambdaQuery().orderByDesc(CountryDO::getCode)), entity -> Tools.Bean.copy(entity, CountryDTO.class)));
    }
}
