package com.ikingtech.platform.business.workbench.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.workbench.mapper.TodoTaskMapper;
import com.ikingtech.platform.business.workbench.entity.TodoTaskDO;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.workbench.model.TodoTaskQueryParamDTO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Service
public class TodoTaskService extends ServiceImpl<TodoTaskMapper, TodoTaskDO> {

    public boolean exist(String id) {
        return this.baseMapper.exists(Wrappers.<TodoTaskDO>lambdaQuery().eq(TodoTaskDO::getId, id));
    }

    public PageResult<TodoTaskDO> listPage(TodoTaskQueryParamDTO queryParam) {
        return PageResult.build(this.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<TodoTaskDO>lambdaQuery()
                .eq(Tools.Str.isNotBlank(queryParam.getUserId()), TodoTaskDO::getUserId, queryParam.getUserId())
                .like(Tools.Str.isNotBlank(queryParam.getSummary()), TodoTaskDO::getSummary, queryParam.getSummary())
                .orderByDesc(TodoTaskDO::getCreateTime)));
    }

    public void removeByBusinessIdAndUserIds(String businessId, List<String> userIds) {
        this.remove(Wrappers.<TodoTaskDO>lambdaQuery().eq(TodoTaskDO::getBusinessId, businessId).in(TodoTaskDO::getUserId, userIds));
    }
}
