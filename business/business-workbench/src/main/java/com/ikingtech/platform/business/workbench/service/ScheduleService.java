package com.ikingtech.platform.business.workbench.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.workbench.model.ScheduleQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.business.workbench.entity.ScheduleDO;
import com.ikingtech.platform.business.workbench.mapper.ScheduleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Service
@RequiredArgsConstructor
public class ScheduleService extends ServiceImpl<ScheduleMapper, ScheduleDO> {

    public Boolean exist(String id) {
        return this.baseMapper.exists(Wrappers.<ScheduleDO>lambdaQuery().eq(ScheduleDO::getId, id));
    }

    public PageResult<ScheduleDO> listPage(ScheduleQueryParamDTO queryParam) {
        return PageResult.build(this.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<ScheduleDO>lambdaQuery()
                .in(Tools.Coll.isNotBlank(queryParam.getIds()), ScheduleDO::getId, queryParam.getIds())
                .eq(Tools.Str.isNotBlank(queryParam.getStatus()), ScheduleDO::getStatus, queryParam.getStatus())
                .like(Tools.Str.isNotBlank(queryParam.getTitle()), ScheduleDO::getTitle, queryParam.getTitle())
                .eq(Tools.Str.isNotBlank(queryParam.getType()), ScheduleDO::getType, queryParam.getType())
                .le(null != queryParam.getEndTime(), ScheduleDO::getEstimateStartTime, Tools.DateTime.Formatter.simple(queryParam.getEndTime()))
                .like(Tools.Str.isNotBlank(queryParam.getRemark()), ScheduleDO::getRemark, queryParam.getRemark())
                .ge(null != queryParam.getStartTime(), ScheduleDO::getEstimateStartTime, Tools.DateTime.Formatter.simple(queryParam.getStartTime()))
                .orderByDesc(ScheduleDO::getCreateTime)));
    }

    public List<ScheduleDO> listByEstimateStartTimeBetween(LocalDateTime startTime, LocalDateTime endTime) {
        return this.list(Wrappers.<ScheduleDO>lambdaQuery()
                .le(null != endTime, ScheduleDO::getEstimateStartTime, Tools.DateTime.Formatter.simple(endTime))
                .ge(null != startTime, ScheduleDO::getEstimateStartTime, Tools.DateTime.Formatter.simple(startTime))
                .orderByDesc(ScheduleDO::getCreateTime));
    }
}
