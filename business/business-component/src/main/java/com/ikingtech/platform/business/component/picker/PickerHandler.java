package com.ikingtech.platform.business.component.picker;

import com.ikingtech.framework.sdk.component.api.CompDepartmentApi;
import com.ikingtech.framework.sdk.component.api.CompPostApi;
import com.ikingtech.framework.sdk.component.api.CompRoleApi;
import com.ikingtech.framework.sdk.component.api.CompUserApi;
import com.ikingtech.framework.sdk.component.model.*;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.ROOT_DEPT_ID;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
@ApiController(value = "/component/pick", name = "业务组件", description = "业务组件")
public class PickerHandler {

    private final CompUserApi compUserApi;

    private final CompDepartmentApi compDepartmentApi;

    private final CompRoleApi compRoleApi;

    private final CompPostApi compPostApi;

    /**
     * 部门选择
     *
     * @param queryParam 查询条件
     * @return 部门选择结果
     */
    @PostRequest(order = 1, value = "/department", summary = "部门选择", description = "部门选择")
    public R<PickResultDTO> departmentPick(@Parameter(name = "queryParam", description = "查询条件")
                                           @RequestBody PickerElementQueryParamDTO queryParam) {
        PickResultDTO result = new PickResultDTO();
        if (Tools.Str.isBlank(queryParam.getParentDepartmentId())) {
            queryParam.setParentDepartmentId(Tools.Str.join(Me.tenantCode(), ROOT_DEPT_ID, "_"));
        }
        result.setRootDepartment(this.compDepartmentApi.getParentDepartment(queryParam.getParentDepartmentId()));
        result.setDepartments(this.pickDepartment(queryParam));
        return R.ok(result);
    }

    /**
     * 角色选择
     *
     * @param queryParam 查询条件
     * @return 选择结果
     */
    @PostRequest(order = 2, value = "/role", summary = "角色选择", description = "角色选择")
    public R<PickResultDTO> rolePick(@Parameter(name = "queryParam", description = "查询条件")
                                     @RequestBody PickerElementQueryParamDTO queryParam) {
        PickResultDTO result = new PickResultDTO();
        result.setRoles(this.pickRole(queryParam));
        return R.ok(result);
    }

    /**
     * 岗位选择
     *
     * @param queryParam 查询条件
     * @return 选择结果
     */
    @PostRequest(order = 3, value = "/post", summary = "岗位选择", description = "岗位选择")
    public R<PickResultDTO> postPick(@Parameter(name = "queryParam", description = "查询条件")
                                     @RequestBody PickerElementQueryParamDTO queryParam) {
        PickResultDTO result = new PickResultDTO();
        result.setPosts(this.pickPost(queryParam));
        return R.ok(result);
    }

    /**
     * 指定用户选择
     *
     * @param queryParam 查询条件
     * @return 选择结果
     */
    @PostRequest(order = 4, value = "/specified-user", summary = "指定用户选择", description = "指定用户选择")
    public R<PickResultDTO> specifiedUserPick(@Parameter(name = "queryParam", description = "查询条件")
                                              @RequestBody PickerElementQueryParamDTO queryParam) {
        PickResultDTO result = new PickResultDTO();
        result.setUsers(this.pickUser(queryParam));
        return R.ok(result);
    }

    /**
     * 混合选择
     *
     * @param queryParam 查询条件
     * @return 混合选择结果
     */
    @PostRequest(order = 5, value = "/mix", summary = "混合选择", description = "混合选择")
    public R<PickResultDTO> mixPick(@Parameter(name = "queryParam", description = "查询条件")
                                    @RequestBody PickerElementQueryParamDTO queryParam) {
        // 创建混合选择结果对象
        PickResultDTO result = new PickResultDTO();
        if (Tools.Str.isBlank(queryParam.getParentDepartmentId())) {
            queryParam.setParentDepartmentId(Tools.Str.join(Me.tenantCode(), ROOT_DEPT_ID, "_"));
        }
        // 设置根部门
        result.setRootDepartment(this.compDepartmentApi.getParentDepartment(queryParam.getParentDepartmentId()));
        // 设置部门
        result.setDepartments(this.pickDepartment(queryParam));
        // 设置用户
        result.setUsers(this.pickUser(queryParam));
        // 设置角色
        result.setRoles(this.pickRole(queryParam));
        // 设置职位
        result.setPosts(this.pickPost(queryParam));
        // 返回混合选择结果
        return R.ok(result);
    }

    /**
     * 根据查询参数选择部门
     *
     * @param queryParam 查询参数
     * @return 部门列表
     */
    private List<ComponentDepartment> pickDepartment(PickerElementQueryParamDTO queryParam) {
        if (Tools.Str.isNotBlank(queryParam.getName())) {
            // 如果查询参数的名称不为空，则根据名称查询部门列表
            return this.compDepartmentApi.listByName(queryParam.getName());
        } else {
            // 如果查询参数的名称为空，则根据父部门ID查询部门列表
            return this.compDepartmentApi.listByParentId(queryParam.getParentDepartmentId());
        }
    }


    /**
     * 根据查询参数选择角色
     *
     * @param queryParam 查询参数
     * @return 选择的角色列表
     */
    private List<ComponentRole> pickRole(PickerElementQueryParamDTO queryParam) {
        return this.compRoleApi.listByName(queryParam.getName());
    }

    /**
     * 根据查询参数选择岗位
     *
     * @param queryParam 查询参数
     * @return 选择的岗位列表
     */
    private List<ComponentPost> pickPost(PickerElementQueryParamDTO queryParam) {
        // 调用compPostApi的listByName方法，根据查询参数的名称获取岗位列表
        return this.compPostApi.listByName(queryParam.getName());
    }

    /**
     * 选择用户
     *
     * @param queryParam 查询参数
     * @return 用户列表
     */
    private List<ComponentUser> pickUser(PickerElementQueryParamDTO queryParam) {
        // 如果用户ID或姓名不为空，则根据用户ID和姓名查询用户列表
        if (Tools.Coll.isNotBlank(queryParam.getUserIds()) || Tools.Str.isNotBlank(queryParam.getName())) {
            return this.compUserApi.listByIdsAndName(queryParam.getUserIds(), queryParam.getName());
        } else {
            // 否则根据部门ID查询用户列表
            return this.compUserApi.listByDeptId(queryParam.getParentDepartmentId());
        }
    }

}
