package com.ikingtech.platform.business.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.business.message.entity.MessageTemplateDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface MessageTemplateMapper extends BaseMapper<MessageTemplateDO> {
}
