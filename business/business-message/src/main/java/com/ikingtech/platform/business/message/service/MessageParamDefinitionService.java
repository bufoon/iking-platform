package com.ikingtech.platform.business.message.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageParamDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageParamDefinitionMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Service
public class MessageParamDefinitionService extends ServiceImpl<MessageParamDefinitionMapper, MessageParamDefinitionDO> {

    public List<MessageParamDefinitionDO> listByChannelDefinitionId(String channelDefinitionId) {
        return this.list(Wrappers.<MessageParamDefinitionDO>lambdaQuery()
                .eq(MessageParamDefinitionDO::getChannelDefinitionId, channelDefinitionId)
                .orderByAsc(MessageParamDefinitionDO::getSortOrder));
    }

    public List<MessageParamDefinitionDO> listPreDefinitionByTemplateId(String templateId) {
        return this.list(Wrappers.<MessageParamDefinitionDO>lambdaQuery()
                .eq(MessageParamDefinitionDO::getTemplateId, templateId)
                .eq(MessageParamDefinitionDO::getPreDefinition, true)
                .orderByAsc(MessageParamDefinitionDO::getSortOrder));
    }

    public void removePreDefinitionByTemplateIds(List<String> templateIds) {
        this.remove(Wrappers.<MessageParamDefinitionDO>lambdaQuery()
                .in(MessageParamDefinitionDO::getTemplateId, templateIds)
                .eq(MessageParamDefinitionDO::getPreDefinition, true));
    }

    public List<MessageParamDefinitionDO> listByTemplateIdExcludePreDefinition(String templateId) {
        return this.list(Wrappers.<MessageParamDefinitionDO>lambdaQuery()
                .eq(MessageParamDefinitionDO::getTemplateId, templateId)
                .ne(MessageParamDefinitionDO::getPreDefinition, true)
                .orderByAsc(MessageParamDefinitionDO::getSortOrder));
    }

    public void removeNonPreDefinitionByTemplateId(String templateId) {
        this.remove(Wrappers.<MessageParamDefinitionDO>lambdaQuery()
                .eq(MessageParamDefinitionDO::getTemplateId, templateId)
                .eq(MessageParamDefinitionDO::getPreDefinition, false));
    }

    public void removeSettledDefinitionByTemplateIds(List<String> templateIds) {
        this.remove(Wrappers.<MessageParamDefinitionDO>lambdaQuery()
                .in(MessageParamDefinitionDO::getTemplateId, templateIds)
                .eq(MessageParamDefinitionDO::getPreDefinition, false));
    }

    public void removeByTemplateIds(List<String> templateIds) {
        this.remove(Wrappers.<MessageParamDefinitionDO>lambdaQuery().in(MessageParamDefinitionDO::getTemplateId, templateIds));
    }

    public List<MessageParamDefinitionDO> listByChannelDefinitionIds(List<String> channelDefinitionIds) {
        return this.list(Wrappers.<MessageParamDefinitionDO>lambdaQuery()
                .in(MessageParamDefinitionDO::getChannelDefinitionId, channelDefinitionIds)
                .orderByAsc(MessageParamDefinitionDO::getSortOrder));
    }
}
