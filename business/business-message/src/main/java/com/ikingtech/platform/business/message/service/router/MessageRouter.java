package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.push.common.MessageRedirect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ikingtech.framework.sdk.utils.Tools.Str.CONTENT_PLACEHOLDER_PREFIX;
import static com.ikingtech.framework.sdk.utils.Tools.Str.CONTENT_PLACEHOLDER_SUFFIX;

/**
 * @author tie yan
 */
public interface MessageRouter {

    RouteResult route(RouteRequest request);

    MessageSendChannelEnum channel();

    default List<MessageRedirect> createMessageRedirect(Map<String, String> redirects, Map<String, Object> messageParamMap) {
        List<MessageRedirect> result = new ArrayList<>();
        redirects.forEach((redirectName, redirectLink) -> {
            MessageRedirect redirect = new MessageRedirect();
            redirect.setRedirectLink(Tools.Str.replace(redirectLink, CONTENT_PLACEHOLDER_PREFIX, CONTENT_PLACEHOLDER_SUFFIX, messageParamMap));
            redirect.setRedirectName(redirectName);
            result.add(redirect);
        });
        return result;
    }

    default Map<String, Object> convertToTemplateParam(Map<String, String> paramDefinitions, Map<String, Object> messageParamMap) {
        Map<String, Object> templateParams = new HashMap<>();
        paramDefinitions.forEach((param, mappedParam) -> templateParams.put(mappedParam, messageParamMap.get(param)));
        return templateParams;
    }
}
