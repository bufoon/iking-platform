package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class MessageRouteProxy {

    private final List<MessageRouter> routers;

    public MessageRouter determine(String channel) {
        for (MessageRouter router : routers) {
            if (channel.equals(router.channel().name())) {
                return router;
            }
        }
        throw new FrameworkException("message router not matched!");
    }
}
