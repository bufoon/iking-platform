package com.ikingtech.platform.business.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 消息定义
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("message_param_definition")
public class MessageParamDefinitionDO extends SortEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -1643389575066357927L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("template_id")
    private String templateId;

    @TableField("channel_definition_id")
    private String channelDefinitionId;

    @TableField("pre_definition")
    private Boolean preDefinition;

    @TableField("param_name")
    private String paramName;

    @TableField("param_description")
    private String paramDescription;

    @TableField("mapped_param_name")
    private String mappedParamName;
}
