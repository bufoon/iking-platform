package com.ikingtech.platform.business.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("message")
public class MessageDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 3712811650301585016L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("template_id")
    private String templateId;

    @TableField("channel")
    private String channel;

    @TableField("business_name")
    private String businessName;

    @TableField("business_key")
    private String businessKey;

    @TableField("message_template_key")
    private String messageTemplateKey;

    @TableField("message_title")
    private String messageTitle;

    @TableField("message_content")
    private String messageContent;

    @TableField("redirect_to")
    private String redirectTo;

    @TableField("deliver_status")
    private String deliverStatus;

    @TableField("deliver_time")
    private LocalDateTime deliverTime;

    @TableField("receiver_id")
    private String receiverId;

    @TableField("read_status")
    private String readStatus;

    @TableField("read_time")
    private LocalDateTime readTime;

    @TableField("cause")
    private String cause;
}
