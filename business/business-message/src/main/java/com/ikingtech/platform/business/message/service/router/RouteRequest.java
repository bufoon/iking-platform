package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.user.model.UserBasicDTO;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class RouteRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 8541351724214370152L;

    protected RouteRequest(Builder builder) {
        this.businessKey = builder.businessKey;
        this.channelId = builder.channelId;
        this.channelTemplateId = builder.channelTemplateId;
        this.showNotification = builder.showNotification;
        this.templateContent = builder.templateContent;
        this.paramDefinitionMap = builder.paramDefinitionMap;
        this.redirectDefinitinoMap = builder.redirectDefinitinoMap;
        this.target = builder.target;
        this.messageWrapper = builder.messageWrapper;
    }

    private String businessKey;

    private String channelId;

    private String channelTemplateId;

    private String templateContent;

    private Boolean showNotification;

    private Map<String, String> paramDefinitionMap;

    private Map<String, String> redirectDefinitinoMap;

    private UserBasicDTO target;

    private Object messageWrapper;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String businessKey;

        private String channelId;

        private String channelTemplateId;

        private String templateContent;

        private Boolean showNotification;

        private Map<String, String> paramDefinitionMap;

        private Map<String, String> redirectDefinitinoMap;

        private UserBasicDTO target;

        private Object messageWrapper;

        public Builder businessKey(String businessKey) {
            this.businessKey = businessKey;
            return this;
        }

        public Builder channelId(String channelId) {
            this.channelId = channelId;
            return this;
        }

        public Builder channelTemplateId(String channelTemplateId) {
            this.channelTemplateId = channelTemplateId;
            return this;
        }

        public Builder templateContent(String content) {
            this.templateContent = content;
            return this;
        }

        public Builder showNotification(Boolean showNotification) {
            this.showNotification = showNotification;
            return this;
        }

        public Builder paramMap(Map<String, String> paramMap) {
            this.paramDefinitionMap = paramMap;
            return this;
        }

        public Builder redirect(Map<String, String> redirect) {
            this.redirectDefinitinoMap = redirect;
            return this;
        }

        public Builder target(UserBasicDTO target) {
            this.target = target;
            return this;
        }

        public Builder messageWrapper(Object messageWrapper) {
            this.messageWrapper = messageWrapper;
            return this;
        }

        public RouteRequest build() {
            return new RouteRequest(this);
        }
    }
}
