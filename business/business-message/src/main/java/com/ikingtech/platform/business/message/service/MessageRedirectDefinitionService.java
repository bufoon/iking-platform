package com.ikingtech.platform.business.message.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageRedirectDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageRedirectDefinitionMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MessageRedirectDefinitionService extends ServiceImpl<MessageRedirectDefinitionMapper, MessageRedirectDefinitionDO> {

    public void removeByTemplateId(String templateId) {
        this.remove(Wrappers.<MessageRedirectDefinitionDO>lambdaQuery().eq(MessageRedirectDefinitionDO::getTemplateId, templateId));
    }

    public List<MessageRedirectDefinitionDO> listByTemplateId(String templateId) {
        return this.list(Wrappers.<MessageRedirectDefinitionDO>lambdaQuery().eq(MessageRedirectDefinitionDO::getTemplateId, templateId));
    }

    public void removeByTemplateIds(List<String> templateIds) {
        this.remove(Wrappers.<MessageRedirectDefinitionDO>lambdaQuery().in(MessageRedirectDefinitionDO::getTemplateId, templateIds));
    }

    public List<MessageRedirectDefinitionDO> listByChannelDefinitionIds(List<String> channelDefinitionIds) {
        return this.list(Wrappers.<MessageRedirectDefinitionDO>lambdaQuery().in(MessageRedirectDefinitionDO::getChannelDefinitionId, channelDefinitionIds));
    }
}
